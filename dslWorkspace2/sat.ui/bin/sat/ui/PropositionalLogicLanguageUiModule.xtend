/*
 * generated by Xtext 2.15.0
 */
package sat.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor
import org.eclipse.xtext.ui.editor.autoedit.DefaultAutoEditStrategyProvider

import sat.ui.autoedit.PropositionalLogicLanguageAutoEditStrategyProvider

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class PropositionalLogicLanguageUiModule extends AbstractPropositionalLogicLanguageUiModule {
	def Class<? extends DefaultAutoEditStrategyProvider> bindDefaultAutoEditStrategyProvider() {
		return PropositionalLogicLanguageAutoEditStrategyProvider
	}
}
