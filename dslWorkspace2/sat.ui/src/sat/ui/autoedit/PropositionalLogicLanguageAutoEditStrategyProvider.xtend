package sat.ui.autoedit;

import org.eclipse.jface.text.IDocument;
import org.eclipse.xtext.ui.editor.autoedit.DefaultAutoEditStrategyProvider;
import org.eclipse.xtext.ui.editor.autoedit.ShortCutEditStrategy;

import java.util.Set;

import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.ui.editor.autoedit.DefaultAutoEditStrategyProvider;
import org.eclipse.xtext.ui.editor.model.XtextDocument;

import com.google.inject.Inject;
import com.google.inject.Provider;



 class PropositionalLogicLanguageAutoEditStrategyProvider extends DefaultAutoEditStrategyProvider {

	@Inject
	Provider<IGrammarAccess> iGrammar;
	
	@Inject
    protected Provider<ShortCutEditStrategy> shortCut;

//	private Set<String> KWDS;↔⟶⨂↓∨↑∧¬⟙⟘

	
	override protected void configure(IEditStrategyAcceptor acceptor) {
		acceptor.accept(shortCut.get().configure("<->", "↔"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("-->", "⟶"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("xor", "⨂"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("nor", "↓"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("or", "∨"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("nand", "↑"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("and", "∧"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("not", "¬"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("true", "⟙"), IDocument.DEFAULT_CONTENT_TYPE);
		acceptor.accept(shortCut.get().configure("false", "⟘"), IDocument.DEFAULT_CONTENT_TYPE);

//	    KWDS = GrammarUtil.getAllKeywords(iGrammar.get().getGrammar());
//
//	    IAutoEditStrategy strategy = new IAutoEditStrategy() 
//	    {
//
//	        @Override
//	        public void customizeDocumentCommand(IDocument document, DocumentCommand command) 
//	        {
//	            if ( command.text.length() == 0 || command.text.charAt(0) > ' ') return;
//
//	            IRegion reg = ((XtextDocument) document).getLastDamage();
//
//	            try {
//	                String token = document.get(reg.getOffset(), reg.getLength());
//	                String possibleKWD = token.toLowerCase();
//	                if ( token.equals(possibleKWD.toUpperCase()) || !KWDS.contains(possibleKWD) ) return;
//	                document.replace(reg.getOffset(), reg.getLength(), possibleKWD.toUpperCase());
//
//	            } 
//	            catch (Exception e) 
//	            {
//	                System.out.println("AutoEdit error.\n" + e.getMessage());   
//	            }
//	        }
//	    };
//
//	    acceptor.accept(strategy, IDocument.DEFAULT_CONTENT_TYPE);
//
	    super.configure(acceptor);

	    }
//
//	@Override
//	protected void configure(IEditStrategyAcceptor acceptor) {
//		super.configure(acceptor);
//
//		acceptor.accept(shortCut.get().configure("->", "\\rightarrow"), IDocument.DEFAULT_CONTENT_TYPE);
//		acceptor.accept(new InterpreterAutoEdit(), IDocument.DEFAULT_CONTENT_TYPE);
//	}
}
