/**
 * generated by Xtext 2.15.0
 */
package sat.ui.contentassist;

import sat.ui.contentassist.AbstractPropositionalLogicLanguageProposalProvider;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class PropositionalLogicLanguageProposalProvider extends AbstractPropositionalLogicLanguageProposalProvider {
}
