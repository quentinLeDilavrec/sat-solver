package sat.ui.autoedit;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.jface.text.IDocument;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.ui.editor.autoedit.AbstractEditStrategyProvider;
import org.eclipse.xtext.ui.editor.autoedit.DefaultAutoEditStrategyProvider;
import org.eclipse.xtext.ui.editor.autoedit.ShortCutEditStrategy;

@SuppressWarnings("all")
public class PropositionalLogicLanguageAutoEditStrategyProvider extends DefaultAutoEditStrategyProvider {
  @Inject
  private Provider<IGrammarAccess> iGrammar;
  
  @Inject
  protected Provider<ShortCutEditStrategy> shortCut;
  
  @Override
  protected void configure(final AbstractEditStrategyProvider.IEditStrategyAcceptor acceptor) {
    acceptor.accept(this.shortCut.get().configure("<->", "↔"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("-->", "⟶"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("xor", "⨂"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("nor", "↓"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("or", "∨"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("nand", "↑"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("and", "∧"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("not", "¬"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("true", "⟙"), IDocument.DEFAULT_CONTENT_TYPE);
    acceptor.accept(this.shortCut.get().configure("false", "⟘"), IDocument.DEFAULT_CONTENT_TYPE);
    super.configure(acceptor);
  }
}
