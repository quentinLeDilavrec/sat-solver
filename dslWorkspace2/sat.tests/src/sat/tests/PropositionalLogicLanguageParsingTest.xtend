/*
 * generated by Xtext 2.15.0
 */
package sat.tests

import com.google.inject.Inject	
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import sat.propositionalLogicLanguage.Proposition
import sat.propositionalLogicLanguage.Primitive
import sat.propositionalLogicLanguage.Biimplies
import org.eclipse.xtext.Disjunction
import sat.propositionalLogicLanguage.Binary
import sat.propositionalLogicLanguage.Conjunction
import sat.propositionalLogicLanguage.ExclusiveDisjunction
import sat.propositionalLogicLanguage.NegativeDisjunction
import sat.propositionalLogicLanguage.NegativeConjunction
import org.eclipse.emf.ecore.EObject
import sat.propositionalLogicLanguage.impl.DisjunctionImpl
import sat.propositionalLogicLanguage.impl.BiimpliesImpl
import sat.propositionalLogicLanguage.impl.BinaryImpl
import sat.propositionalLogicLanguage.File

@ExtendWith(InjectionExtension)
@InjectWith(PropositionalLogicLanguageInjectorProvider)
class PropositionalLogicLanguageParsingTest {
	@Inject
	ParseHelper<File> parseHelper
	
	@Test
	def void trueTest() {
		val result = parseHelper.parse('''
			True
		''').content.get(0)
		println(result);
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			Primitive: 
				Assertions.assertEquals("True", result.name)
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
		
	}
	
	@Test
	def void falseTest() {
		val result = parseHelper.parse('''
			False
		''').content.get(0)
		println(result);
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			Primitive: 
				Assertions.assertEquals("False", result.name)
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	@Test
	def void variable() {
		val result = parseHelper.parse('''
			A
		''').content.get(0)
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			Primitive: 
				Assertions.assertEquals("A", result.name)
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	@Test
	def void simpleBiimpliesTest() {
		val result = parseHelper.parse('''
			True <-> False
		''').content.get(0)
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			Biimplies: {
				val left = result.left
				switch left{
					Primitive: 
						Assertions.assertEquals("True", left.name)
					default:
						Assertions.fail('''Unexpected left Node of type «result.class»''')
				}
				val right = result.right
				switch right{
					Primitive: 
						Assertions.assertEquals("False", right.name)
					default:
						Assertions.fail('''Unexpected right Node of type «result.class»''')
				}
			}
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	@Test
	def void simpleConjunctionTest() {
		val result = parseHelper.parse('''
			Aa and Bb
		''').content.get(0)
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			Conjunction: {
				val left = result.left
				switch left{
					Primitive: 
						Assertions.assertEquals("Aa", left.name)
					default:
						Assertions.fail('''Unexpected left Node of type «result.class»''')
				}
				val right = result.right
				switch right{
					Primitive: 
						Assertions.assertEquals("Bb", right.name)
					default:
						Assertions.fail('''Unexpected right Node of type «result.class»''')
				}
			}
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	@Test
	def void simpleDisjunctionTest() {
		val result = parseHelper.parse('''
			Aa or Bb
		''').content.get(0)
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			DisjunctionImpl: {
				val left = result.left
				switch left{
					Primitive: 
						Assertions.assertEquals("Aa", left.name)
					default:
						Assertions.fail('''Unexpected left Node of type «result.class»''')
				}
				val right = result.right
				switch right{
					Primitive: 
						Assertions.assertEquals("Bb", right.name)
					default:
						Assertions.fail('''Unexpected right Node of type «result.class»''')
				}
			}
			Disjunction: {}
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	@Test
	def void complexeFormulaTest() {
		val result = parseHelper.parse('''
			⟙ ∨ not ⟘ ∨ ( Aa ∧ ⟙ ) ∨ ⟘ ↔ Bb ↓ Aa
		''').content.get(0)
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		switch result{
			Biimplies: {
			}
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	static class RepBinary<T extends Class<? extends EObject>>{
		T op
		String left
		String right
		new(T op, String left, String right){
			this.op = op
			this.left = left
			this.right = right
		}
	}
	
	def <T extends Class<? extends EObject>>f(CharSequence s, RepBinary<T> v){
		val result = parseHelper.parse(s).content.get(0)
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		Assertions.assertEquals(v.op.simpleName+"Impl", result.class.simpleName)
		switch result{
			Binary: {
				val left = result.left
				switch left{
					Primitive: 
						Assertions.assertEquals(v.left, left.name)
					default:
						Assertions.fail('''Unexpected left Node of type «result.class»''')
				}
				val right = result.right
				switch right{
					Primitive: 
						Assertions.assertEquals(v.right, right.name)
					default:
						Assertions.fail('''Unexpected right Node of type «result.class»''')
				}
			}
			default:
				Assertions.fail('''Unexpected Node of type «result.class»''')
		}
	}
	
	@Test
	def void binaryTest() {
		val list = #{'''A or B'''  -> new RepBinary(Disjunction, "A", "B"),
		             '''A nor B''' -> new RepBinary(NegativeDisjunction, "A", "B"),
		             '''A and B''' -> new RepBinary(Conjunction, "A", "B"),
		             '''A nand B'''-> new RepBinary(NegativeConjunction, "A", "B"),
		             '''A xor B''' -> new RepBinary(ExclusiveDisjunction, "A", "B")}
		list.forEach[p1, p2| f(p1,p2)]
	}

}
