package sat.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import sat.services.PropositionalLogicLanguageGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPropositionalLogicLanguageParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_WS", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_ANY_OTHER", "'<->'", "'\\u2194'", "'-->'", "'\\u27F6'", "'xor'", "'\\u2A02'", "'nor'", "'\\u2193'", "'or'", "'\\u2228'", "'nand'", "'\\u2191'", "'and'", "'\\u2227'", "'not'", "'\\u00AC'", "'true'", "'\\u27D9'", "'false'", "'\\u27D8'", "'using'", "'from'", "'('", "')'"
    };
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPropositionalLogicLanguageParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPropositionalLogicLanguageParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPropositionalLogicLanguageParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPropositionalLogicLanguage.g"; }


    	private PropositionalLogicLanguageGrammarAccess grammarAccess;

    	public void setGrammarAccess(PropositionalLogicLanguageGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleFile"
    // InternalPropositionalLogicLanguage.g:53:1: entryRuleFile : ruleFile EOF ;
    public final void entryRuleFile() throws RecognitionException {
         
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_ML_COMMENT", "RULE_SL_COMMENT");

        try {
            // InternalPropositionalLogicLanguage.g:57:1: ( ruleFile EOF )
            // InternalPropositionalLogicLanguage.g:58:1: ruleFile EOF
            {
             before(grammarAccess.getFileRule()); 
            pushFollow(FOLLOW_1);
            ruleFile();

            state._fsp--;

             after(grammarAccess.getFileRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "entryRuleFile"


    // $ANTLR start "ruleFile"
    // InternalPropositionalLogicLanguage.g:68:1: ruleFile : ( ( rule__File__Group__0 ) ) ;
    public final void ruleFile() throws RecognitionException {

        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_ML_COMMENT", "RULE_SL_COMMENT");
        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:73:2: ( ( ( rule__File__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:74:2: ( ( rule__File__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:74:2: ( ( rule__File__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:75:3: ( rule__File__Group__0 )
            {
             before(grammarAccess.getFileAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:76:3: ( rule__File__Group__0 )
            // InternalPropositionalLogicLanguage.g:76:4: rule__File__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__File__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);
            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "ruleFile"


    // $ANTLR start "entryRuleUsing"
    // InternalPropositionalLogicLanguage.g:86:1: entryRuleUsing : ruleUsing EOF ;
    public final void entryRuleUsing() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:87:1: ( ruleUsing EOF )
            // InternalPropositionalLogicLanguage.g:88:1: ruleUsing EOF
            {
             before(grammarAccess.getUsingRule()); 
            pushFollow(FOLLOW_1);
            ruleUsing();

            state._fsp--;

             after(grammarAccess.getUsingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUsing"


    // $ANTLR start "ruleUsing"
    // InternalPropositionalLogicLanguage.g:95:1: ruleUsing : ( ( rule__Using__Alternatives ) ) ;
    public final void ruleUsing() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:99:2: ( ( ( rule__Using__Alternatives ) ) )
            // InternalPropositionalLogicLanguage.g:100:2: ( ( rule__Using__Alternatives ) )
            {
            // InternalPropositionalLogicLanguage.g:100:2: ( ( rule__Using__Alternatives ) )
            // InternalPropositionalLogicLanguage.g:101:3: ( rule__Using__Alternatives )
            {
             before(grammarAccess.getUsingAccess().getAlternatives()); 
            // InternalPropositionalLogicLanguage.g:102:3: ( rule__Using__Alternatives )
            // InternalPropositionalLogicLanguage.g:102:4: rule__Using__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Using__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUsingAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUsing"


    // $ANTLR start "entryRuleBiimplies"
    // InternalPropositionalLogicLanguage.g:111:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
         
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS");

        try {
            // InternalPropositionalLogicLanguage.g:115:1: ( ruleBiimplies EOF )
            // InternalPropositionalLogicLanguage.g:116:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalPropositionalLogicLanguage.g:126:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS");
        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:131:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:132:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:132:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:133:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:134:3: ( rule__Biimplies__Group__0 )
            // InternalPropositionalLogicLanguage.g:134:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);
            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalPropositionalLogicLanguage.g:144:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:145:1: ( ruleImplies EOF )
            // InternalPropositionalLogicLanguage.g:146:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalPropositionalLogicLanguage.g:153:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:157:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:158:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:158:2: ( ( rule__Implies__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:159:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:160:3: ( rule__Implies__Group__0 )
            // InternalPropositionalLogicLanguage.g:160:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExclusiveDisjunction"
    // InternalPropositionalLogicLanguage.g:169:1: entryRuleExclusiveDisjunction : ruleExclusiveDisjunction EOF ;
    public final void entryRuleExclusiveDisjunction() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:170:1: ( ruleExclusiveDisjunction EOF )
            // InternalPropositionalLogicLanguage.g:171:1: ruleExclusiveDisjunction EOF
            {
             before(grammarAccess.getExclusiveDisjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleExclusiveDisjunction();

            state._fsp--;

             after(grammarAccess.getExclusiveDisjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExclusiveDisjunction"


    // $ANTLR start "ruleExclusiveDisjunction"
    // InternalPropositionalLogicLanguage.g:178:1: ruleExclusiveDisjunction : ( ( rule__ExclusiveDisjunction__Group__0 ) ) ;
    public final void ruleExclusiveDisjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:182:2: ( ( ( rule__ExclusiveDisjunction__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:183:2: ( ( rule__ExclusiveDisjunction__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:183:2: ( ( rule__ExclusiveDisjunction__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:184:3: ( rule__ExclusiveDisjunction__Group__0 )
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:185:3: ( rule__ExclusiveDisjunction__Group__0 )
            // InternalPropositionalLogicLanguage.g:185:4: rule__ExclusiveDisjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExclusiveDisjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExclusiveDisjunction"


    // $ANTLR start "entryRuleNegativeDisjunction"
    // InternalPropositionalLogicLanguage.g:194:1: entryRuleNegativeDisjunction : ruleNegativeDisjunction EOF ;
    public final void entryRuleNegativeDisjunction() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:195:1: ( ruleNegativeDisjunction EOF )
            // InternalPropositionalLogicLanguage.g:196:1: ruleNegativeDisjunction EOF
            {
             before(grammarAccess.getNegativeDisjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleNegativeDisjunction();

            state._fsp--;

             after(grammarAccess.getNegativeDisjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNegativeDisjunction"


    // $ANTLR start "ruleNegativeDisjunction"
    // InternalPropositionalLogicLanguage.g:203:1: ruleNegativeDisjunction : ( ( rule__NegativeDisjunction__Group__0 ) ) ;
    public final void ruleNegativeDisjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:207:2: ( ( ( rule__NegativeDisjunction__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:208:2: ( ( rule__NegativeDisjunction__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:208:2: ( ( rule__NegativeDisjunction__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:209:3: ( rule__NegativeDisjunction__Group__0 )
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:210:3: ( rule__NegativeDisjunction__Group__0 )
            // InternalPropositionalLogicLanguage.g:210:4: rule__NegativeDisjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNegativeDisjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNegativeDisjunction"


    // $ANTLR start "entryRuleDisjunction"
    // InternalPropositionalLogicLanguage.g:219:1: entryRuleDisjunction : ruleDisjunction EOF ;
    public final void entryRuleDisjunction() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:220:1: ( ruleDisjunction EOF )
            // InternalPropositionalLogicLanguage.g:221:1: ruleDisjunction EOF
            {
             before(grammarAccess.getDisjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleDisjunction();

            state._fsp--;

             after(grammarAccess.getDisjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDisjunction"


    // $ANTLR start "ruleDisjunction"
    // InternalPropositionalLogicLanguage.g:228:1: ruleDisjunction : ( ( rule__Disjunction__Group__0 ) ) ;
    public final void ruleDisjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:232:2: ( ( ( rule__Disjunction__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:233:2: ( ( rule__Disjunction__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:233:2: ( ( rule__Disjunction__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:234:3: ( rule__Disjunction__Group__0 )
            {
             before(grammarAccess.getDisjunctionAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:235:3: ( rule__Disjunction__Group__0 )
            // InternalPropositionalLogicLanguage.g:235:4: rule__Disjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Disjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDisjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisjunction"


    // $ANTLR start "entryRuleNegativeConjunction"
    // InternalPropositionalLogicLanguage.g:244:1: entryRuleNegativeConjunction : ruleNegativeConjunction EOF ;
    public final void entryRuleNegativeConjunction() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:245:1: ( ruleNegativeConjunction EOF )
            // InternalPropositionalLogicLanguage.g:246:1: ruleNegativeConjunction EOF
            {
             before(grammarAccess.getNegativeConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleNegativeConjunction();

            state._fsp--;

             after(grammarAccess.getNegativeConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNegativeConjunction"


    // $ANTLR start "ruleNegativeConjunction"
    // InternalPropositionalLogicLanguage.g:253:1: ruleNegativeConjunction : ( ( rule__NegativeConjunction__Group__0 ) ) ;
    public final void ruleNegativeConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:257:2: ( ( ( rule__NegativeConjunction__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:258:2: ( ( rule__NegativeConjunction__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:258:2: ( ( rule__NegativeConjunction__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:259:3: ( rule__NegativeConjunction__Group__0 )
            {
             before(grammarAccess.getNegativeConjunctionAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:260:3: ( rule__NegativeConjunction__Group__0 )
            // InternalPropositionalLogicLanguage.g:260:4: rule__NegativeConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNegativeConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNegativeConjunction"


    // $ANTLR start "entryRuleConjunction"
    // InternalPropositionalLogicLanguage.g:269:1: entryRuleConjunction : ruleConjunction EOF ;
    public final void entryRuleConjunction() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:270:1: ( ruleConjunction EOF )
            // InternalPropositionalLogicLanguage.g:271:1: ruleConjunction EOF
            {
             before(grammarAccess.getConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleConjunction();

            state._fsp--;

             after(grammarAccess.getConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConjunction"


    // $ANTLR start "ruleConjunction"
    // InternalPropositionalLogicLanguage.g:278:1: ruleConjunction : ( ( rule__Conjunction__Group__0 ) ) ;
    public final void ruleConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:282:2: ( ( ( rule__Conjunction__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:283:2: ( ( rule__Conjunction__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:283:2: ( ( rule__Conjunction__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:284:3: ( rule__Conjunction__Group__0 )
            {
             before(grammarAccess.getConjunctionAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:285:3: ( rule__Conjunction__Group__0 )
            // InternalPropositionalLogicLanguage.g:285:4: rule__Conjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Conjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConjunction"


    // $ANTLR start "entryRuleProposition"
    // InternalPropositionalLogicLanguage.g:294:1: entryRuleProposition : ruleProposition EOF ;
    public final void entryRuleProposition() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:295:1: ( ruleProposition EOF )
            // InternalPropositionalLogicLanguage.g:296:1: ruleProposition EOF
            {
             before(grammarAccess.getPropositionRule()); 
            pushFollow(FOLLOW_1);
            ruleProposition();

            state._fsp--;

             after(grammarAccess.getPropositionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProposition"


    // $ANTLR start "ruleProposition"
    // InternalPropositionalLogicLanguage.g:303:1: ruleProposition : ( ( rule__Proposition__Alternatives ) ) ;
    public final void ruleProposition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:307:2: ( ( ( rule__Proposition__Alternatives ) ) )
            // InternalPropositionalLogicLanguage.g:308:2: ( ( rule__Proposition__Alternatives ) )
            {
            // InternalPropositionalLogicLanguage.g:308:2: ( ( rule__Proposition__Alternatives ) )
            // InternalPropositionalLogicLanguage.g:309:3: ( rule__Proposition__Alternatives )
            {
             before(grammarAccess.getPropositionAccess().getAlternatives()); 
            // InternalPropositionalLogicLanguage.g:310:3: ( rule__Proposition__Alternatives )
            // InternalPropositionalLogicLanguage.g:310:4: rule__Proposition__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Proposition__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPropositionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProposition"


    // $ANTLR start "entryRuleNegation"
    // InternalPropositionalLogicLanguage.g:319:1: entryRuleNegation : ruleNegation EOF ;
    public final void entryRuleNegation() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:320:1: ( ruleNegation EOF )
            // InternalPropositionalLogicLanguage.g:321:1: ruleNegation EOF
            {
             before(grammarAccess.getNegationRule()); 
            pushFollow(FOLLOW_1);
            ruleNegation();

            state._fsp--;

             after(grammarAccess.getNegationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNegation"


    // $ANTLR start "ruleNegation"
    // InternalPropositionalLogicLanguage.g:328:1: ruleNegation : ( ( rule__Negation__Group__0 ) ) ;
    public final void ruleNegation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:332:2: ( ( ( rule__Negation__Group__0 ) ) )
            // InternalPropositionalLogicLanguage.g:333:2: ( ( rule__Negation__Group__0 ) )
            {
            // InternalPropositionalLogicLanguage.g:333:2: ( ( rule__Negation__Group__0 ) )
            // InternalPropositionalLogicLanguage.g:334:3: ( rule__Negation__Group__0 )
            {
             before(grammarAccess.getNegationAccess().getGroup()); 
            // InternalPropositionalLogicLanguage.g:335:3: ( rule__Negation__Group__0 )
            // InternalPropositionalLogicLanguage.g:335:4: rule__Negation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Negation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNegationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNegation"


    // $ANTLR start "entryRulePrimitive"
    // InternalPropositionalLogicLanguage.g:344:1: entryRulePrimitive : rulePrimitive EOF ;
    public final void entryRulePrimitive() throws RecognitionException {
        try {
            // InternalPropositionalLogicLanguage.g:345:1: ( rulePrimitive EOF )
            // InternalPropositionalLogicLanguage.g:346:1: rulePrimitive EOF
            {
             before(grammarAccess.getPrimitiveRule()); 
            pushFollow(FOLLOW_1);
            rulePrimitive();

            state._fsp--;

             after(grammarAccess.getPrimitiveRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimitive"


    // $ANTLR start "rulePrimitive"
    // InternalPropositionalLogicLanguage.g:353:1: rulePrimitive : ( ( rule__Primitive__Alternatives ) ) ;
    public final void rulePrimitive() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:357:2: ( ( ( rule__Primitive__Alternatives ) ) )
            // InternalPropositionalLogicLanguage.g:358:2: ( ( rule__Primitive__Alternatives ) )
            {
            // InternalPropositionalLogicLanguage.g:358:2: ( ( rule__Primitive__Alternatives ) )
            // InternalPropositionalLogicLanguage.g:359:3: ( rule__Primitive__Alternatives )
            {
             before(grammarAccess.getPrimitiveAccess().getAlternatives()); 
            // InternalPropositionalLogicLanguage.g:360:3: ( rule__Primitive__Alternatives )
            // InternalPropositionalLogicLanguage.g:360:4: rule__Primitive__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primitive__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimitive"


    // $ANTLR start "rule__Using__Alternatives"
    // InternalPropositionalLogicLanguage.g:368:1: rule__Using__Alternatives : ( ( ( rule__Using__Group_0__0 ) ) | ( ( rule__Using__Group_1__0 ) ) );
    public final void rule__Using__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:372:1: ( ( ( rule__Using__Group_0__0 ) ) | ( ( rule__Using__Group_1__0 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==31) ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==RULE_WS) ) {
                    int LA1_2 = input.LA(3);

                    if ( (LA1_2==RULE_ID) ) {
                        int LA1_3 = input.LA(4);

                        if ( (LA1_3==RULE_WS) ) {
                            int LA1_4 = input.LA(5);

                            if ( (LA1_4==EOF||LA1_4==RULE_ID||(LA1_4>=25 && LA1_4<=31)||LA1_4==33) ) {
                                alt1=2;
                            }
                            else if ( (LA1_4==32) ) {
                                alt1=1;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 1, 4, input);

                                throw nvae;
                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 1, 3, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:373:2: ( ( rule__Using__Group_0__0 ) )
                    {
                    // InternalPropositionalLogicLanguage.g:373:2: ( ( rule__Using__Group_0__0 ) )
                    // InternalPropositionalLogicLanguage.g:374:3: ( rule__Using__Group_0__0 )
                    {
                     before(grammarAccess.getUsingAccess().getGroup_0()); 
                    // InternalPropositionalLogicLanguage.g:375:3: ( rule__Using__Group_0__0 )
                    // InternalPropositionalLogicLanguage.g:375:4: rule__Using__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Using__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUsingAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:379:2: ( ( rule__Using__Group_1__0 ) )
                    {
                    // InternalPropositionalLogicLanguage.g:379:2: ( ( rule__Using__Group_1__0 ) )
                    // InternalPropositionalLogicLanguage.g:380:3: ( rule__Using__Group_1__0 )
                    {
                     before(grammarAccess.getUsingAccess().getGroup_1()); 
                    // InternalPropositionalLogicLanguage.g:381:3: ( rule__Using__Group_1__0 )
                    // InternalPropositionalLogicLanguage.g:381:4: rule__Using__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Using__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUsingAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Alternatives"


    // $ANTLR start "rule__Biimplies__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:389:1: rule__Biimplies__Alternatives_1_1 : ( ( '<->' ) | ( '\\u2194' ) );
    public final void rule__Biimplies__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:393:1: ( ( '<->' ) | ( '\\u2194' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:394:2: ( '<->' )
                    {
                    // InternalPropositionalLogicLanguage.g:394:2: ( '<->' )
                    // InternalPropositionalLogicLanguage.g:395:3: '<->'
                    {
                     before(grammarAccess.getBiimpliesAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getBiimpliesAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:400:2: ( '\\u2194' )
                    {
                    // InternalPropositionalLogicLanguage.g:400:2: ( '\\u2194' )
                    // InternalPropositionalLogicLanguage.g:401:3: '\\u2194'
                    {
                     before(grammarAccess.getBiimpliesAccess().getLeftRightArrowKeyword_1_1_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getBiimpliesAccess().getLeftRightArrowKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Alternatives_1_1"


    // $ANTLR start "rule__Implies__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:410:1: rule__Implies__Alternatives_1_1 : ( ( '-->' ) | ( '\\u27F6' ) );
    public final void rule__Implies__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:414:1: ( ( '-->' ) | ( '\\u27F6' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:415:2: ( '-->' )
                    {
                    // InternalPropositionalLogicLanguage.g:415:2: ( '-->' )
                    // InternalPropositionalLogicLanguage.g:416:3: '-->'
                    {
                     before(grammarAccess.getImpliesAccess().getHyphenMinusHyphenMinusGreaterThanSignKeyword_1_1_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getImpliesAccess().getHyphenMinusHyphenMinusGreaterThanSignKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:421:2: ( '\\u27F6' )
                    {
                    // InternalPropositionalLogicLanguage.g:421:2: ( '\\u27F6' )
                    // InternalPropositionalLogicLanguage.g:422:3: '\\u27F6'
                    {
                     before(grammarAccess.getImpliesAccess().getLongRightwardsArrowKeyword_1_1_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getImpliesAccess().getLongRightwardsArrowKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Alternatives_1_1"


    // $ANTLR start "rule__ExclusiveDisjunction__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:431:1: rule__ExclusiveDisjunction__Alternatives_1_1 : ( ( 'xor' ) | ( '\\u2A02' ) );
    public final void rule__ExclusiveDisjunction__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:435:1: ( ( 'xor' ) | ( '\\u2A02' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            else if ( (LA4_0==16) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:436:2: ( 'xor' )
                    {
                    // InternalPropositionalLogicLanguage.g:436:2: ( 'xor' )
                    // InternalPropositionalLogicLanguage.g:437:3: 'xor'
                    {
                     before(grammarAccess.getExclusiveDisjunctionAccess().getXorKeyword_1_1_0()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getExclusiveDisjunctionAccess().getXorKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:442:2: ( '\\u2A02' )
                    {
                    // InternalPropositionalLogicLanguage.g:442:2: ( '\\u2A02' )
                    // InternalPropositionalLogicLanguage.g:443:3: '\\u2A02'
                    {
                     before(grammarAccess.getExclusiveDisjunctionAccess().getNAryCircledTimesOperatorKeyword_1_1_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getExclusiveDisjunctionAccess().getNAryCircledTimesOperatorKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Alternatives_1_1"


    // $ANTLR start "rule__NegativeDisjunction__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:452:1: rule__NegativeDisjunction__Alternatives_1_1 : ( ( 'nor' ) | ( '\\u2193' ) );
    public final void rule__NegativeDisjunction__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:456:1: ( ( 'nor' ) | ( '\\u2193' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            else if ( (LA5_0==18) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:457:2: ( 'nor' )
                    {
                    // InternalPropositionalLogicLanguage.g:457:2: ( 'nor' )
                    // InternalPropositionalLogicLanguage.g:458:3: 'nor'
                    {
                     before(grammarAccess.getNegativeDisjunctionAccess().getNorKeyword_1_1_0()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getNegativeDisjunctionAccess().getNorKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:463:2: ( '\\u2193' )
                    {
                    // InternalPropositionalLogicLanguage.g:463:2: ( '\\u2193' )
                    // InternalPropositionalLogicLanguage.g:464:3: '\\u2193'
                    {
                     before(grammarAccess.getNegativeDisjunctionAccess().getDownwardsArrowKeyword_1_1_1()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getNegativeDisjunctionAccess().getDownwardsArrowKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Alternatives_1_1"


    // $ANTLR start "rule__Disjunction__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:473:1: rule__Disjunction__Alternatives_1_1 : ( ( 'or' ) | ( '\\u2228' ) );
    public final void rule__Disjunction__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:477:1: ( ( 'or' ) | ( '\\u2228' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            else if ( (LA6_0==20) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:478:2: ( 'or' )
                    {
                    // InternalPropositionalLogicLanguage.g:478:2: ( 'or' )
                    // InternalPropositionalLogicLanguage.g:479:3: 'or'
                    {
                     before(grammarAccess.getDisjunctionAccess().getOrKeyword_1_1_0()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getDisjunctionAccess().getOrKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:484:2: ( '\\u2228' )
                    {
                    // InternalPropositionalLogicLanguage.g:484:2: ( '\\u2228' )
                    // InternalPropositionalLogicLanguage.g:485:3: '\\u2228'
                    {
                     before(grammarAccess.getDisjunctionAccess().getLogicalOrKeyword_1_1_1()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getDisjunctionAccess().getLogicalOrKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Alternatives_1_1"


    // $ANTLR start "rule__NegativeConjunction__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:494:1: rule__NegativeConjunction__Alternatives_1_1 : ( ( 'nand' ) | ( '\\u2191' ) );
    public final void rule__NegativeConjunction__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:498:1: ( ( 'nand' ) | ( '\\u2191' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            else if ( (LA7_0==22) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:499:2: ( 'nand' )
                    {
                    // InternalPropositionalLogicLanguage.g:499:2: ( 'nand' )
                    // InternalPropositionalLogicLanguage.g:500:3: 'nand'
                    {
                     before(grammarAccess.getNegativeConjunctionAccess().getNandKeyword_1_1_0()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getNegativeConjunctionAccess().getNandKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:505:2: ( '\\u2191' )
                    {
                    // InternalPropositionalLogicLanguage.g:505:2: ( '\\u2191' )
                    // InternalPropositionalLogicLanguage.g:506:3: '\\u2191'
                    {
                     before(grammarAccess.getNegativeConjunctionAccess().getUpwardsArrowKeyword_1_1_1()); 
                    match(input,22,FOLLOW_2); 
                     after(grammarAccess.getNegativeConjunctionAccess().getUpwardsArrowKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Alternatives_1_1"


    // $ANTLR start "rule__Conjunction__Alternatives_1_1"
    // InternalPropositionalLogicLanguage.g:515:1: rule__Conjunction__Alternatives_1_1 : ( ( 'and' ) | ( '\\u2227' ) );
    public final void rule__Conjunction__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:519:1: ( ( 'and' ) | ( '\\u2227' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==23) ) {
                alt8=1;
            }
            else if ( (LA8_0==24) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:520:2: ( 'and' )
                    {
                    // InternalPropositionalLogicLanguage.g:520:2: ( 'and' )
                    // InternalPropositionalLogicLanguage.g:521:3: 'and'
                    {
                     before(grammarAccess.getConjunctionAccess().getAndKeyword_1_1_0()); 
                    match(input,23,FOLLOW_2); 
                     after(grammarAccess.getConjunctionAccess().getAndKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:526:2: ( '\\u2227' )
                    {
                    // InternalPropositionalLogicLanguage.g:526:2: ( '\\u2227' )
                    // InternalPropositionalLogicLanguage.g:527:3: '\\u2227'
                    {
                     before(grammarAccess.getConjunctionAccess().getLogicalAndKeyword_1_1_1()); 
                    match(input,24,FOLLOW_2); 
                     after(grammarAccess.getConjunctionAccess().getLogicalAndKeyword_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Alternatives_1_1"


    // $ANTLR start "rule__Proposition__Alternatives"
    // InternalPropositionalLogicLanguage.g:536:1: rule__Proposition__Alternatives : ( ( ( rule__Proposition__Group_0__0 ) ) | ( ruleNegation ) | ( rulePrimitive ) );
    public final void rule__Proposition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:540:1: ( ( ( rule__Proposition__Group_0__0 ) ) | ( ruleNegation ) | ( rulePrimitive ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt9=1;
                }
                break;
            case 25:
            case 26:
                {
                alt9=2;
                }
                break;
            case RULE_ID:
            case 27:
            case 28:
            case 29:
            case 30:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:541:2: ( ( rule__Proposition__Group_0__0 ) )
                    {
                    // InternalPropositionalLogicLanguage.g:541:2: ( ( rule__Proposition__Group_0__0 ) )
                    // InternalPropositionalLogicLanguage.g:542:3: ( rule__Proposition__Group_0__0 )
                    {
                     before(grammarAccess.getPropositionAccess().getGroup_0()); 
                    // InternalPropositionalLogicLanguage.g:543:3: ( rule__Proposition__Group_0__0 )
                    // InternalPropositionalLogicLanguage.g:543:4: rule__Proposition__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Proposition__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPropositionAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:547:2: ( ruleNegation )
                    {
                    // InternalPropositionalLogicLanguage.g:547:2: ( ruleNegation )
                    // InternalPropositionalLogicLanguage.g:548:3: ruleNegation
                    {
                     before(grammarAccess.getPropositionAccess().getNegationParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleNegation();

                    state._fsp--;

                     after(grammarAccess.getPropositionAccess().getNegationParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPropositionalLogicLanguage.g:553:2: ( rulePrimitive )
                    {
                    // InternalPropositionalLogicLanguage.g:553:2: ( rulePrimitive )
                    // InternalPropositionalLogicLanguage.g:554:3: rulePrimitive
                    {
                     before(grammarAccess.getPropositionAccess().getPrimitiveParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    rulePrimitive();

                    state._fsp--;

                     after(grammarAccess.getPropositionAccess().getPrimitiveParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Alternatives"


    // $ANTLR start "rule__Negation__Alternatives_0"
    // InternalPropositionalLogicLanguage.g:563:1: rule__Negation__Alternatives_0 : ( ( 'not' ) | ( '\\u00AC' ) );
    public final void rule__Negation__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:567:1: ( ( 'not' ) | ( '\\u00AC' ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==25) ) {
                alt10=1;
            }
            else if ( (LA10_0==26) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:568:2: ( 'not' )
                    {
                    // InternalPropositionalLogicLanguage.g:568:2: ( 'not' )
                    // InternalPropositionalLogicLanguage.g:569:3: 'not'
                    {
                     before(grammarAccess.getNegationAccess().getNotKeyword_0_0()); 
                    match(input,25,FOLLOW_2); 
                     after(grammarAccess.getNegationAccess().getNotKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:574:2: ( '\\u00AC' )
                    {
                    // InternalPropositionalLogicLanguage.g:574:2: ( '\\u00AC' )
                    // InternalPropositionalLogicLanguage.g:575:3: '\\u00AC'
                    {
                     before(grammarAccess.getNegationAccess().getNotSignKeyword_0_1()); 
                    match(input,26,FOLLOW_2); 
                     after(grammarAccess.getNegationAccess().getNotSignKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Negation__Alternatives_0"


    // $ANTLR start "rule__Primitive__Alternatives"
    // InternalPropositionalLogicLanguage.g:584:1: rule__Primitive__Alternatives : ( ( ( rule__Primitive__NameAssignment_0 ) ) | ( ( rule__Primitive__NameAssignment_1 ) ) | ( ( rule__Primitive__NameAssignment_2 ) ) );
    public final void rule__Primitive__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:588:1: ( ( ( rule__Primitive__NameAssignment_0 ) ) | ( ( rule__Primitive__NameAssignment_1 ) ) | ( ( rule__Primitive__NameAssignment_2 ) ) )
            int alt11=3;
            switch ( input.LA(1) ) {
            case 27:
            case 28:
                {
                alt11=1;
                }
                break;
            case 29:
            case 30:
                {
                alt11=2;
                }
                break;
            case RULE_ID:
                {
                alt11=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:589:2: ( ( rule__Primitive__NameAssignment_0 ) )
                    {
                    // InternalPropositionalLogicLanguage.g:589:2: ( ( rule__Primitive__NameAssignment_0 ) )
                    // InternalPropositionalLogicLanguage.g:590:3: ( rule__Primitive__NameAssignment_0 )
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameAssignment_0()); 
                    // InternalPropositionalLogicLanguage.g:591:3: ( rule__Primitive__NameAssignment_0 )
                    // InternalPropositionalLogicLanguage.g:591:4: rule__Primitive__NameAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primitive__NameAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimitiveAccess().getNameAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:595:2: ( ( rule__Primitive__NameAssignment_1 ) )
                    {
                    // InternalPropositionalLogicLanguage.g:595:2: ( ( rule__Primitive__NameAssignment_1 ) )
                    // InternalPropositionalLogicLanguage.g:596:3: ( rule__Primitive__NameAssignment_1 )
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameAssignment_1()); 
                    // InternalPropositionalLogicLanguage.g:597:3: ( rule__Primitive__NameAssignment_1 )
                    // InternalPropositionalLogicLanguage.g:597:4: rule__Primitive__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primitive__NameAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimitiveAccess().getNameAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPropositionalLogicLanguage.g:601:2: ( ( rule__Primitive__NameAssignment_2 ) )
                    {
                    // InternalPropositionalLogicLanguage.g:601:2: ( ( rule__Primitive__NameAssignment_2 ) )
                    // InternalPropositionalLogicLanguage.g:602:3: ( rule__Primitive__NameAssignment_2 )
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameAssignment_2()); 
                    // InternalPropositionalLogicLanguage.g:603:3: ( rule__Primitive__NameAssignment_2 )
                    // InternalPropositionalLogicLanguage.g:603:4: rule__Primitive__NameAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primitive__NameAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimitiveAccess().getNameAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primitive__Alternatives"


    // $ANTLR start "rule__Primitive__NameAlternatives_0_0"
    // InternalPropositionalLogicLanguage.g:611:1: rule__Primitive__NameAlternatives_0_0 : ( ( 'true' ) | ( '\\u27D9' ) );
    public final void rule__Primitive__NameAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:615:1: ( ( 'true' ) | ( '\\u27D9' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==27) ) {
                alt12=1;
            }
            else if ( (LA12_0==28) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:616:2: ( 'true' )
                    {
                    // InternalPropositionalLogicLanguage.g:616:2: ( 'true' )
                    // InternalPropositionalLogicLanguage.g:617:3: 'true'
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameTrueKeyword_0_0_0()); 
                    match(input,27,FOLLOW_2); 
                     after(grammarAccess.getPrimitiveAccess().getNameTrueKeyword_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:622:2: ( '\\u27D9' )
                    {
                    // InternalPropositionalLogicLanguage.g:622:2: ( '\\u27D9' )
                    // InternalPropositionalLogicLanguage.g:623:3: '\\u27D9'
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameLargeDownTackKeyword_0_0_1()); 
                    match(input,28,FOLLOW_2); 
                     after(grammarAccess.getPrimitiveAccess().getNameLargeDownTackKeyword_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primitive__NameAlternatives_0_0"


    // $ANTLR start "rule__Primitive__NameAlternatives_1_0"
    // InternalPropositionalLogicLanguage.g:632:1: rule__Primitive__NameAlternatives_1_0 : ( ( 'false' ) | ( '\\u27D8' ) );
    public final void rule__Primitive__NameAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:636:1: ( ( 'false' ) | ( '\\u27D8' ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==29) ) {
                alt13=1;
            }
            else if ( (LA13_0==30) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:637:2: ( 'false' )
                    {
                    // InternalPropositionalLogicLanguage.g:637:2: ( 'false' )
                    // InternalPropositionalLogicLanguage.g:638:3: 'false'
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameFalseKeyword_1_0_0()); 
                    match(input,29,FOLLOW_2); 
                     after(grammarAccess.getPrimitiveAccess().getNameFalseKeyword_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:643:2: ( '\\u27D8' )
                    {
                    // InternalPropositionalLogicLanguage.g:643:2: ( '\\u27D8' )
                    // InternalPropositionalLogicLanguage.g:644:3: '\\u27D8'
                    {
                     before(grammarAccess.getPrimitiveAccess().getNameLargeUpTackKeyword_1_0_1()); 
                    match(input,30,FOLLOW_2); 
                     after(grammarAccess.getPrimitiveAccess().getNameLargeUpTackKeyword_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primitive__NameAlternatives_1_0"


    // $ANTLR start "rule__File__Group__0"
    // InternalPropositionalLogicLanguage.g:653:1: rule__File__Group__0 : rule__File__Group__0__Impl rule__File__Group__1 ;
    public final void rule__File__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:657:1: ( rule__File__Group__0__Impl rule__File__Group__1 )
            // InternalPropositionalLogicLanguage.g:658:2: rule__File__Group__0__Impl rule__File__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__File__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__0"


    // $ANTLR start "rule__File__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:665:1: rule__File__Group__0__Impl : ( ( rule__File__UsingsAssignment_0 )* ) ;
    public final void rule__File__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:669:1: ( ( ( rule__File__UsingsAssignment_0 )* ) )
            // InternalPropositionalLogicLanguage.g:670:1: ( ( rule__File__UsingsAssignment_0 )* )
            {
            // InternalPropositionalLogicLanguage.g:670:1: ( ( rule__File__UsingsAssignment_0 )* )
            // InternalPropositionalLogicLanguage.g:671:2: ( rule__File__UsingsAssignment_0 )*
            {
             before(grammarAccess.getFileAccess().getUsingsAssignment_0()); 
            // InternalPropositionalLogicLanguage.g:672:2: ( rule__File__UsingsAssignment_0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==31) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:672:3: rule__File__UsingsAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__File__UsingsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getUsingsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__0__Impl"


    // $ANTLR start "rule__File__Group__1"
    // InternalPropositionalLogicLanguage.g:680:1: rule__File__Group__1 : rule__File__Group__1__Impl ;
    public final void rule__File__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:684:1: ( rule__File__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:685:2: rule__File__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__1"


    // $ANTLR start "rule__File__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:691:1: rule__File__Group__1__Impl : ( ( rule__File__ContentAssignment_1 )* ) ;
    public final void rule__File__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:695:1: ( ( ( rule__File__ContentAssignment_1 )* ) )
            // InternalPropositionalLogicLanguage.g:696:1: ( ( rule__File__ContentAssignment_1 )* )
            {
            // InternalPropositionalLogicLanguage.g:696:1: ( ( rule__File__ContentAssignment_1 )* )
            // InternalPropositionalLogicLanguage.g:697:2: ( rule__File__ContentAssignment_1 )*
            {
             before(grammarAccess.getFileAccess().getContentAssignment_1()); 
            // InternalPropositionalLogicLanguage.g:698:2: ( rule__File__ContentAssignment_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID||(LA15_0>=25 && LA15_0<=30)||LA15_0==33) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:698:3: rule__File__ContentAssignment_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__File__ContentAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getContentAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__1__Impl"


    // $ANTLR start "rule__Using__Group_0__0"
    // InternalPropositionalLogicLanguage.g:707:1: rule__Using__Group_0__0 : rule__Using__Group_0__0__Impl rule__Using__Group_0__1 ;
    public final void rule__Using__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:711:1: ( rule__Using__Group_0__0__Impl rule__Using__Group_0__1 )
            // InternalPropositionalLogicLanguage.g:712:2: rule__Using__Group_0__0__Impl rule__Using__Group_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Using__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__0"


    // $ANTLR start "rule__Using__Group_0__0__Impl"
    // InternalPropositionalLogicLanguage.g:719:1: rule__Using__Group_0__0__Impl : ( 'using' ) ;
    public final void rule__Using__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:723:1: ( ( 'using' ) )
            // InternalPropositionalLogicLanguage.g:724:1: ( 'using' )
            {
            // InternalPropositionalLogicLanguage.g:724:1: ( 'using' )
            // InternalPropositionalLogicLanguage.g:725:2: 'using'
            {
             before(grammarAccess.getUsingAccess().getUsingKeyword_0_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getUsingKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__0__Impl"


    // $ANTLR start "rule__Using__Group_0__1"
    // InternalPropositionalLogicLanguage.g:734:1: rule__Using__Group_0__1 : rule__Using__Group_0__1__Impl rule__Using__Group_0__2 ;
    public final void rule__Using__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:738:1: ( rule__Using__Group_0__1__Impl rule__Using__Group_0__2 )
            // InternalPropositionalLogicLanguage.g:739:2: rule__Using__Group_0__1__Impl rule__Using__Group_0__2
            {
            pushFollow(FOLLOW_7);
            rule__Using__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__1"


    // $ANTLR start "rule__Using__Group_0__1__Impl"
    // InternalPropositionalLogicLanguage.g:746:1: rule__Using__Group_0__1__Impl : ( RULE_WS ) ;
    public final void rule__Using__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:750:1: ( ( RULE_WS ) )
            // InternalPropositionalLogicLanguage.g:751:1: ( RULE_WS )
            {
            // InternalPropositionalLogicLanguage.g:751:1: ( RULE_WS )
            // InternalPropositionalLogicLanguage.g:752:2: RULE_WS
            {
             before(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_1()); 
            match(input,RULE_WS,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__1__Impl"


    // $ANTLR start "rule__Using__Group_0__2"
    // InternalPropositionalLogicLanguage.g:761:1: rule__Using__Group_0__2 : rule__Using__Group_0__2__Impl rule__Using__Group_0__3 ;
    public final void rule__Using__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:765:1: ( rule__Using__Group_0__2__Impl rule__Using__Group_0__3 )
            // InternalPropositionalLogicLanguage.g:766:2: rule__Using__Group_0__2__Impl rule__Using__Group_0__3
            {
            pushFollow(FOLLOW_6);
            rule__Using__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__2"


    // $ANTLR start "rule__Using__Group_0__2__Impl"
    // InternalPropositionalLogicLanguage.g:773:1: rule__Using__Group_0__2__Impl : ( ( rule__Using__KindAssignment_0_2 ) ) ;
    public final void rule__Using__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:777:1: ( ( ( rule__Using__KindAssignment_0_2 ) ) )
            // InternalPropositionalLogicLanguage.g:778:1: ( ( rule__Using__KindAssignment_0_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:778:1: ( ( rule__Using__KindAssignment_0_2 ) )
            // InternalPropositionalLogicLanguage.g:779:2: ( rule__Using__KindAssignment_0_2 )
            {
             before(grammarAccess.getUsingAccess().getKindAssignment_0_2()); 
            // InternalPropositionalLogicLanguage.g:780:2: ( rule__Using__KindAssignment_0_2 )
            // InternalPropositionalLogicLanguage.g:780:3: rule__Using__KindAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Using__KindAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getUsingAccess().getKindAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__2__Impl"


    // $ANTLR start "rule__Using__Group_0__3"
    // InternalPropositionalLogicLanguage.g:788:1: rule__Using__Group_0__3 : rule__Using__Group_0__3__Impl rule__Using__Group_0__4 ;
    public final void rule__Using__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:792:1: ( rule__Using__Group_0__3__Impl rule__Using__Group_0__4 )
            // InternalPropositionalLogicLanguage.g:793:2: rule__Using__Group_0__3__Impl rule__Using__Group_0__4
            {
            pushFollow(FOLLOW_8);
            rule__Using__Group_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__3"


    // $ANTLR start "rule__Using__Group_0__3__Impl"
    // InternalPropositionalLogicLanguage.g:800:1: rule__Using__Group_0__3__Impl : ( RULE_WS ) ;
    public final void rule__Using__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:804:1: ( ( RULE_WS ) )
            // InternalPropositionalLogicLanguage.g:805:1: ( RULE_WS )
            {
            // InternalPropositionalLogicLanguage.g:805:1: ( RULE_WS )
            // InternalPropositionalLogicLanguage.g:806:2: RULE_WS
            {
             before(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_3()); 
            match(input,RULE_WS,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__3__Impl"


    // $ANTLR start "rule__Using__Group_0__4"
    // InternalPropositionalLogicLanguage.g:815:1: rule__Using__Group_0__4 : rule__Using__Group_0__4__Impl rule__Using__Group_0__5 ;
    public final void rule__Using__Group_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:819:1: ( rule__Using__Group_0__4__Impl rule__Using__Group_0__5 )
            // InternalPropositionalLogicLanguage.g:820:2: rule__Using__Group_0__4__Impl rule__Using__Group_0__5
            {
            pushFollow(FOLLOW_6);
            rule__Using__Group_0__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__4"


    // $ANTLR start "rule__Using__Group_0__4__Impl"
    // InternalPropositionalLogicLanguage.g:827:1: rule__Using__Group_0__4__Impl : ( 'from' ) ;
    public final void rule__Using__Group_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:831:1: ( ( 'from' ) )
            // InternalPropositionalLogicLanguage.g:832:1: ( 'from' )
            {
            // InternalPropositionalLogicLanguage.g:832:1: ( 'from' )
            // InternalPropositionalLogicLanguage.g:833:2: 'from'
            {
             before(grammarAccess.getUsingAccess().getFromKeyword_0_4()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getFromKeyword_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__4__Impl"


    // $ANTLR start "rule__Using__Group_0__5"
    // InternalPropositionalLogicLanguage.g:842:1: rule__Using__Group_0__5 : rule__Using__Group_0__5__Impl rule__Using__Group_0__6 ;
    public final void rule__Using__Group_0__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:846:1: ( rule__Using__Group_0__5__Impl rule__Using__Group_0__6 )
            // InternalPropositionalLogicLanguage.g:847:2: rule__Using__Group_0__5__Impl rule__Using__Group_0__6
            {
            pushFollow(FOLLOW_7);
            rule__Using__Group_0__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__5"


    // $ANTLR start "rule__Using__Group_0__5__Impl"
    // InternalPropositionalLogicLanguage.g:854:1: rule__Using__Group_0__5__Impl : ( RULE_WS ) ;
    public final void rule__Using__Group_0__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:858:1: ( ( RULE_WS ) )
            // InternalPropositionalLogicLanguage.g:859:1: ( RULE_WS )
            {
            // InternalPropositionalLogicLanguage.g:859:1: ( RULE_WS )
            // InternalPropositionalLogicLanguage.g:860:2: RULE_WS
            {
             before(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_5()); 
            match(input,RULE_WS,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__5__Impl"


    // $ANTLR start "rule__Using__Group_0__6"
    // InternalPropositionalLogicLanguage.g:869:1: rule__Using__Group_0__6 : rule__Using__Group_0__6__Impl rule__Using__Group_0__7 ;
    public final void rule__Using__Group_0__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:873:1: ( rule__Using__Group_0__6__Impl rule__Using__Group_0__7 )
            // InternalPropositionalLogicLanguage.g:874:2: rule__Using__Group_0__6__Impl rule__Using__Group_0__7
            {
            pushFollow(FOLLOW_6);
            rule__Using__Group_0__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_0__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__6"


    // $ANTLR start "rule__Using__Group_0__6__Impl"
    // InternalPropositionalLogicLanguage.g:881:1: rule__Using__Group_0__6__Impl : ( ( rule__Using__NameAssignment_0_6 ) ) ;
    public final void rule__Using__Group_0__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:885:1: ( ( ( rule__Using__NameAssignment_0_6 ) ) )
            // InternalPropositionalLogicLanguage.g:886:1: ( ( rule__Using__NameAssignment_0_6 ) )
            {
            // InternalPropositionalLogicLanguage.g:886:1: ( ( rule__Using__NameAssignment_0_6 ) )
            // InternalPropositionalLogicLanguage.g:887:2: ( rule__Using__NameAssignment_0_6 )
            {
             before(grammarAccess.getUsingAccess().getNameAssignment_0_6()); 
            // InternalPropositionalLogicLanguage.g:888:2: ( rule__Using__NameAssignment_0_6 )
            // InternalPropositionalLogicLanguage.g:888:3: rule__Using__NameAssignment_0_6
            {
            pushFollow(FOLLOW_2);
            rule__Using__NameAssignment_0_6();

            state._fsp--;


            }

             after(grammarAccess.getUsingAccess().getNameAssignment_0_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__6__Impl"


    // $ANTLR start "rule__Using__Group_0__7"
    // InternalPropositionalLogicLanguage.g:896:1: rule__Using__Group_0__7 : rule__Using__Group_0__7__Impl ;
    public final void rule__Using__Group_0__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:900:1: ( rule__Using__Group_0__7__Impl )
            // InternalPropositionalLogicLanguage.g:901:2: rule__Using__Group_0__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Using__Group_0__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__7"


    // $ANTLR start "rule__Using__Group_0__7__Impl"
    // InternalPropositionalLogicLanguage.g:907:1: rule__Using__Group_0__7__Impl : ( RULE_WS ) ;
    public final void rule__Using__Group_0__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:911:1: ( ( RULE_WS ) )
            // InternalPropositionalLogicLanguage.g:912:1: ( RULE_WS )
            {
            // InternalPropositionalLogicLanguage.g:912:1: ( RULE_WS )
            // InternalPropositionalLogicLanguage.g:913:2: RULE_WS
            {
             before(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_7()); 
            match(input,RULE_WS,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_0__7__Impl"


    // $ANTLR start "rule__Using__Group_1__0"
    // InternalPropositionalLogicLanguage.g:923:1: rule__Using__Group_1__0 : rule__Using__Group_1__0__Impl rule__Using__Group_1__1 ;
    public final void rule__Using__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:927:1: ( rule__Using__Group_1__0__Impl rule__Using__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:928:2: rule__Using__Group_1__0__Impl rule__Using__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Using__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__0"


    // $ANTLR start "rule__Using__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:935:1: rule__Using__Group_1__0__Impl : ( 'using' ) ;
    public final void rule__Using__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:939:1: ( ( 'using' ) )
            // InternalPropositionalLogicLanguage.g:940:1: ( 'using' )
            {
            // InternalPropositionalLogicLanguage.g:940:1: ( 'using' )
            // InternalPropositionalLogicLanguage.g:941:2: 'using'
            {
             before(grammarAccess.getUsingAccess().getUsingKeyword_1_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getUsingKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__0__Impl"


    // $ANTLR start "rule__Using__Group_1__1"
    // InternalPropositionalLogicLanguage.g:950:1: rule__Using__Group_1__1 : rule__Using__Group_1__1__Impl rule__Using__Group_1__2 ;
    public final void rule__Using__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:954:1: ( rule__Using__Group_1__1__Impl rule__Using__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:955:2: rule__Using__Group_1__1__Impl rule__Using__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Using__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__1"


    // $ANTLR start "rule__Using__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:962:1: rule__Using__Group_1__1__Impl : ( RULE_WS ) ;
    public final void rule__Using__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:966:1: ( ( RULE_WS ) )
            // InternalPropositionalLogicLanguage.g:967:1: ( RULE_WS )
            {
            // InternalPropositionalLogicLanguage.g:967:1: ( RULE_WS )
            // InternalPropositionalLogicLanguage.g:968:2: RULE_WS
            {
             before(grammarAccess.getUsingAccess().getWSTerminalRuleCall_1_1()); 
            match(input,RULE_WS,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getWSTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__1__Impl"


    // $ANTLR start "rule__Using__Group_1__2"
    // InternalPropositionalLogicLanguage.g:977:1: rule__Using__Group_1__2 : rule__Using__Group_1__2__Impl rule__Using__Group_1__3 ;
    public final void rule__Using__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:981:1: ( rule__Using__Group_1__2__Impl rule__Using__Group_1__3 )
            // InternalPropositionalLogicLanguage.g:982:2: rule__Using__Group_1__2__Impl rule__Using__Group_1__3
            {
            pushFollow(FOLLOW_6);
            rule__Using__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Using__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__2"


    // $ANTLR start "rule__Using__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:989:1: rule__Using__Group_1__2__Impl : ( ( rule__Using__NameAssignment_1_2 ) ) ;
    public final void rule__Using__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:993:1: ( ( ( rule__Using__NameAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:994:1: ( ( rule__Using__NameAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:994:1: ( ( rule__Using__NameAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:995:2: ( rule__Using__NameAssignment_1_2 )
            {
             before(grammarAccess.getUsingAccess().getNameAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:996:2: ( rule__Using__NameAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:996:3: rule__Using__NameAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Using__NameAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getUsingAccess().getNameAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__2__Impl"


    // $ANTLR start "rule__Using__Group_1__3"
    // InternalPropositionalLogicLanguage.g:1004:1: rule__Using__Group_1__3 : rule__Using__Group_1__3__Impl ;
    public final void rule__Using__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1008:1: ( rule__Using__Group_1__3__Impl )
            // InternalPropositionalLogicLanguage.g:1009:2: rule__Using__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Using__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__3"


    // $ANTLR start "rule__Using__Group_1__3__Impl"
    // InternalPropositionalLogicLanguage.g:1015:1: rule__Using__Group_1__3__Impl : ( RULE_WS ) ;
    public final void rule__Using__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1019:1: ( ( RULE_WS ) )
            // InternalPropositionalLogicLanguage.g:1020:1: ( RULE_WS )
            {
            // InternalPropositionalLogicLanguage.g:1020:1: ( RULE_WS )
            // InternalPropositionalLogicLanguage.g:1021:2: RULE_WS
            {
             before(grammarAccess.getUsingAccess().getWSTerminalRuleCall_1_3()); 
            match(input,RULE_WS,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getWSTerminalRuleCall_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__Group_1__3__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalPropositionalLogicLanguage.g:1031:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1035:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalPropositionalLogicLanguage.g:1036:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1043:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1047:1: ( ( ruleImplies ) )
            // InternalPropositionalLogicLanguage.g:1048:1: ( ruleImplies )
            {
            // InternalPropositionalLogicLanguage.g:1048:1: ( ruleImplies )
            // InternalPropositionalLogicLanguage.g:1049:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalPropositionalLogicLanguage.g:1058:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1062:1: ( rule__Biimplies__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1063:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1069:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1073:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1074:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1074:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1075:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1076:2: ( rule__Biimplies__Group_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>=11 && LA16_0<=12)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1076:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1085:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1089:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1090:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1097:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1101:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1102:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1102:1: ( () )
            // InternalPropositionalLogicLanguage.g:1103:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1104:2: ()
            // InternalPropositionalLogicLanguage.g:1104:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1112:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1116:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1117:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1124:1: rule__Biimplies__Group_1__1__Impl : ( ( rule__Biimplies__Alternatives_1_1 ) ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1128:1: ( ( ( rule__Biimplies__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1129:1: ( ( rule__Biimplies__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1129:1: ( ( rule__Biimplies__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1130:2: ( rule__Biimplies__Alternatives_1_1 )
            {
             before(grammarAccess.getBiimpliesAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1131:2: ( rule__Biimplies__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1131:3: rule__Biimplies__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1139:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1143:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1144:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1150:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1154:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1155:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1155:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1156:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1157:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1157:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalPropositionalLogicLanguage.g:1166:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1170:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalPropositionalLogicLanguage.g:1171:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1178:1: rule__Implies__Group__0__Impl : ( ruleExclusiveDisjunction ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1182:1: ( ( ruleExclusiveDisjunction ) )
            // InternalPropositionalLogicLanguage.g:1183:1: ( ruleExclusiveDisjunction )
            {
            // InternalPropositionalLogicLanguage.g:1183:1: ( ruleExclusiveDisjunction )
            // InternalPropositionalLogicLanguage.g:1184:2: ruleExclusiveDisjunction
            {
             before(grammarAccess.getImpliesAccess().getExclusiveDisjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExclusiveDisjunction();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getExclusiveDisjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalPropositionalLogicLanguage.g:1193:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1197:1: ( rule__Implies__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1198:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1204:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1208:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1209:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1209:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1210:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1211:2: ( rule__Implies__Group_1__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=13 && LA17_0<=14)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1211:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1220:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1224:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1225:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1232:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1236:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1237:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1237:1: ( () )
            // InternalPropositionalLogicLanguage.g:1238:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1239:2: ()
            // InternalPropositionalLogicLanguage.g:1239:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1247:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1251:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1252:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1259:1: rule__Implies__Group_1__1__Impl : ( ( rule__Implies__Alternatives_1_1 ) ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1263:1: ( ( ( rule__Implies__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1264:1: ( ( rule__Implies__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1264:1: ( ( rule__Implies__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1265:2: ( rule__Implies__Alternatives_1_1 )
            {
             before(grammarAccess.getImpliesAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1266:2: ( rule__Implies__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1266:3: rule__Implies__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1274:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1278:1: ( rule__Implies__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1279:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1285:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1289:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1290:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1290:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1291:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1292:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1292:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__ExclusiveDisjunction__Group__0"
    // InternalPropositionalLogicLanguage.g:1301:1: rule__ExclusiveDisjunction__Group__0 : rule__ExclusiveDisjunction__Group__0__Impl rule__ExclusiveDisjunction__Group__1 ;
    public final void rule__ExclusiveDisjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1305:1: ( rule__ExclusiveDisjunction__Group__0__Impl rule__ExclusiveDisjunction__Group__1 )
            // InternalPropositionalLogicLanguage.g:1306:2: rule__ExclusiveDisjunction__Group__0__Impl rule__ExclusiveDisjunction__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__ExclusiveDisjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group__0"


    // $ANTLR start "rule__ExclusiveDisjunction__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1313:1: rule__ExclusiveDisjunction__Group__0__Impl : ( ruleNegativeDisjunction ) ;
    public final void rule__ExclusiveDisjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1317:1: ( ( ruleNegativeDisjunction ) )
            // InternalPropositionalLogicLanguage.g:1318:1: ( ruleNegativeDisjunction )
            {
            // InternalPropositionalLogicLanguage.g:1318:1: ( ruleNegativeDisjunction )
            // InternalPropositionalLogicLanguage.g:1319:2: ruleNegativeDisjunction
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getNegativeDisjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNegativeDisjunction();

            state._fsp--;

             after(grammarAccess.getExclusiveDisjunctionAccess().getNegativeDisjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group__0__Impl"


    // $ANTLR start "rule__ExclusiveDisjunction__Group__1"
    // InternalPropositionalLogicLanguage.g:1328:1: rule__ExclusiveDisjunction__Group__1 : rule__ExclusiveDisjunction__Group__1__Impl ;
    public final void rule__ExclusiveDisjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1332:1: ( rule__ExclusiveDisjunction__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1333:2: rule__ExclusiveDisjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group__1"


    // $ANTLR start "rule__ExclusiveDisjunction__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1339:1: rule__ExclusiveDisjunction__Group__1__Impl : ( ( rule__ExclusiveDisjunction__Group_1__0 )* ) ;
    public final void rule__ExclusiveDisjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1343:1: ( ( ( rule__ExclusiveDisjunction__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1344:1: ( ( rule__ExclusiveDisjunction__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1344:1: ( ( rule__ExclusiveDisjunction__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1345:2: ( rule__ExclusiveDisjunction__Group_1__0 )*
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1346:2: ( rule__ExclusiveDisjunction__Group_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=15 && LA18_0<=16)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1346:3: rule__ExclusiveDisjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__ExclusiveDisjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getExclusiveDisjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group__1__Impl"


    // $ANTLR start "rule__ExclusiveDisjunction__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1355:1: rule__ExclusiveDisjunction__Group_1__0 : rule__ExclusiveDisjunction__Group_1__0__Impl rule__ExclusiveDisjunction__Group_1__1 ;
    public final void rule__ExclusiveDisjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1359:1: ( rule__ExclusiveDisjunction__Group_1__0__Impl rule__ExclusiveDisjunction__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1360:2: rule__ExclusiveDisjunction__Group_1__0__Impl rule__ExclusiveDisjunction__Group_1__1
            {
            pushFollow(FOLLOW_13);
            rule__ExclusiveDisjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group_1__0"


    // $ANTLR start "rule__ExclusiveDisjunction__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1367:1: rule__ExclusiveDisjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__ExclusiveDisjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1371:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1372:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1372:1: ( () )
            // InternalPropositionalLogicLanguage.g:1373:2: ()
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getExclusiveDisjunctionLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1374:2: ()
            // InternalPropositionalLogicLanguage.g:1374:3: 
            {
            }

             after(grammarAccess.getExclusiveDisjunctionAccess().getExclusiveDisjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group_1__0__Impl"


    // $ANTLR start "rule__ExclusiveDisjunction__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1382:1: rule__ExclusiveDisjunction__Group_1__1 : rule__ExclusiveDisjunction__Group_1__1__Impl rule__ExclusiveDisjunction__Group_1__2 ;
    public final void rule__ExclusiveDisjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1386:1: ( rule__ExclusiveDisjunction__Group_1__1__Impl rule__ExclusiveDisjunction__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1387:2: rule__ExclusiveDisjunction__Group_1__1__Impl rule__ExclusiveDisjunction__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__ExclusiveDisjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group_1__1"


    // $ANTLR start "rule__ExclusiveDisjunction__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1394:1: rule__ExclusiveDisjunction__Group_1__1__Impl : ( ( rule__ExclusiveDisjunction__Alternatives_1_1 ) ) ;
    public final void rule__ExclusiveDisjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1398:1: ( ( ( rule__ExclusiveDisjunction__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1399:1: ( ( rule__ExclusiveDisjunction__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1399:1: ( ( rule__ExclusiveDisjunction__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1400:2: ( rule__ExclusiveDisjunction__Alternatives_1_1 )
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1401:2: ( rule__ExclusiveDisjunction__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1401:3: rule__ExclusiveDisjunction__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExclusiveDisjunctionAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group_1__1__Impl"


    // $ANTLR start "rule__ExclusiveDisjunction__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1409:1: rule__ExclusiveDisjunction__Group_1__2 : rule__ExclusiveDisjunction__Group_1__2__Impl ;
    public final void rule__ExclusiveDisjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1413:1: ( rule__ExclusiveDisjunction__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1414:2: rule__ExclusiveDisjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group_1__2"


    // $ANTLR start "rule__ExclusiveDisjunction__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1420:1: rule__ExclusiveDisjunction__Group_1__2__Impl : ( ( rule__ExclusiveDisjunction__RightAssignment_1_2 ) ) ;
    public final void rule__ExclusiveDisjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1424:1: ( ( ( rule__ExclusiveDisjunction__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1425:1: ( ( rule__ExclusiveDisjunction__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1425:1: ( ( rule__ExclusiveDisjunction__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1426:2: ( rule__ExclusiveDisjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1427:2: ( rule__ExclusiveDisjunction__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1427:3: rule__ExclusiveDisjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__ExclusiveDisjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExclusiveDisjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__Group_1__2__Impl"


    // $ANTLR start "rule__NegativeDisjunction__Group__0"
    // InternalPropositionalLogicLanguage.g:1436:1: rule__NegativeDisjunction__Group__0 : rule__NegativeDisjunction__Group__0__Impl rule__NegativeDisjunction__Group__1 ;
    public final void rule__NegativeDisjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1440:1: ( rule__NegativeDisjunction__Group__0__Impl rule__NegativeDisjunction__Group__1 )
            // InternalPropositionalLogicLanguage.g:1441:2: rule__NegativeDisjunction__Group__0__Impl rule__NegativeDisjunction__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__NegativeDisjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group__0"


    // $ANTLR start "rule__NegativeDisjunction__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1448:1: rule__NegativeDisjunction__Group__0__Impl : ( ruleDisjunction ) ;
    public final void rule__NegativeDisjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1452:1: ( ( ruleDisjunction ) )
            // InternalPropositionalLogicLanguage.g:1453:1: ( ruleDisjunction )
            {
            // InternalPropositionalLogicLanguage.g:1453:1: ( ruleDisjunction )
            // InternalPropositionalLogicLanguage.g:1454:2: ruleDisjunction
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getDisjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDisjunction();

            state._fsp--;

             after(grammarAccess.getNegativeDisjunctionAccess().getDisjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group__0__Impl"


    // $ANTLR start "rule__NegativeDisjunction__Group__1"
    // InternalPropositionalLogicLanguage.g:1463:1: rule__NegativeDisjunction__Group__1 : rule__NegativeDisjunction__Group__1__Impl ;
    public final void rule__NegativeDisjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1467:1: ( rule__NegativeDisjunction__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1468:2: rule__NegativeDisjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group__1"


    // $ANTLR start "rule__NegativeDisjunction__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1474:1: rule__NegativeDisjunction__Group__1__Impl : ( ( rule__NegativeDisjunction__Group_1__0 )* ) ;
    public final void rule__NegativeDisjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1478:1: ( ( ( rule__NegativeDisjunction__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1479:1: ( ( rule__NegativeDisjunction__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1479:1: ( ( rule__NegativeDisjunction__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1480:2: ( rule__NegativeDisjunction__Group_1__0 )*
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1481:2: ( rule__NegativeDisjunction__Group_1__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=17 && LA19_0<=18)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1481:3: rule__NegativeDisjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__NegativeDisjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getNegativeDisjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group__1__Impl"


    // $ANTLR start "rule__NegativeDisjunction__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1490:1: rule__NegativeDisjunction__Group_1__0 : rule__NegativeDisjunction__Group_1__0__Impl rule__NegativeDisjunction__Group_1__1 ;
    public final void rule__NegativeDisjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1494:1: ( rule__NegativeDisjunction__Group_1__0__Impl rule__NegativeDisjunction__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1495:2: rule__NegativeDisjunction__Group_1__0__Impl rule__NegativeDisjunction__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__NegativeDisjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group_1__0"


    // $ANTLR start "rule__NegativeDisjunction__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1502:1: rule__NegativeDisjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__NegativeDisjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1506:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1507:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1507:1: ( () )
            // InternalPropositionalLogicLanguage.g:1508:2: ()
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getNegativeDisjunctionLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1509:2: ()
            // InternalPropositionalLogicLanguage.g:1509:3: 
            {
            }

             after(grammarAccess.getNegativeDisjunctionAccess().getNegativeDisjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group_1__0__Impl"


    // $ANTLR start "rule__NegativeDisjunction__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1517:1: rule__NegativeDisjunction__Group_1__1 : rule__NegativeDisjunction__Group_1__1__Impl rule__NegativeDisjunction__Group_1__2 ;
    public final void rule__NegativeDisjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1521:1: ( rule__NegativeDisjunction__Group_1__1__Impl rule__NegativeDisjunction__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1522:2: rule__NegativeDisjunction__Group_1__1__Impl rule__NegativeDisjunction__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__NegativeDisjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group_1__1"


    // $ANTLR start "rule__NegativeDisjunction__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1529:1: rule__NegativeDisjunction__Group_1__1__Impl : ( ( rule__NegativeDisjunction__Alternatives_1_1 ) ) ;
    public final void rule__NegativeDisjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1533:1: ( ( ( rule__NegativeDisjunction__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1534:1: ( ( rule__NegativeDisjunction__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1534:1: ( ( rule__NegativeDisjunction__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1535:2: ( rule__NegativeDisjunction__Alternatives_1_1 )
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1536:2: ( rule__NegativeDisjunction__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1536:3: rule__NegativeDisjunction__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getNegativeDisjunctionAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group_1__1__Impl"


    // $ANTLR start "rule__NegativeDisjunction__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1544:1: rule__NegativeDisjunction__Group_1__2 : rule__NegativeDisjunction__Group_1__2__Impl ;
    public final void rule__NegativeDisjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1548:1: ( rule__NegativeDisjunction__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1549:2: rule__NegativeDisjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group_1__2"


    // $ANTLR start "rule__NegativeDisjunction__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1555:1: rule__NegativeDisjunction__Group_1__2__Impl : ( ( rule__NegativeDisjunction__RightAssignment_1_2 ) ) ;
    public final void rule__NegativeDisjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1559:1: ( ( ( rule__NegativeDisjunction__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1560:1: ( ( rule__NegativeDisjunction__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1560:1: ( ( rule__NegativeDisjunction__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1561:2: ( rule__NegativeDisjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1562:2: ( rule__NegativeDisjunction__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1562:3: rule__NegativeDisjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__NegativeDisjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getNegativeDisjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__Group_1__2__Impl"


    // $ANTLR start "rule__Disjunction__Group__0"
    // InternalPropositionalLogicLanguage.g:1571:1: rule__Disjunction__Group__0 : rule__Disjunction__Group__0__Impl rule__Disjunction__Group__1 ;
    public final void rule__Disjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1575:1: ( rule__Disjunction__Group__0__Impl rule__Disjunction__Group__1 )
            // InternalPropositionalLogicLanguage.g:1576:2: rule__Disjunction__Group__0__Impl rule__Disjunction__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Disjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Disjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group__0"


    // $ANTLR start "rule__Disjunction__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1583:1: rule__Disjunction__Group__0__Impl : ( ruleNegativeConjunction ) ;
    public final void rule__Disjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1587:1: ( ( ruleNegativeConjunction ) )
            // InternalPropositionalLogicLanguage.g:1588:1: ( ruleNegativeConjunction )
            {
            // InternalPropositionalLogicLanguage.g:1588:1: ( ruleNegativeConjunction )
            // InternalPropositionalLogicLanguage.g:1589:2: ruleNegativeConjunction
            {
             before(grammarAccess.getDisjunctionAccess().getNegativeConjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNegativeConjunction();

            state._fsp--;

             after(grammarAccess.getDisjunctionAccess().getNegativeConjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group__0__Impl"


    // $ANTLR start "rule__Disjunction__Group__1"
    // InternalPropositionalLogicLanguage.g:1598:1: rule__Disjunction__Group__1 : rule__Disjunction__Group__1__Impl ;
    public final void rule__Disjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1602:1: ( rule__Disjunction__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1603:2: rule__Disjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Disjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group__1"


    // $ANTLR start "rule__Disjunction__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1609:1: rule__Disjunction__Group__1__Impl : ( ( rule__Disjunction__Group_1__0 )* ) ;
    public final void rule__Disjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1613:1: ( ( ( rule__Disjunction__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1614:1: ( ( rule__Disjunction__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1614:1: ( ( rule__Disjunction__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1615:2: ( rule__Disjunction__Group_1__0 )*
            {
             before(grammarAccess.getDisjunctionAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1616:2: ( rule__Disjunction__Group_1__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=19 && LA20_0<=20)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1616:3: rule__Disjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__Disjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getDisjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group__1__Impl"


    // $ANTLR start "rule__Disjunction__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1625:1: rule__Disjunction__Group_1__0 : rule__Disjunction__Group_1__0__Impl rule__Disjunction__Group_1__1 ;
    public final void rule__Disjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1629:1: ( rule__Disjunction__Group_1__0__Impl rule__Disjunction__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1630:2: rule__Disjunction__Group_1__0__Impl rule__Disjunction__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__Disjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Disjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group_1__0"


    // $ANTLR start "rule__Disjunction__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1637:1: rule__Disjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__Disjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1641:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1642:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1642:1: ( () )
            // InternalPropositionalLogicLanguage.g:1643:2: ()
            {
             before(grammarAccess.getDisjunctionAccess().getDisjunctionLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1644:2: ()
            // InternalPropositionalLogicLanguage.g:1644:3: 
            {
            }

             after(grammarAccess.getDisjunctionAccess().getDisjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group_1__0__Impl"


    // $ANTLR start "rule__Disjunction__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1652:1: rule__Disjunction__Group_1__1 : rule__Disjunction__Group_1__1__Impl rule__Disjunction__Group_1__2 ;
    public final void rule__Disjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1656:1: ( rule__Disjunction__Group_1__1__Impl rule__Disjunction__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1657:2: rule__Disjunction__Group_1__1__Impl rule__Disjunction__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Disjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Disjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group_1__1"


    // $ANTLR start "rule__Disjunction__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1664:1: rule__Disjunction__Group_1__1__Impl : ( ( rule__Disjunction__Alternatives_1_1 ) ) ;
    public final void rule__Disjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1668:1: ( ( ( rule__Disjunction__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1669:1: ( ( rule__Disjunction__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1669:1: ( ( rule__Disjunction__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1670:2: ( rule__Disjunction__Alternatives_1_1 )
            {
             before(grammarAccess.getDisjunctionAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1671:2: ( rule__Disjunction__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1671:3: rule__Disjunction__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Disjunction__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getDisjunctionAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group_1__1__Impl"


    // $ANTLR start "rule__Disjunction__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1679:1: rule__Disjunction__Group_1__2 : rule__Disjunction__Group_1__2__Impl ;
    public final void rule__Disjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1683:1: ( rule__Disjunction__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1684:2: rule__Disjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Disjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group_1__2"


    // $ANTLR start "rule__Disjunction__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1690:1: rule__Disjunction__Group_1__2__Impl : ( ( rule__Disjunction__RightAssignment_1_2 ) ) ;
    public final void rule__Disjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1694:1: ( ( ( rule__Disjunction__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1695:1: ( ( rule__Disjunction__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1695:1: ( ( rule__Disjunction__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1696:2: ( rule__Disjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getDisjunctionAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1697:2: ( rule__Disjunction__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1697:3: rule__Disjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Disjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getDisjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__Group_1__2__Impl"


    // $ANTLR start "rule__NegativeConjunction__Group__0"
    // InternalPropositionalLogicLanguage.g:1706:1: rule__NegativeConjunction__Group__0 : rule__NegativeConjunction__Group__0__Impl rule__NegativeConjunction__Group__1 ;
    public final void rule__NegativeConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1710:1: ( rule__NegativeConjunction__Group__0__Impl rule__NegativeConjunction__Group__1 )
            // InternalPropositionalLogicLanguage.g:1711:2: rule__NegativeConjunction__Group__0__Impl rule__NegativeConjunction__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__NegativeConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group__0"


    // $ANTLR start "rule__NegativeConjunction__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1718:1: rule__NegativeConjunction__Group__0__Impl : ( ruleConjunction ) ;
    public final void rule__NegativeConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1722:1: ( ( ruleConjunction ) )
            // InternalPropositionalLogicLanguage.g:1723:1: ( ruleConjunction )
            {
            // InternalPropositionalLogicLanguage.g:1723:1: ( ruleConjunction )
            // InternalPropositionalLogicLanguage.g:1724:2: ruleConjunction
            {
             before(grammarAccess.getNegativeConjunctionAccess().getConjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleConjunction();

            state._fsp--;

             after(grammarAccess.getNegativeConjunctionAccess().getConjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group__0__Impl"


    // $ANTLR start "rule__NegativeConjunction__Group__1"
    // InternalPropositionalLogicLanguage.g:1733:1: rule__NegativeConjunction__Group__1 : rule__NegativeConjunction__Group__1__Impl ;
    public final void rule__NegativeConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1737:1: ( rule__NegativeConjunction__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1738:2: rule__NegativeConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group__1"


    // $ANTLR start "rule__NegativeConjunction__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1744:1: rule__NegativeConjunction__Group__1__Impl : ( ( rule__NegativeConjunction__Group_1__0 )* ) ;
    public final void rule__NegativeConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1748:1: ( ( ( rule__NegativeConjunction__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1749:1: ( ( rule__NegativeConjunction__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1749:1: ( ( rule__NegativeConjunction__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1750:2: ( rule__NegativeConjunction__Group_1__0 )*
            {
             before(grammarAccess.getNegativeConjunctionAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1751:2: ( rule__NegativeConjunction__Group_1__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>=21 && LA21_0<=22)) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1751:3: rule__NegativeConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__NegativeConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getNegativeConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group__1__Impl"


    // $ANTLR start "rule__NegativeConjunction__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1760:1: rule__NegativeConjunction__Group_1__0 : rule__NegativeConjunction__Group_1__0__Impl rule__NegativeConjunction__Group_1__1 ;
    public final void rule__NegativeConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1764:1: ( rule__NegativeConjunction__Group_1__0__Impl rule__NegativeConjunction__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1765:2: rule__NegativeConjunction__Group_1__0__Impl rule__NegativeConjunction__Group_1__1
            {
            pushFollow(FOLLOW_19);
            rule__NegativeConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group_1__0"


    // $ANTLR start "rule__NegativeConjunction__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1772:1: rule__NegativeConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__NegativeConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1776:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1777:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1777:1: ( () )
            // InternalPropositionalLogicLanguage.g:1778:2: ()
            {
             before(grammarAccess.getNegativeConjunctionAccess().getNegativeConjunctionLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1779:2: ()
            // InternalPropositionalLogicLanguage.g:1779:3: 
            {
            }

             after(grammarAccess.getNegativeConjunctionAccess().getNegativeConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__NegativeConjunction__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1787:1: rule__NegativeConjunction__Group_1__1 : rule__NegativeConjunction__Group_1__1__Impl rule__NegativeConjunction__Group_1__2 ;
    public final void rule__NegativeConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1791:1: ( rule__NegativeConjunction__Group_1__1__Impl rule__NegativeConjunction__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1792:2: rule__NegativeConjunction__Group_1__1__Impl rule__NegativeConjunction__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__NegativeConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group_1__1"


    // $ANTLR start "rule__NegativeConjunction__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1799:1: rule__NegativeConjunction__Group_1__1__Impl : ( ( rule__NegativeConjunction__Alternatives_1_1 ) ) ;
    public final void rule__NegativeConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1803:1: ( ( ( rule__NegativeConjunction__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1804:1: ( ( rule__NegativeConjunction__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1804:1: ( ( rule__NegativeConjunction__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1805:2: ( rule__NegativeConjunction__Alternatives_1_1 )
            {
             before(grammarAccess.getNegativeConjunctionAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1806:2: ( rule__NegativeConjunction__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1806:3: rule__NegativeConjunction__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getNegativeConjunctionAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__NegativeConjunction__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1814:1: rule__NegativeConjunction__Group_1__2 : rule__NegativeConjunction__Group_1__2__Impl ;
    public final void rule__NegativeConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1818:1: ( rule__NegativeConjunction__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1819:2: rule__NegativeConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group_1__2"


    // $ANTLR start "rule__NegativeConjunction__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1825:1: rule__NegativeConjunction__Group_1__2__Impl : ( ( rule__NegativeConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__NegativeConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1829:1: ( ( ( rule__NegativeConjunction__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1830:1: ( ( rule__NegativeConjunction__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1830:1: ( ( rule__NegativeConjunction__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1831:2: ( rule__NegativeConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getNegativeConjunctionAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1832:2: ( rule__NegativeConjunction__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1832:3: rule__NegativeConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__NegativeConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getNegativeConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__Conjunction__Group__0"
    // InternalPropositionalLogicLanguage.g:1841:1: rule__Conjunction__Group__0 : rule__Conjunction__Group__0__Impl rule__Conjunction__Group__1 ;
    public final void rule__Conjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1845:1: ( rule__Conjunction__Group__0__Impl rule__Conjunction__Group__1 )
            // InternalPropositionalLogicLanguage.g:1846:2: rule__Conjunction__Group__0__Impl rule__Conjunction__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__Conjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group__0"


    // $ANTLR start "rule__Conjunction__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:1853:1: rule__Conjunction__Group__0__Impl : ( ruleProposition ) ;
    public final void rule__Conjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1857:1: ( ( ruleProposition ) )
            // InternalPropositionalLogicLanguage.g:1858:1: ( ruleProposition )
            {
            // InternalPropositionalLogicLanguage.g:1858:1: ( ruleProposition )
            // InternalPropositionalLogicLanguage.g:1859:2: ruleProposition
            {
             before(grammarAccess.getConjunctionAccess().getPropositionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleProposition();

            state._fsp--;

             after(grammarAccess.getConjunctionAccess().getPropositionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group__0__Impl"


    // $ANTLR start "rule__Conjunction__Group__1"
    // InternalPropositionalLogicLanguage.g:1868:1: rule__Conjunction__Group__1 : rule__Conjunction__Group__1__Impl ;
    public final void rule__Conjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1872:1: ( rule__Conjunction__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:1873:2: rule__Conjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Conjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group__1"


    // $ANTLR start "rule__Conjunction__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:1879:1: rule__Conjunction__Group__1__Impl : ( ( rule__Conjunction__Group_1__0 )* ) ;
    public final void rule__Conjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1883:1: ( ( ( rule__Conjunction__Group_1__0 )* ) )
            // InternalPropositionalLogicLanguage.g:1884:1: ( ( rule__Conjunction__Group_1__0 )* )
            {
            // InternalPropositionalLogicLanguage.g:1884:1: ( ( rule__Conjunction__Group_1__0 )* )
            // InternalPropositionalLogicLanguage.g:1885:2: ( rule__Conjunction__Group_1__0 )*
            {
             before(grammarAccess.getConjunctionAccess().getGroup_1()); 
            // InternalPropositionalLogicLanguage.g:1886:2: ( rule__Conjunction__Group_1__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>=23 && LA22_0<=24)) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:1886:3: rule__Conjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_22);
            	    rule__Conjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group__1__Impl"


    // $ANTLR start "rule__Conjunction__Group_1__0"
    // InternalPropositionalLogicLanguage.g:1895:1: rule__Conjunction__Group_1__0 : rule__Conjunction__Group_1__0__Impl rule__Conjunction__Group_1__1 ;
    public final void rule__Conjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1899:1: ( rule__Conjunction__Group_1__0__Impl rule__Conjunction__Group_1__1 )
            // InternalPropositionalLogicLanguage.g:1900:2: rule__Conjunction__Group_1__0__Impl rule__Conjunction__Group_1__1
            {
            pushFollow(FOLLOW_21);
            rule__Conjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group_1__0"


    // $ANTLR start "rule__Conjunction__Group_1__0__Impl"
    // InternalPropositionalLogicLanguage.g:1907:1: rule__Conjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__Conjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1911:1: ( ( () ) )
            // InternalPropositionalLogicLanguage.g:1912:1: ( () )
            {
            // InternalPropositionalLogicLanguage.g:1912:1: ( () )
            // InternalPropositionalLogicLanguage.g:1913:2: ()
            {
             before(grammarAccess.getConjunctionAccess().getConjunctionLeftAction_1_0()); 
            // InternalPropositionalLogicLanguage.g:1914:2: ()
            // InternalPropositionalLogicLanguage.g:1914:3: 
            {
            }

             after(grammarAccess.getConjunctionAccess().getConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group_1__0__Impl"


    // $ANTLR start "rule__Conjunction__Group_1__1"
    // InternalPropositionalLogicLanguage.g:1922:1: rule__Conjunction__Group_1__1 : rule__Conjunction__Group_1__1__Impl rule__Conjunction__Group_1__2 ;
    public final void rule__Conjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1926:1: ( rule__Conjunction__Group_1__1__Impl rule__Conjunction__Group_1__2 )
            // InternalPropositionalLogicLanguage.g:1927:2: rule__Conjunction__Group_1__1__Impl rule__Conjunction__Group_1__2
            {
            pushFollow(FOLLOW_3);
            rule__Conjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Conjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group_1__1"


    // $ANTLR start "rule__Conjunction__Group_1__1__Impl"
    // InternalPropositionalLogicLanguage.g:1934:1: rule__Conjunction__Group_1__1__Impl : ( ( rule__Conjunction__Alternatives_1_1 ) ) ;
    public final void rule__Conjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1938:1: ( ( ( rule__Conjunction__Alternatives_1_1 ) ) )
            // InternalPropositionalLogicLanguage.g:1939:1: ( ( rule__Conjunction__Alternatives_1_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:1939:1: ( ( rule__Conjunction__Alternatives_1_1 ) )
            // InternalPropositionalLogicLanguage.g:1940:2: ( rule__Conjunction__Alternatives_1_1 )
            {
             before(grammarAccess.getConjunctionAccess().getAlternatives_1_1()); 
            // InternalPropositionalLogicLanguage.g:1941:2: ( rule__Conjunction__Alternatives_1_1 )
            // InternalPropositionalLogicLanguage.g:1941:3: rule__Conjunction__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Conjunction__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getConjunctionAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group_1__1__Impl"


    // $ANTLR start "rule__Conjunction__Group_1__2"
    // InternalPropositionalLogicLanguage.g:1949:1: rule__Conjunction__Group_1__2 : rule__Conjunction__Group_1__2__Impl ;
    public final void rule__Conjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1953:1: ( rule__Conjunction__Group_1__2__Impl )
            // InternalPropositionalLogicLanguage.g:1954:2: rule__Conjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Conjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group_1__2"


    // $ANTLR start "rule__Conjunction__Group_1__2__Impl"
    // InternalPropositionalLogicLanguage.g:1960:1: rule__Conjunction__Group_1__2__Impl : ( ( rule__Conjunction__RightAssignment_1_2 ) ) ;
    public final void rule__Conjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1964:1: ( ( ( rule__Conjunction__RightAssignment_1_2 ) ) )
            // InternalPropositionalLogicLanguage.g:1965:1: ( ( rule__Conjunction__RightAssignment_1_2 ) )
            {
            // InternalPropositionalLogicLanguage.g:1965:1: ( ( rule__Conjunction__RightAssignment_1_2 ) )
            // InternalPropositionalLogicLanguage.g:1966:2: ( rule__Conjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getConjunctionAccess().getRightAssignment_1_2()); 
            // InternalPropositionalLogicLanguage.g:1967:2: ( rule__Conjunction__RightAssignment_1_2 )
            // InternalPropositionalLogicLanguage.g:1967:3: rule__Conjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Conjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__Group_1__2__Impl"


    // $ANTLR start "rule__Proposition__Group_0__0"
    // InternalPropositionalLogicLanguage.g:1976:1: rule__Proposition__Group_0__0 : rule__Proposition__Group_0__0__Impl rule__Proposition__Group_0__1 ;
    public final void rule__Proposition__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1980:1: ( rule__Proposition__Group_0__0__Impl rule__Proposition__Group_0__1 )
            // InternalPropositionalLogicLanguage.g:1981:2: rule__Proposition__Group_0__0__Impl rule__Proposition__Group_0__1
            {
            pushFollow(FOLLOW_3);
            rule__Proposition__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Proposition__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Group_0__0"


    // $ANTLR start "rule__Proposition__Group_0__0__Impl"
    // InternalPropositionalLogicLanguage.g:1988:1: rule__Proposition__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Proposition__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:1992:1: ( ( '(' ) )
            // InternalPropositionalLogicLanguage.g:1993:1: ( '(' )
            {
            // InternalPropositionalLogicLanguage.g:1993:1: ( '(' )
            // InternalPropositionalLogicLanguage.g:1994:2: '('
            {
             before(grammarAccess.getPropositionAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getPropositionAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Group_0__0__Impl"


    // $ANTLR start "rule__Proposition__Group_0__1"
    // InternalPropositionalLogicLanguage.g:2003:1: rule__Proposition__Group_0__1 : rule__Proposition__Group_0__1__Impl rule__Proposition__Group_0__2 ;
    public final void rule__Proposition__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2007:1: ( rule__Proposition__Group_0__1__Impl rule__Proposition__Group_0__2 )
            // InternalPropositionalLogicLanguage.g:2008:2: rule__Proposition__Group_0__1__Impl rule__Proposition__Group_0__2
            {
            pushFollow(FOLLOW_23);
            rule__Proposition__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Proposition__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Group_0__1"


    // $ANTLR start "rule__Proposition__Group_0__1__Impl"
    // InternalPropositionalLogicLanguage.g:2015:1: rule__Proposition__Group_0__1__Impl : ( ruleBiimplies ) ;
    public final void rule__Proposition__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2019:1: ( ( ruleBiimplies ) )
            // InternalPropositionalLogicLanguage.g:2020:1: ( ruleBiimplies )
            {
            // InternalPropositionalLogicLanguage.g:2020:1: ( ruleBiimplies )
            // InternalPropositionalLogicLanguage.g:2021:2: ruleBiimplies
            {
             before(grammarAccess.getPropositionAccess().getBiimpliesParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPropositionAccess().getBiimpliesParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Group_0__1__Impl"


    // $ANTLR start "rule__Proposition__Group_0__2"
    // InternalPropositionalLogicLanguage.g:2030:1: rule__Proposition__Group_0__2 : rule__Proposition__Group_0__2__Impl ;
    public final void rule__Proposition__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2034:1: ( rule__Proposition__Group_0__2__Impl )
            // InternalPropositionalLogicLanguage.g:2035:2: rule__Proposition__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Proposition__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Group_0__2"


    // $ANTLR start "rule__Proposition__Group_0__2__Impl"
    // InternalPropositionalLogicLanguage.g:2041:1: rule__Proposition__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Proposition__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2045:1: ( ( ')' ) )
            // InternalPropositionalLogicLanguage.g:2046:1: ( ')' )
            {
            // InternalPropositionalLogicLanguage.g:2046:1: ( ')' )
            // InternalPropositionalLogicLanguage.g:2047:2: ')'
            {
             before(grammarAccess.getPropositionAccess().getRightParenthesisKeyword_0_2()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getPropositionAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__Group_0__2__Impl"


    // $ANTLR start "rule__Negation__Group__0"
    // InternalPropositionalLogicLanguage.g:2057:1: rule__Negation__Group__0 : rule__Negation__Group__0__Impl rule__Negation__Group__1 ;
    public final void rule__Negation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2061:1: ( rule__Negation__Group__0__Impl rule__Negation__Group__1 )
            // InternalPropositionalLogicLanguage.g:2062:2: rule__Negation__Group__0__Impl rule__Negation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Negation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Negation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Negation__Group__0"


    // $ANTLR start "rule__Negation__Group__0__Impl"
    // InternalPropositionalLogicLanguage.g:2069:1: rule__Negation__Group__0__Impl : ( ( rule__Negation__Alternatives_0 ) ) ;
    public final void rule__Negation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2073:1: ( ( ( rule__Negation__Alternatives_0 ) ) )
            // InternalPropositionalLogicLanguage.g:2074:1: ( ( rule__Negation__Alternatives_0 ) )
            {
            // InternalPropositionalLogicLanguage.g:2074:1: ( ( rule__Negation__Alternatives_0 ) )
            // InternalPropositionalLogicLanguage.g:2075:2: ( rule__Negation__Alternatives_0 )
            {
             before(grammarAccess.getNegationAccess().getAlternatives_0()); 
            // InternalPropositionalLogicLanguage.g:2076:2: ( rule__Negation__Alternatives_0 )
            // InternalPropositionalLogicLanguage.g:2076:3: rule__Negation__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Negation__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getNegationAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Negation__Group__0__Impl"


    // $ANTLR start "rule__Negation__Group__1"
    // InternalPropositionalLogicLanguage.g:2084:1: rule__Negation__Group__1 : rule__Negation__Group__1__Impl ;
    public final void rule__Negation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2088:1: ( rule__Negation__Group__1__Impl )
            // InternalPropositionalLogicLanguage.g:2089:2: rule__Negation__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Negation__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Negation__Group__1"


    // $ANTLR start "rule__Negation__Group__1__Impl"
    // InternalPropositionalLogicLanguage.g:2095:1: rule__Negation__Group__1__Impl : ( ( rule__Negation__ContentAssignment_1 ) ) ;
    public final void rule__Negation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2099:1: ( ( ( rule__Negation__ContentAssignment_1 ) ) )
            // InternalPropositionalLogicLanguage.g:2100:1: ( ( rule__Negation__ContentAssignment_1 ) )
            {
            // InternalPropositionalLogicLanguage.g:2100:1: ( ( rule__Negation__ContentAssignment_1 ) )
            // InternalPropositionalLogicLanguage.g:2101:2: ( rule__Negation__ContentAssignment_1 )
            {
             before(grammarAccess.getNegationAccess().getContentAssignment_1()); 
            // InternalPropositionalLogicLanguage.g:2102:2: ( rule__Negation__ContentAssignment_1 )
            // InternalPropositionalLogicLanguage.g:2102:3: rule__Negation__ContentAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Negation__ContentAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNegationAccess().getContentAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Negation__Group__1__Impl"


    // $ANTLR start "rule__File__UsingsAssignment_0"
    // InternalPropositionalLogicLanguage.g:2111:1: rule__File__UsingsAssignment_0 : ( ruleUsing ) ;
    public final void rule__File__UsingsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2115:1: ( ( ruleUsing ) )
            // InternalPropositionalLogicLanguage.g:2116:2: ( ruleUsing )
            {
            // InternalPropositionalLogicLanguage.g:2116:2: ( ruleUsing )
            // InternalPropositionalLogicLanguage.g:2117:3: ruleUsing
            {
             before(grammarAccess.getFileAccess().getUsingsUsingParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleUsing();

            state._fsp--;

             after(grammarAccess.getFileAccess().getUsingsUsingParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__UsingsAssignment_0"


    // $ANTLR start "rule__File__ContentAssignment_1"
    // InternalPropositionalLogicLanguage.g:2126:1: rule__File__ContentAssignment_1 : ( ruleBiimplies ) ;
    public final void rule__File__ContentAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2130:1: ( ( ruleBiimplies ) )
            // InternalPropositionalLogicLanguage.g:2131:2: ( ruleBiimplies )
            {
            // InternalPropositionalLogicLanguage.g:2131:2: ( ruleBiimplies )
            // InternalPropositionalLogicLanguage.g:2132:3: ruleBiimplies
            {
             before(grammarAccess.getFileAccess().getContentBiimpliesParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getFileAccess().getContentBiimpliesParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__ContentAssignment_1"


    // $ANTLR start "rule__Using__KindAssignment_0_2"
    // InternalPropositionalLogicLanguage.g:2141:1: rule__Using__KindAssignment_0_2 : ( RULE_ID ) ;
    public final void rule__Using__KindAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2145:1: ( ( RULE_ID ) )
            // InternalPropositionalLogicLanguage.g:2146:2: ( RULE_ID )
            {
            // InternalPropositionalLogicLanguage.g:2146:2: ( RULE_ID )
            // InternalPropositionalLogicLanguage.g:2147:3: RULE_ID
            {
             before(grammarAccess.getUsingAccess().getKindIDTerminalRuleCall_0_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getKindIDTerminalRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__KindAssignment_0_2"


    // $ANTLR start "rule__Using__NameAssignment_0_6"
    // InternalPropositionalLogicLanguage.g:2156:1: rule__Using__NameAssignment_0_6 : ( RULE_ID ) ;
    public final void rule__Using__NameAssignment_0_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2160:1: ( ( RULE_ID ) )
            // InternalPropositionalLogicLanguage.g:2161:2: ( RULE_ID )
            {
            // InternalPropositionalLogicLanguage.g:2161:2: ( RULE_ID )
            // InternalPropositionalLogicLanguage.g:2162:3: RULE_ID
            {
             before(grammarAccess.getUsingAccess().getNameIDTerminalRuleCall_0_6_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getNameIDTerminalRuleCall_0_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__NameAssignment_0_6"


    // $ANTLR start "rule__Using__NameAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2171:1: rule__Using__NameAssignment_1_2 : ( RULE_ID ) ;
    public final void rule__Using__NameAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2175:1: ( ( RULE_ID ) )
            // InternalPropositionalLogicLanguage.g:2176:2: ( RULE_ID )
            {
            // InternalPropositionalLogicLanguage.g:2176:2: ( RULE_ID )
            // InternalPropositionalLogicLanguage.g:2177:3: RULE_ID
            {
             before(grammarAccess.getUsingAccess().getNameIDTerminalRuleCall_1_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUsingAccess().getNameIDTerminalRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Using__NameAssignment_1_2"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2186:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2190:1: ( ( ruleImplies ) )
            // InternalPropositionalLogicLanguage.g:2191:2: ( ruleImplies )
            {
            // InternalPropositionalLogicLanguage.g:2191:2: ( ruleImplies )
            // InternalPropositionalLogicLanguage.g:2192:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2201:1: rule__Implies__RightAssignment_1_2 : ( ruleExclusiveDisjunction ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2205:1: ( ( ruleExclusiveDisjunction ) )
            // InternalPropositionalLogicLanguage.g:2206:2: ( ruleExclusiveDisjunction )
            {
            // InternalPropositionalLogicLanguage.g:2206:2: ( ruleExclusiveDisjunction )
            // InternalPropositionalLogicLanguage.g:2207:3: ruleExclusiveDisjunction
            {
             before(grammarAccess.getImpliesAccess().getRightExclusiveDisjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExclusiveDisjunction();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightExclusiveDisjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__ExclusiveDisjunction__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2216:1: rule__ExclusiveDisjunction__RightAssignment_1_2 : ( ruleNegativeDisjunction ) ;
    public final void rule__ExclusiveDisjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2220:1: ( ( ruleNegativeDisjunction ) )
            // InternalPropositionalLogicLanguage.g:2221:2: ( ruleNegativeDisjunction )
            {
            // InternalPropositionalLogicLanguage.g:2221:2: ( ruleNegativeDisjunction )
            // InternalPropositionalLogicLanguage.g:2222:3: ruleNegativeDisjunction
            {
             before(grammarAccess.getExclusiveDisjunctionAccess().getRightNegativeDisjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNegativeDisjunction();

            state._fsp--;

             after(grammarAccess.getExclusiveDisjunctionAccess().getRightNegativeDisjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExclusiveDisjunction__RightAssignment_1_2"


    // $ANTLR start "rule__NegativeDisjunction__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2231:1: rule__NegativeDisjunction__RightAssignment_1_2 : ( ruleDisjunction ) ;
    public final void rule__NegativeDisjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2235:1: ( ( ruleDisjunction ) )
            // InternalPropositionalLogicLanguage.g:2236:2: ( ruleDisjunction )
            {
            // InternalPropositionalLogicLanguage.g:2236:2: ( ruleDisjunction )
            // InternalPropositionalLogicLanguage.g:2237:3: ruleDisjunction
            {
             before(grammarAccess.getNegativeDisjunctionAccess().getRightDisjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDisjunction();

            state._fsp--;

             after(grammarAccess.getNegativeDisjunctionAccess().getRightDisjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeDisjunction__RightAssignment_1_2"


    // $ANTLR start "rule__Disjunction__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2246:1: rule__Disjunction__RightAssignment_1_2 : ( ruleNegativeConjunction ) ;
    public final void rule__Disjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2250:1: ( ( ruleNegativeConjunction ) )
            // InternalPropositionalLogicLanguage.g:2251:2: ( ruleNegativeConjunction )
            {
            // InternalPropositionalLogicLanguage.g:2251:2: ( ruleNegativeConjunction )
            // InternalPropositionalLogicLanguage.g:2252:3: ruleNegativeConjunction
            {
             before(grammarAccess.getDisjunctionAccess().getRightNegativeConjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNegativeConjunction();

            state._fsp--;

             after(grammarAccess.getDisjunctionAccess().getRightNegativeConjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjunction__RightAssignment_1_2"


    // $ANTLR start "rule__NegativeConjunction__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2261:1: rule__NegativeConjunction__RightAssignment_1_2 : ( ruleConjunction ) ;
    public final void rule__NegativeConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2265:1: ( ( ruleConjunction ) )
            // InternalPropositionalLogicLanguage.g:2266:2: ( ruleConjunction )
            {
            // InternalPropositionalLogicLanguage.g:2266:2: ( ruleConjunction )
            // InternalPropositionalLogicLanguage.g:2267:3: ruleConjunction
            {
             before(grammarAccess.getNegativeConjunctionAccess().getRightConjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleConjunction();

            state._fsp--;

             after(grammarAccess.getNegativeConjunctionAccess().getRightConjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegativeConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__Conjunction__RightAssignment_1_2"
    // InternalPropositionalLogicLanguage.g:2276:1: rule__Conjunction__RightAssignment_1_2 : ( ruleProposition ) ;
    public final void rule__Conjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2280:1: ( ( ruleProposition ) )
            // InternalPropositionalLogicLanguage.g:2281:2: ( ruleProposition )
            {
            // InternalPropositionalLogicLanguage.g:2281:2: ( ruleProposition )
            // InternalPropositionalLogicLanguage.g:2282:3: ruleProposition
            {
             before(grammarAccess.getConjunctionAccess().getRightPropositionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleProposition();

            state._fsp--;

             after(grammarAccess.getConjunctionAccess().getRightPropositionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Conjunction__RightAssignment_1_2"


    // $ANTLR start "rule__Negation__ContentAssignment_1"
    // InternalPropositionalLogicLanguage.g:2291:1: rule__Negation__ContentAssignment_1 : ( ruleProposition ) ;
    public final void rule__Negation__ContentAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2295:1: ( ( ruleProposition ) )
            // InternalPropositionalLogicLanguage.g:2296:2: ( ruleProposition )
            {
            // InternalPropositionalLogicLanguage.g:2296:2: ( ruleProposition )
            // InternalPropositionalLogicLanguage.g:2297:3: ruleProposition
            {
             before(grammarAccess.getNegationAccess().getContentPropositionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleProposition();

            state._fsp--;

             after(grammarAccess.getNegationAccess().getContentPropositionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Negation__ContentAssignment_1"


    // $ANTLR start "rule__Primitive__NameAssignment_0"
    // InternalPropositionalLogicLanguage.g:2306:1: rule__Primitive__NameAssignment_0 : ( ( rule__Primitive__NameAlternatives_0_0 ) ) ;
    public final void rule__Primitive__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2310:1: ( ( ( rule__Primitive__NameAlternatives_0_0 ) ) )
            // InternalPropositionalLogicLanguage.g:2311:2: ( ( rule__Primitive__NameAlternatives_0_0 ) )
            {
            // InternalPropositionalLogicLanguage.g:2311:2: ( ( rule__Primitive__NameAlternatives_0_0 ) )
            // InternalPropositionalLogicLanguage.g:2312:3: ( rule__Primitive__NameAlternatives_0_0 )
            {
             before(grammarAccess.getPrimitiveAccess().getNameAlternatives_0_0()); 
            // InternalPropositionalLogicLanguage.g:2313:3: ( rule__Primitive__NameAlternatives_0_0 )
            // InternalPropositionalLogicLanguage.g:2313:4: rule__Primitive__NameAlternatives_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Primitive__NameAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveAccess().getNameAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primitive__NameAssignment_0"


    // $ANTLR start "rule__Primitive__NameAssignment_1"
    // InternalPropositionalLogicLanguage.g:2321:1: rule__Primitive__NameAssignment_1 : ( ( rule__Primitive__NameAlternatives_1_0 ) ) ;
    public final void rule__Primitive__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2325:1: ( ( ( rule__Primitive__NameAlternatives_1_0 ) ) )
            // InternalPropositionalLogicLanguage.g:2326:2: ( ( rule__Primitive__NameAlternatives_1_0 ) )
            {
            // InternalPropositionalLogicLanguage.g:2326:2: ( ( rule__Primitive__NameAlternatives_1_0 ) )
            // InternalPropositionalLogicLanguage.g:2327:3: ( rule__Primitive__NameAlternatives_1_0 )
            {
             before(grammarAccess.getPrimitiveAccess().getNameAlternatives_1_0()); 
            // InternalPropositionalLogicLanguage.g:2328:3: ( rule__Primitive__NameAlternatives_1_0 )
            // InternalPropositionalLogicLanguage.g:2328:4: rule__Primitive__NameAlternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Primitive__NameAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveAccess().getNameAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primitive__NameAssignment_1"


    // $ANTLR start "rule__Primitive__NameAssignment_2"
    // InternalPropositionalLogicLanguage.g:2336:1: rule__Primitive__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Primitive__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPropositionalLogicLanguage.g:2340:1: ( ( RULE_ID ) )
            // InternalPropositionalLogicLanguage.g:2341:2: ( RULE_ID )
            {
            // InternalPropositionalLogicLanguage.g:2341:2: ( RULE_ID )
            // InternalPropositionalLogicLanguage.g:2342:3: RULE_ID
            {
             before(grammarAccess.getPrimitiveAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimitiveAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primitive__NameAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000027E000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000027E000022L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000018002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000600000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000600002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000001800000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000001800002L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000400000000L});

}