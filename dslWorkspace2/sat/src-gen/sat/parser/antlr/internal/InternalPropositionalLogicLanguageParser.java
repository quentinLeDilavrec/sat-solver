package sat.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import sat.services.PropositionalLogicLanguageGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPropositionalLogicLanguageParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_WS", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_ANY_OTHER", "'using'", "'from'", "'<->'", "'\\u2194'", "'-->'", "'\\u27F6'", "'xor'", "'\\u2A02'", "'nor'", "'\\u2193'", "'or'", "'\\u2228'", "'nand'", "'\\u2191'", "'and'", "'\\u2227'", "'('", "')'", "'not'", "'\\u00AC'", "'true'", "'\\u27D9'", "'false'", "'\\u27D8'"
    };
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPropositionalLogicLanguageParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPropositionalLogicLanguageParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPropositionalLogicLanguageParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPropositionalLogicLanguage.g"; }



     	private PropositionalLogicLanguageGrammarAccess grammarAccess;

        public InternalPropositionalLogicLanguageParser(TokenStream input, PropositionalLogicLanguageGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "File";
       	}

       	@Override
       	protected PropositionalLogicLanguageGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFile"
    // InternalPropositionalLogicLanguage.g:64:1: entryRuleFile returns [EObject current=null] : iv_ruleFile= ruleFile EOF ;
    public final EObject entryRuleFile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFile = null;



        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_ML_COMMENT", "RULE_SL_COMMENT");

        try {
            // InternalPropositionalLogicLanguage.g:66:2: (iv_ruleFile= ruleFile EOF )
            // InternalPropositionalLogicLanguage.g:67:2: iv_ruleFile= ruleFile EOF
            {
             newCompositeNode(grammarAccess.getFileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFile=ruleFile();

            state._fsp--;

             current =iv_ruleFile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleFile"


    // $ANTLR start "ruleFile"
    // InternalPropositionalLogicLanguage.g:76:1: ruleFile returns [EObject current=null] : ( ( (lv_usings_0_0= ruleUsing ) )* ( (lv_content_1_0= ruleBiimplies ) )* ) ;
    public final EObject ruleFile() throws RecognitionException {
        EObject current = null;

        EObject lv_usings_0_0 = null;

        EObject lv_content_1_0 = null;



        	enterRule();
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_ML_COMMENT", "RULE_SL_COMMENT");

        try {
            // InternalPropositionalLogicLanguage.g:83:2: ( ( ( (lv_usings_0_0= ruleUsing ) )* ( (lv_content_1_0= ruleBiimplies ) )* ) )
            // InternalPropositionalLogicLanguage.g:84:2: ( ( (lv_usings_0_0= ruleUsing ) )* ( (lv_content_1_0= ruleBiimplies ) )* )
            {
            // InternalPropositionalLogicLanguage.g:84:2: ( ( (lv_usings_0_0= ruleUsing ) )* ( (lv_content_1_0= ruleBiimplies ) )* )
            // InternalPropositionalLogicLanguage.g:85:3: ( (lv_usings_0_0= ruleUsing ) )* ( (lv_content_1_0= ruleBiimplies ) )*
            {
            // InternalPropositionalLogicLanguage.g:85:3: ( (lv_usings_0_0= ruleUsing ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:86:4: (lv_usings_0_0= ruleUsing )
            	    {
            	    // InternalPropositionalLogicLanguage.g:86:4: (lv_usings_0_0= ruleUsing )
            	    // InternalPropositionalLogicLanguage.g:87:5: lv_usings_0_0= ruleUsing
            	    {

            	    					newCompositeNode(grammarAccess.getFileAccess().getUsingsUsingParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_usings_0_0=ruleUsing();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFileRule());
            	    					}
            	    					add(
            	    						current,
            	    						"usings",
            	    						lv_usings_0_0,
            	    						"sat.PropositionalLogicLanguage.Using");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalPropositionalLogicLanguage.g:104:3: ( (lv_content_1_0= ruleBiimplies ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||LA2_0==27||(LA2_0>=29 && LA2_0<=34)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:105:4: (lv_content_1_0= ruleBiimplies )
            	    {
            	    // InternalPropositionalLogicLanguage.g:105:4: (lv_content_1_0= ruleBiimplies )
            	    // InternalPropositionalLogicLanguage.g:106:5: lv_content_1_0= ruleBiimplies
            	    {

            	    					newCompositeNode(grammarAccess.getFileAccess().getContentBiimpliesParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_content_1_0=ruleBiimplies();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFileRule());
            	    					}
            	    					add(
            	    						current,
            	    						"content",
            	    						lv_content_1_0,
            	    						"sat.PropositionalLogicLanguage.Biimplies");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleFile"


    // $ANTLR start "entryRuleUsing"
    // InternalPropositionalLogicLanguage.g:130:1: entryRuleUsing returns [EObject current=null] : iv_ruleUsing= ruleUsing EOF ;
    public final EObject entryRuleUsing() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUsing = null;


        try {
            // InternalPropositionalLogicLanguage.g:130:46: (iv_ruleUsing= ruleUsing EOF )
            // InternalPropositionalLogicLanguage.g:131:2: iv_ruleUsing= ruleUsing EOF
            {
             newCompositeNode(grammarAccess.getUsingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUsing=ruleUsing();

            state._fsp--;

             current =iv_ruleUsing; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUsing"


    // $ANTLR start "ruleUsing"
    // InternalPropositionalLogicLanguage.g:137:1: ruleUsing returns [EObject current=null] : ( (otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS ) | (otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS ) ) ;
    public final EObject ruleUsing() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_WS_1=null;
        Token lv_kind_2_0=null;
        Token this_WS_3=null;
        Token otherlv_4=null;
        Token this_WS_5=null;
        Token lv_name_6_0=null;
        Token this_WS_7=null;
        Token otherlv_8=null;
        Token this_WS_9=null;
        Token lv_name_10_0=null;
        Token this_WS_11=null;


        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:143:2: ( ( (otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS ) | (otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS ) ) )
            // InternalPropositionalLogicLanguage.g:144:2: ( (otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS ) | (otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS ) )
            {
            // InternalPropositionalLogicLanguage.g:144:2: ( (otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS ) | (otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==RULE_WS) ) {
                    int LA3_2 = input.LA(3);

                    if ( (LA3_2==RULE_ID) ) {
                        int LA3_3 = input.LA(4);

                        if ( (LA3_3==RULE_WS) ) {
                            int LA3_4 = input.LA(5);

                            if ( (LA3_4==EOF||LA3_4==RULE_ID||LA3_4==11||LA3_4==27||(LA3_4>=29 && LA3_4<=34)) ) {
                                alt3=2;
                            }
                            else if ( (LA3_4==12) ) {
                                alt3=1;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 3, 4, input);

                                throw nvae;
                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 3, 3, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:145:3: (otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS )
                    {
                    // InternalPropositionalLogicLanguage.g:145:3: (otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS )
                    // InternalPropositionalLogicLanguage.g:146:4: otherlv_0= 'using' this_WS_1= RULE_WS ( (lv_kind_2_0= RULE_ID ) ) this_WS_3= RULE_WS otherlv_4= 'from' this_WS_5= RULE_WS ( (lv_name_6_0= RULE_ID ) ) this_WS_7= RULE_WS
                    {
                    otherlv_0=(Token)match(input,11,FOLLOW_5); 

                    				newLeafNode(otherlv_0, grammarAccess.getUsingAccess().getUsingKeyword_0_0());
                    			
                    this_WS_1=(Token)match(input,RULE_WS,FOLLOW_6); 

                    				newLeafNode(this_WS_1, grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_1());
                    			
                    // InternalPropositionalLogicLanguage.g:154:4: ( (lv_kind_2_0= RULE_ID ) )
                    // InternalPropositionalLogicLanguage.g:155:5: (lv_kind_2_0= RULE_ID )
                    {
                    // InternalPropositionalLogicLanguage.g:155:5: (lv_kind_2_0= RULE_ID )
                    // InternalPropositionalLogicLanguage.g:156:6: lv_kind_2_0= RULE_ID
                    {
                    lv_kind_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

                    						newLeafNode(lv_kind_2_0, grammarAccess.getUsingAccess().getKindIDTerminalRuleCall_0_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getUsingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"kind",
                    							lv_kind_2_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    this_WS_3=(Token)match(input,RULE_WS,FOLLOW_7); 

                    				newLeafNode(this_WS_3, grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_3());
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getUsingAccess().getFromKeyword_0_4());
                    			
                    this_WS_5=(Token)match(input,RULE_WS,FOLLOW_6); 

                    				newLeafNode(this_WS_5, grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_5());
                    			
                    // InternalPropositionalLogicLanguage.g:184:4: ( (lv_name_6_0= RULE_ID ) )
                    // InternalPropositionalLogicLanguage.g:185:5: (lv_name_6_0= RULE_ID )
                    {
                    // InternalPropositionalLogicLanguage.g:185:5: (lv_name_6_0= RULE_ID )
                    // InternalPropositionalLogicLanguage.g:186:6: lv_name_6_0= RULE_ID
                    {
                    lv_name_6_0=(Token)match(input,RULE_ID,FOLLOW_5); 

                    						newLeafNode(lv_name_6_0, grammarAccess.getUsingAccess().getNameIDTerminalRuleCall_0_6_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getUsingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"name",
                    							lv_name_6_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    this_WS_7=(Token)match(input,RULE_WS,FOLLOW_2); 

                    				newLeafNode(this_WS_7, grammarAccess.getUsingAccess().getWSTerminalRuleCall_0_7());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:208:3: (otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS )
                    {
                    // InternalPropositionalLogicLanguage.g:208:3: (otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS )
                    // InternalPropositionalLogicLanguage.g:209:4: otherlv_8= 'using' this_WS_9= RULE_WS ( (lv_name_10_0= RULE_ID ) ) this_WS_11= RULE_WS
                    {
                    otherlv_8=(Token)match(input,11,FOLLOW_5); 

                    				newLeafNode(otherlv_8, grammarAccess.getUsingAccess().getUsingKeyword_1_0());
                    			
                    this_WS_9=(Token)match(input,RULE_WS,FOLLOW_6); 

                    				newLeafNode(this_WS_9, grammarAccess.getUsingAccess().getWSTerminalRuleCall_1_1());
                    			
                    // InternalPropositionalLogicLanguage.g:217:4: ( (lv_name_10_0= RULE_ID ) )
                    // InternalPropositionalLogicLanguage.g:218:5: (lv_name_10_0= RULE_ID )
                    {
                    // InternalPropositionalLogicLanguage.g:218:5: (lv_name_10_0= RULE_ID )
                    // InternalPropositionalLogicLanguage.g:219:6: lv_name_10_0= RULE_ID
                    {
                    lv_name_10_0=(Token)match(input,RULE_ID,FOLLOW_5); 

                    						newLeafNode(lv_name_10_0, grammarAccess.getUsingAccess().getNameIDTerminalRuleCall_1_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getUsingRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"name",
                    							lv_name_10_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    this_WS_11=(Token)match(input,RULE_WS,FOLLOW_2); 

                    				newLeafNode(this_WS_11, grammarAccess.getUsingAccess().getWSTerminalRuleCall_1_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUsing"


    // $ANTLR start "entryRuleBiimplies"
    // InternalPropositionalLogicLanguage.g:244:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;



        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS");

        try {
            // InternalPropositionalLogicLanguage.g:246:2: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalPropositionalLogicLanguage.g:247:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalPropositionalLogicLanguage.g:256:1: ruleBiimplies returns [EObject current=null] : (this_Implies_0= ruleImplies ( () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) ) )* ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_Implies_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS");

        try {
            // InternalPropositionalLogicLanguage.g:263:2: ( (this_Implies_0= ruleImplies ( () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:264:2: (this_Implies_0= ruleImplies ( () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:264:2: (this_Implies_0= ruleImplies ( () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) ) )* )
            // InternalPropositionalLogicLanguage.g:265:3: this_Implies_0= ruleImplies ( () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) ) )*
            {

            			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
            		
            pushFollow(FOLLOW_8);
            this_Implies_0=ruleImplies();

            state._fsp--;


            			current = this_Implies_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:273:3: ( () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=13 && LA5_0<=14)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:274:4: () (otherlv_2= '<->' | otherlv_3= '\\u2194' ) ( (lv_right_4_0= ruleImplies ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:274:4: ()
            	    // InternalPropositionalLogicLanguage.g:275:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:281:4: (otherlv_2= '<->' | otherlv_3= '\\u2194' )
            	    int alt4=2;
            	    int LA4_0 = input.LA(1);

            	    if ( (LA4_0==13) ) {
            	        alt4=1;
            	    }
            	    else if ( (LA4_0==14) ) {
            	        alt4=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 4, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt4) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:282:5: otherlv_2= '<->'
            	            {
            	            otherlv_2=(Token)match(input,13,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getBiimpliesAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:287:5: otherlv_3= '\\u2194'
            	            {
            	            otherlv_3=(Token)match(input,14,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getBiimpliesAccess().getLeftRightArrowKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:292:4: ( (lv_right_4_0= ruleImplies ) )
            	    // InternalPropositionalLogicLanguage.g:293:5: (lv_right_4_0= ruleImplies )
            	    {
            	    // InternalPropositionalLogicLanguage.g:293:5: (lv_right_4_0= ruleImplies )
            	    // InternalPropositionalLogicLanguage.g:294:6: lv_right_4_0= ruleImplies
            	    {

            	    						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_right_4_0=ruleImplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBiimpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.Implies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalPropositionalLogicLanguage.g:319:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalPropositionalLogicLanguage.g:319:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalPropositionalLogicLanguage.g:320:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalPropositionalLogicLanguage.g:326:1: ruleImplies returns [EObject current=null] : (this_ExclusiveDisjunction_0= ruleExclusiveDisjunction ( () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) ) )* ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_ExclusiveDisjunction_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:332:2: ( (this_ExclusiveDisjunction_0= ruleExclusiveDisjunction ( () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:333:2: (this_ExclusiveDisjunction_0= ruleExclusiveDisjunction ( () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:333:2: (this_ExclusiveDisjunction_0= ruleExclusiveDisjunction ( () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) ) )* )
            // InternalPropositionalLogicLanguage.g:334:3: this_ExclusiveDisjunction_0= ruleExclusiveDisjunction ( () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesAccess().getExclusiveDisjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_10);
            this_ExclusiveDisjunction_0=ruleExclusiveDisjunction();

            state._fsp--;


            			current = this_ExclusiveDisjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:342:3: ( () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=15 && LA7_0<=16)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:343:4: () (otherlv_2= '-->' | otherlv_3= '\\u27F6' ) ( (lv_right_4_0= ruleExclusiveDisjunction ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:343:4: ()
            	    // InternalPropositionalLogicLanguage.g:344:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:350:4: (otherlv_2= '-->' | otherlv_3= '\\u27F6' )
            	    int alt6=2;
            	    int LA6_0 = input.LA(1);

            	    if ( (LA6_0==15) ) {
            	        alt6=1;
            	    }
            	    else if ( (LA6_0==16) ) {
            	        alt6=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 6, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt6) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:351:5: otherlv_2= '-->'
            	            {
            	            otherlv_2=(Token)match(input,15,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getImpliesAccess().getHyphenMinusHyphenMinusGreaterThanSignKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:356:5: otherlv_3= '\\u27F6'
            	            {
            	            otherlv_3=(Token)match(input,16,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getImpliesAccess().getLongRightwardsArrowKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:361:4: ( (lv_right_4_0= ruleExclusiveDisjunction ) )
            	    // InternalPropositionalLogicLanguage.g:362:5: (lv_right_4_0= ruleExclusiveDisjunction )
            	    {
            	    // InternalPropositionalLogicLanguage.g:362:5: (lv_right_4_0= ruleExclusiveDisjunction )
            	    // InternalPropositionalLogicLanguage.g:363:6: lv_right_4_0= ruleExclusiveDisjunction
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesAccess().getRightExclusiveDisjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_10);
            	    lv_right_4_0=ruleExclusiveDisjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.ExclusiveDisjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExclusiveDisjunction"
    // InternalPropositionalLogicLanguage.g:385:1: entryRuleExclusiveDisjunction returns [EObject current=null] : iv_ruleExclusiveDisjunction= ruleExclusiveDisjunction EOF ;
    public final EObject entryRuleExclusiveDisjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExclusiveDisjunction = null;


        try {
            // InternalPropositionalLogicLanguage.g:385:61: (iv_ruleExclusiveDisjunction= ruleExclusiveDisjunction EOF )
            // InternalPropositionalLogicLanguage.g:386:2: iv_ruleExclusiveDisjunction= ruleExclusiveDisjunction EOF
            {
             newCompositeNode(grammarAccess.getExclusiveDisjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExclusiveDisjunction=ruleExclusiveDisjunction();

            state._fsp--;

             current =iv_ruleExclusiveDisjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExclusiveDisjunction"


    // $ANTLR start "ruleExclusiveDisjunction"
    // InternalPropositionalLogicLanguage.g:392:1: ruleExclusiveDisjunction returns [EObject current=null] : (this_NegativeDisjunction_0= ruleNegativeDisjunction ( () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) ) )* ) ;
    public final EObject ruleExclusiveDisjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_NegativeDisjunction_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:398:2: ( (this_NegativeDisjunction_0= ruleNegativeDisjunction ( () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:399:2: (this_NegativeDisjunction_0= ruleNegativeDisjunction ( () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:399:2: (this_NegativeDisjunction_0= ruleNegativeDisjunction ( () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) ) )* )
            // InternalPropositionalLogicLanguage.g:400:3: this_NegativeDisjunction_0= ruleNegativeDisjunction ( () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getExclusiveDisjunctionAccess().getNegativeDisjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_NegativeDisjunction_0=ruleNegativeDisjunction();

            state._fsp--;


            			current = this_NegativeDisjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:408:3: ( () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=17 && LA9_0<=18)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:409:4: () (otherlv_2= 'xor' | otherlv_3= '\\u2A02' ) ( (lv_right_4_0= ruleNegativeDisjunction ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:409:4: ()
            	    // InternalPropositionalLogicLanguage.g:410:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getExclusiveDisjunctionAccess().getExclusiveDisjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:416:4: (otherlv_2= 'xor' | otherlv_3= '\\u2A02' )
            	    int alt8=2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0==17) ) {
            	        alt8=1;
            	    }
            	    else if ( (LA8_0==18) ) {
            	        alt8=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 8, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt8) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:417:5: otherlv_2= 'xor'
            	            {
            	            otherlv_2=(Token)match(input,17,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getExclusiveDisjunctionAccess().getXorKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:422:5: otherlv_3= '\\u2A02'
            	            {
            	            otherlv_3=(Token)match(input,18,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getExclusiveDisjunctionAccess().getNAryCircledTimesOperatorKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:427:4: ( (lv_right_4_0= ruleNegativeDisjunction ) )
            	    // InternalPropositionalLogicLanguage.g:428:5: (lv_right_4_0= ruleNegativeDisjunction )
            	    {
            	    // InternalPropositionalLogicLanguage.g:428:5: (lv_right_4_0= ruleNegativeDisjunction )
            	    // InternalPropositionalLogicLanguage.g:429:6: lv_right_4_0= ruleNegativeDisjunction
            	    {

            	    						newCompositeNode(grammarAccess.getExclusiveDisjunctionAccess().getRightNegativeDisjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_4_0=ruleNegativeDisjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExclusiveDisjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.NegativeDisjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExclusiveDisjunction"


    // $ANTLR start "entryRuleNegativeDisjunction"
    // InternalPropositionalLogicLanguage.g:451:1: entryRuleNegativeDisjunction returns [EObject current=null] : iv_ruleNegativeDisjunction= ruleNegativeDisjunction EOF ;
    public final EObject entryRuleNegativeDisjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegativeDisjunction = null;


        try {
            // InternalPropositionalLogicLanguage.g:451:60: (iv_ruleNegativeDisjunction= ruleNegativeDisjunction EOF )
            // InternalPropositionalLogicLanguage.g:452:2: iv_ruleNegativeDisjunction= ruleNegativeDisjunction EOF
            {
             newCompositeNode(grammarAccess.getNegativeDisjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNegativeDisjunction=ruleNegativeDisjunction();

            state._fsp--;

             current =iv_ruleNegativeDisjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegativeDisjunction"


    // $ANTLR start "ruleNegativeDisjunction"
    // InternalPropositionalLogicLanguage.g:458:1: ruleNegativeDisjunction returns [EObject current=null] : (this_Disjunction_0= ruleDisjunction ( () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) ) )* ) ;
    public final EObject ruleNegativeDisjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_Disjunction_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:464:2: ( (this_Disjunction_0= ruleDisjunction ( () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:465:2: (this_Disjunction_0= ruleDisjunction ( () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:465:2: (this_Disjunction_0= ruleDisjunction ( () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) ) )* )
            // InternalPropositionalLogicLanguage.g:466:3: this_Disjunction_0= ruleDisjunction ( () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getNegativeDisjunctionAccess().getDisjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_12);
            this_Disjunction_0=ruleDisjunction();

            state._fsp--;


            			current = this_Disjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:474:3: ( () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>=19 && LA11_0<=20)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:475:4: () (otherlv_2= 'nor' | otherlv_3= '\\u2193' ) ( (lv_right_4_0= ruleDisjunction ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:475:4: ()
            	    // InternalPropositionalLogicLanguage.g:476:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getNegativeDisjunctionAccess().getNegativeDisjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:482:4: (otherlv_2= 'nor' | otherlv_3= '\\u2193' )
            	    int alt10=2;
            	    int LA10_0 = input.LA(1);

            	    if ( (LA10_0==19) ) {
            	        alt10=1;
            	    }
            	    else if ( (LA10_0==20) ) {
            	        alt10=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 10, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt10) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:483:5: otherlv_2= 'nor'
            	            {
            	            otherlv_2=(Token)match(input,19,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getNegativeDisjunctionAccess().getNorKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:488:5: otherlv_3= '\\u2193'
            	            {
            	            otherlv_3=(Token)match(input,20,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getNegativeDisjunctionAccess().getDownwardsArrowKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:493:4: ( (lv_right_4_0= ruleDisjunction ) )
            	    // InternalPropositionalLogicLanguage.g:494:5: (lv_right_4_0= ruleDisjunction )
            	    {
            	    // InternalPropositionalLogicLanguage.g:494:5: (lv_right_4_0= ruleDisjunction )
            	    // InternalPropositionalLogicLanguage.g:495:6: lv_right_4_0= ruleDisjunction
            	    {

            	    						newCompositeNode(grammarAccess.getNegativeDisjunctionAccess().getRightDisjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_right_4_0=ruleDisjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getNegativeDisjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.Disjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegativeDisjunction"


    // $ANTLR start "entryRuleDisjunction"
    // InternalPropositionalLogicLanguage.g:517:1: entryRuleDisjunction returns [EObject current=null] : iv_ruleDisjunction= ruleDisjunction EOF ;
    public final EObject entryRuleDisjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunction = null;


        try {
            // InternalPropositionalLogicLanguage.g:517:52: (iv_ruleDisjunction= ruleDisjunction EOF )
            // InternalPropositionalLogicLanguage.g:518:2: iv_ruleDisjunction= ruleDisjunction EOF
            {
             newCompositeNode(grammarAccess.getDisjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDisjunction=ruleDisjunction();

            state._fsp--;

             current =iv_ruleDisjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunction"


    // $ANTLR start "ruleDisjunction"
    // InternalPropositionalLogicLanguage.g:524:1: ruleDisjunction returns [EObject current=null] : (this_NegativeConjunction_0= ruleNegativeConjunction ( () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) ) )* ) ;
    public final EObject ruleDisjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_NegativeConjunction_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:530:2: ( (this_NegativeConjunction_0= ruleNegativeConjunction ( () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:531:2: (this_NegativeConjunction_0= ruleNegativeConjunction ( () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:531:2: (this_NegativeConjunction_0= ruleNegativeConjunction ( () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) ) )* )
            // InternalPropositionalLogicLanguage.g:532:3: this_NegativeConjunction_0= ruleNegativeConjunction ( () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getDisjunctionAccess().getNegativeConjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_13);
            this_NegativeConjunction_0=ruleNegativeConjunction();

            state._fsp--;


            			current = this_NegativeConjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:540:3: ( () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>=21 && LA13_0<=22)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:541:4: () (otherlv_2= 'or' | otherlv_3= '\\u2228' ) ( (lv_right_4_0= ruleNegativeConjunction ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:541:4: ()
            	    // InternalPropositionalLogicLanguage.g:542:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getDisjunctionAccess().getDisjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:548:4: (otherlv_2= 'or' | otherlv_3= '\\u2228' )
            	    int alt12=2;
            	    int LA12_0 = input.LA(1);

            	    if ( (LA12_0==21) ) {
            	        alt12=1;
            	    }
            	    else if ( (LA12_0==22) ) {
            	        alt12=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 12, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt12) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:549:5: otherlv_2= 'or'
            	            {
            	            otherlv_2=(Token)match(input,21,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getDisjunctionAccess().getOrKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:554:5: otherlv_3= '\\u2228'
            	            {
            	            otherlv_3=(Token)match(input,22,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getDisjunctionAccess().getLogicalOrKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:559:4: ( (lv_right_4_0= ruleNegativeConjunction ) )
            	    // InternalPropositionalLogicLanguage.g:560:5: (lv_right_4_0= ruleNegativeConjunction )
            	    {
            	    // InternalPropositionalLogicLanguage.g:560:5: (lv_right_4_0= ruleNegativeConjunction )
            	    // InternalPropositionalLogicLanguage.g:561:6: lv_right_4_0= ruleNegativeConjunction
            	    {

            	    						newCompositeNode(grammarAccess.getDisjunctionAccess().getRightNegativeConjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_right_4_0=ruleNegativeConjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDisjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.NegativeConjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunction"


    // $ANTLR start "entryRuleNegativeConjunction"
    // InternalPropositionalLogicLanguage.g:583:1: entryRuleNegativeConjunction returns [EObject current=null] : iv_ruleNegativeConjunction= ruleNegativeConjunction EOF ;
    public final EObject entryRuleNegativeConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegativeConjunction = null;


        try {
            // InternalPropositionalLogicLanguage.g:583:60: (iv_ruleNegativeConjunction= ruleNegativeConjunction EOF )
            // InternalPropositionalLogicLanguage.g:584:2: iv_ruleNegativeConjunction= ruleNegativeConjunction EOF
            {
             newCompositeNode(grammarAccess.getNegativeConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNegativeConjunction=ruleNegativeConjunction();

            state._fsp--;

             current =iv_ruleNegativeConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegativeConjunction"


    // $ANTLR start "ruleNegativeConjunction"
    // InternalPropositionalLogicLanguage.g:590:1: ruleNegativeConjunction returns [EObject current=null] : (this_Conjunction_0= ruleConjunction ( () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) ) )* ) ;
    public final EObject ruleNegativeConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_Conjunction_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:596:2: ( (this_Conjunction_0= ruleConjunction ( () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:597:2: (this_Conjunction_0= ruleConjunction ( () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:597:2: (this_Conjunction_0= ruleConjunction ( () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) ) )* )
            // InternalPropositionalLogicLanguage.g:598:3: this_Conjunction_0= ruleConjunction ( () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getNegativeConjunctionAccess().getConjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_14);
            this_Conjunction_0=ruleConjunction();

            state._fsp--;


            			current = this_Conjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:606:3: ( () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>=23 && LA15_0<=24)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:607:4: () (otherlv_2= 'nand' | otherlv_3= '\\u2191' ) ( (lv_right_4_0= ruleConjunction ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:607:4: ()
            	    // InternalPropositionalLogicLanguage.g:608:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getNegativeConjunctionAccess().getNegativeConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:614:4: (otherlv_2= 'nand' | otherlv_3= '\\u2191' )
            	    int alt14=2;
            	    int LA14_0 = input.LA(1);

            	    if ( (LA14_0==23) ) {
            	        alt14=1;
            	    }
            	    else if ( (LA14_0==24) ) {
            	        alt14=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 14, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt14) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:615:5: otherlv_2= 'nand'
            	            {
            	            otherlv_2=(Token)match(input,23,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getNegativeConjunctionAccess().getNandKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:620:5: otherlv_3= '\\u2191'
            	            {
            	            otherlv_3=(Token)match(input,24,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getNegativeConjunctionAccess().getUpwardsArrowKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:625:4: ( (lv_right_4_0= ruleConjunction ) )
            	    // InternalPropositionalLogicLanguage.g:626:5: (lv_right_4_0= ruleConjunction )
            	    {
            	    // InternalPropositionalLogicLanguage.g:626:5: (lv_right_4_0= ruleConjunction )
            	    // InternalPropositionalLogicLanguage.g:627:6: lv_right_4_0= ruleConjunction
            	    {

            	    						newCompositeNode(grammarAccess.getNegativeConjunctionAccess().getRightConjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_14);
            	    lv_right_4_0=ruleConjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getNegativeConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.Conjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegativeConjunction"


    // $ANTLR start "entryRuleConjunction"
    // InternalPropositionalLogicLanguage.g:649:1: entryRuleConjunction returns [EObject current=null] : iv_ruleConjunction= ruleConjunction EOF ;
    public final EObject entryRuleConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunction = null;


        try {
            // InternalPropositionalLogicLanguage.g:649:52: (iv_ruleConjunction= ruleConjunction EOF )
            // InternalPropositionalLogicLanguage.g:650:2: iv_ruleConjunction= ruleConjunction EOF
            {
             newCompositeNode(grammarAccess.getConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConjunction=ruleConjunction();

            state._fsp--;

             current =iv_ruleConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunction"


    // $ANTLR start "ruleConjunction"
    // InternalPropositionalLogicLanguage.g:656:1: ruleConjunction returns [EObject current=null] : (this_Proposition_0= ruleProposition ( () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) ) )* ) ;
    public final EObject ruleConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_Proposition_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:662:2: ( (this_Proposition_0= ruleProposition ( () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) ) )* ) )
            // InternalPropositionalLogicLanguage.g:663:2: (this_Proposition_0= ruleProposition ( () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) ) )* )
            {
            // InternalPropositionalLogicLanguage.g:663:2: (this_Proposition_0= ruleProposition ( () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) ) )* )
            // InternalPropositionalLogicLanguage.g:664:3: this_Proposition_0= ruleProposition ( () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) ) )*
            {

            			newCompositeNode(grammarAccess.getConjunctionAccess().getPropositionParserRuleCall_0());
            		
            pushFollow(FOLLOW_15);
            this_Proposition_0=ruleProposition();

            state._fsp--;


            			current = this_Proposition_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPropositionalLogicLanguage.g:672:3: ( () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=25 && LA17_0<=26)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalPropositionalLogicLanguage.g:673:4: () (otherlv_2= 'and' | otherlv_3= '\\u2227' ) ( (lv_right_4_0= ruleProposition ) )
            	    {
            	    // InternalPropositionalLogicLanguage.g:673:4: ()
            	    // InternalPropositionalLogicLanguage.g:674:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getConjunctionAccess().getConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalPropositionalLogicLanguage.g:680:4: (otherlv_2= 'and' | otherlv_3= '\\u2227' )
            	    int alt16=2;
            	    int LA16_0 = input.LA(1);

            	    if ( (LA16_0==25) ) {
            	        alt16=1;
            	    }
            	    else if ( (LA16_0==26) ) {
            	        alt16=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 16, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt16) {
            	        case 1 :
            	            // InternalPropositionalLogicLanguage.g:681:5: otherlv_2= 'and'
            	            {
            	            otherlv_2=(Token)match(input,25,FOLLOW_9); 

            	            					newLeafNode(otherlv_2, grammarAccess.getConjunctionAccess().getAndKeyword_1_1_0());
            	            				

            	            }
            	            break;
            	        case 2 :
            	            // InternalPropositionalLogicLanguage.g:686:5: otherlv_3= '\\u2227'
            	            {
            	            otherlv_3=(Token)match(input,26,FOLLOW_9); 

            	            					newLeafNode(otherlv_3, grammarAccess.getConjunctionAccess().getLogicalAndKeyword_1_1_1());
            	            				

            	            }
            	            break;

            	    }

            	    // InternalPropositionalLogicLanguage.g:691:4: ( (lv_right_4_0= ruleProposition ) )
            	    // InternalPropositionalLogicLanguage.g:692:5: (lv_right_4_0= ruleProposition )
            	    {
            	    // InternalPropositionalLogicLanguage.g:692:5: (lv_right_4_0= ruleProposition )
            	    // InternalPropositionalLogicLanguage.g:693:6: lv_right_4_0= ruleProposition
            	    {

            	    						newCompositeNode(grammarAccess.getConjunctionAccess().getRightPropositionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_15);
            	    lv_right_4_0=ruleProposition();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"sat.PropositionalLogicLanguage.Proposition");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunction"


    // $ANTLR start "entryRuleProposition"
    // InternalPropositionalLogicLanguage.g:715:1: entryRuleProposition returns [EObject current=null] : iv_ruleProposition= ruleProposition EOF ;
    public final EObject entryRuleProposition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProposition = null;


        try {
            // InternalPropositionalLogicLanguage.g:715:52: (iv_ruleProposition= ruleProposition EOF )
            // InternalPropositionalLogicLanguage.g:716:2: iv_ruleProposition= ruleProposition EOF
            {
             newCompositeNode(grammarAccess.getPropositionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProposition=ruleProposition();

            state._fsp--;

             current =iv_ruleProposition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProposition"


    // $ANTLR start "ruleProposition"
    // InternalPropositionalLogicLanguage.g:722:1: ruleProposition returns [EObject current=null] : ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | this_Negation_3= ruleNegation | this_Primitive_4= rulePrimitive ) ;
    public final EObject ruleProposition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject this_Biimplies_1 = null;

        EObject this_Negation_3 = null;

        EObject this_Primitive_4 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:728:2: ( ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | this_Negation_3= ruleNegation | this_Primitive_4= rulePrimitive ) )
            // InternalPropositionalLogicLanguage.g:729:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | this_Negation_3= ruleNegation | this_Primitive_4= rulePrimitive )
            {
            // InternalPropositionalLogicLanguage.g:729:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | this_Negation_3= ruleNegation | this_Primitive_4= rulePrimitive )
            int alt18=3;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt18=1;
                }
                break;
            case 29:
            case 30:
                {
                alt18=2;
                }
                break;
            case RULE_ID:
            case 31:
            case 32:
            case 33:
            case 34:
                {
                alt18=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:730:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    {
                    // InternalPropositionalLogicLanguage.g:730:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    // InternalPropositionalLogicLanguage.g:731:4: otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,27,FOLLOW_9); 

                    				newLeafNode(otherlv_0, grammarAccess.getPropositionAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getPropositionAccess().getBiimpliesParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_16);
                    this_Biimplies_1=ruleBiimplies();

                    state._fsp--;


                    				current = this_Biimplies_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,28,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getPropositionAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:749:3: this_Negation_3= ruleNegation
                    {

                    			newCompositeNode(grammarAccess.getPropositionAccess().getNegationParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Negation_3=ruleNegation();

                    state._fsp--;


                    			current = this_Negation_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPropositionalLogicLanguage.g:758:3: this_Primitive_4= rulePrimitive
                    {

                    			newCompositeNode(grammarAccess.getPropositionAccess().getPrimitiveParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Primitive_4=rulePrimitive();

                    state._fsp--;


                    			current = this_Primitive_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProposition"


    // $ANTLR start "entryRuleNegation"
    // InternalPropositionalLogicLanguage.g:770:1: entryRuleNegation returns [EObject current=null] : iv_ruleNegation= ruleNegation EOF ;
    public final EObject entryRuleNegation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegation = null;


        try {
            // InternalPropositionalLogicLanguage.g:770:49: (iv_ruleNegation= ruleNegation EOF )
            // InternalPropositionalLogicLanguage.g:771:2: iv_ruleNegation= ruleNegation EOF
            {
             newCompositeNode(grammarAccess.getNegationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNegation=ruleNegation();

            state._fsp--;

             current =iv_ruleNegation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegation"


    // $ANTLR start "ruleNegation"
    // InternalPropositionalLogicLanguage.g:777:1: ruleNegation returns [EObject current=null] : ( (otherlv_0= 'not' | otherlv_1= '\\u00AC' ) ( (lv_content_2_0= ruleProposition ) ) ) ;
    public final EObject ruleNegation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_content_2_0 = null;



        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:783:2: ( ( (otherlv_0= 'not' | otherlv_1= '\\u00AC' ) ( (lv_content_2_0= ruleProposition ) ) ) )
            // InternalPropositionalLogicLanguage.g:784:2: ( (otherlv_0= 'not' | otherlv_1= '\\u00AC' ) ( (lv_content_2_0= ruleProposition ) ) )
            {
            // InternalPropositionalLogicLanguage.g:784:2: ( (otherlv_0= 'not' | otherlv_1= '\\u00AC' ) ( (lv_content_2_0= ruleProposition ) ) )
            // InternalPropositionalLogicLanguage.g:785:3: (otherlv_0= 'not' | otherlv_1= '\\u00AC' ) ( (lv_content_2_0= ruleProposition ) )
            {
            // InternalPropositionalLogicLanguage.g:785:3: (otherlv_0= 'not' | otherlv_1= '\\u00AC' )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==29) ) {
                alt19=1;
            }
            else if ( (LA19_0==30) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:786:4: otherlv_0= 'not'
                    {
                    otherlv_0=(Token)match(input,29,FOLLOW_9); 

                    				newLeafNode(otherlv_0, grammarAccess.getNegationAccess().getNotKeyword_0_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:791:4: otherlv_1= '\\u00AC'
                    {
                    otherlv_1=(Token)match(input,30,FOLLOW_9); 

                    				newLeafNode(otherlv_1, grammarAccess.getNegationAccess().getNotSignKeyword_0_1());
                    			

                    }
                    break;

            }

            // InternalPropositionalLogicLanguage.g:796:3: ( (lv_content_2_0= ruleProposition ) )
            // InternalPropositionalLogicLanguage.g:797:4: (lv_content_2_0= ruleProposition )
            {
            // InternalPropositionalLogicLanguage.g:797:4: (lv_content_2_0= ruleProposition )
            // InternalPropositionalLogicLanguage.g:798:5: lv_content_2_0= ruleProposition
            {

            					newCompositeNode(grammarAccess.getNegationAccess().getContentPropositionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_content_2_0=ruleProposition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNegationRule());
            					}
            					set(
            						current,
            						"content",
            						lv_content_2_0,
            						"sat.PropositionalLogicLanguage.Proposition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegation"


    // $ANTLR start "entryRulePrimitive"
    // InternalPropositionalLogicLanguage.g:819:1: entryRulePrimitive returns [EObject current=null] : iv_rulePrimitive= rulePrimitive EOF ;
    public final EObject entryRulePrimitive() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitive = null;


        try {
            // InternalPropositionalLogicLanguage.g:819:50: (iv_rulePrimitive= rulePrimitive EOF )
            // InternalPropositionalLogicLanguage.g:820:2: iv_rulePrimitive= rulePrimitive EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimitive=rulePrimitive();

            state._fsp--;

             current =iv_rulePrimitive; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitive"


    // $ANTLR start "rulePrimitive"
    // InternalPropositionalLogicLanguage.g:826:1: rulePrimitive returns [EObject current=null] : ( ( ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) ) ) | ( ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) ) ) | ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject rulePrimitive() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_1=null;
        Token lv_name_0_2=null;
        Token lv_name_1_1=null;
        Token lv_name_1_2=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalPropositionalLogicLanguage.g:832:2: ( ( ( ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) ) ) | ( ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) ) ) | ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalPropositionalLogicLanguage.g:833:2: ( ( ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) ) ) | ( ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) ) ) | ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalPropositionalLogicLanguage.g:833:2: ( ( ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) ) ) | ( ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) ) ) | ( (lv_name_2_0= RULE_ID ) ) )
            int alt22=3;
            switch ( input.LA(1) ) {
            case 31:
            case 32:
                {
                alt22=1;
                }
                break;
            case 33:
            case 34:
                {
                alt22=2;
                }
                break;
            case RULE_ID:
                {
                alt22=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // InternalPropositionalLogicLanguage.g:834:3: ( ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) ) )
                    {
                    // InternalPropositionalLogicLanguage.g:834:3: ( ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) ) )
                    // InternalPropositionalLogicLanguage.g:835:4: ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) )
                    {
                    // InternalPropositionalLogicLanguage.g:835:4: ( (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' ) )
                    // InternalPropositionalLogicLanguage.g:836:5: (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' )
                    {
                    // InternalPropositionalLogicLanguage.g:836:5: (lv_name_0_1= 'true' | lv_name_0_2= '\\u27D9' )
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==31) ) {
                        alt20=1;
                    }
                    else if ( (LA20_0==32) ) {
                        alt20=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 0, input);

                        throw nvae;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalPropositionalLogicLanguage.g:837:6: lv_name_0_1= 'true'
                            {
                            lv_name_0_1=(Token)match(input,31,FOLLOW_2); 

                            						newLeafNode(lv_name_0_1, grammarAccess.getPrimitiveAccess().getNameTrueKeyword_0_0_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getPrimitiveRule());
                            						}
                            						setWithLastConsumed(current, "name", lv_name_0_1, null);
                            					

                            }
                            break;
                        case 2 :
                            // InternalPropositionalLogicLanguage.g:848:6: lv_name_0_2= '\\u27D9'
                            {
                            lv_name_0_2=(Token)match(input,32,FOLLOW_2); 

                            						newLeafNode(lv_name_0_2, grammarAccess.getPrimitiveAccess().getNameLargeDownTackKeyword_0_0_1());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getPrimitiveRule());
                            						}
                            						setWithLastConsumed(current, "name", lv_name_0_2, null);
                            					

                            }
                            break;

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPropositionalLogicLanguage.g:862:3: ( ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) ) )
                    {
                    // InternalPropositionalLogicLanguage.g:862:3: ( ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) ) )
                    // InternalPropositionalLogicLanguage.g:863:4: ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) )
                    {
                    // InternalPropositionalLogicLanguage.g:863:4: ( (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' ) )
                    // InternalPropositionalLogicLanguage.g:864:5: (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' )
                    {
                    // InternalPropositionalLogicLanguage.g:864:5: (lv_name_1_1= 'false' | lv_name_1_2= '\\u27D8' )
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0==33) ) {
                        alt21=1;
                    }
                    else if ( (LA21_0==34) ) {
                        alt21=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 21, 0, input);

                        throw nvae;
                    }
                    switch (alt21) {
                        case 1 :
                            // InternalPropositionalLogicLanguage.g:865:6: lv_name_1_1= 'false'
                            {
                            lv_name_1_1=(Token)match(input,33,FOLLOW_2); 

                            						newLeafNode(lv_name_1_1, grammarAccess.getPrimitiveAccess().getNameFalseKeyword_1_0_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getPrimitiveRule());
                            						}
                            						setWithLastConsumed(current, "name", lv_name_1_1, null);
                            					

                            }
                            break;
                        case 2 :
                            // InternalPropositionalLogicLanguage.g:876:6: lv_name_1_2= '\\u27D8'
                            {
                            lv_name_1_2=(Token)match(input,34,FOLLOW_2); 

                            						newLeafNode(lv_name_1_2, grammarAccess.getPrimitiveAccess().getNameLargeUpTackKeyword_1_0_1());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getPrimitiveRule());
                            						}
                            						setWithLastConsumed(current, "name", lv_name_1_2, null);
                            					

                            }
                            break;

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPropositionalLogicLanguage.g:890:3: ( (lv_name_2_0= RULE_ID ) )
                    {
                    // InternalPropositionalLogicLanguage.g:890:3: ( (lv_name_2_0= RULE_ID ) )
                    // InternalPropositionalLogicLanguage.g:891:4: (lv_name_2_0= RULE_ID )
                    {
                    // InternalPropositionalLogicLanguage.g:891:4: (lv_name_2_0= RULE_ID )
                    // InternalPropositionalLogicLanguage.g:892:5: lv_name_2_0= RULE_ID
                    {
                    lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_name_2_0, grammarAccess.getPrimitiveAccess().getNameIDTerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimitiveRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_2_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitive"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000007E8000822L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00000007E8000022L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000007E8000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000018002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000600002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001800002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000006000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000010000000L});

}