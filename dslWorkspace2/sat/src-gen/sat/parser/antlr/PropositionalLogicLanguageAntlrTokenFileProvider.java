/*
 * generated by Xtext 2.15.0
 */
package sat.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class PropositionalLogicLanguageAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("sat/parser/antlr/internal/InternalPropositionalLogicLanguage.tokens");
	}
}
