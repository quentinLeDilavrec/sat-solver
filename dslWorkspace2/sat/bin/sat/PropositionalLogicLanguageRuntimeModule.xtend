/*
 * generated by Xtext 2.15.0
 */
package sat


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class PropositionalLogicLanguageRuntimeModule extends AbstractPropositionalLogicLanguageRuntimeModule {
}
