package sat.solver.sat4j;

import java.io.InputStream;
import java.util.List;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import sat.solver.Solver;

@SuppressWarnings("all")
public class Sat4jSolver extends Solver {
  @Override
  public Solver.Result solveCNF(final String filePath) {
    try {
      Solver.Result _xblockexpression = null;
      {
        final ISolver solver = SolverFactory.newDefault();
        solver.setTimeout(3600);
        final Reader reader = new DimacsReader(solver);
        Solver.Result _xtrycatchfinallyexpression = null;
        try {
          Solver.Result _xblockexpression_1 = null;
          {
            final IProblem problem = reader.parseInstance(filePath);
            boolean _isSatisfiable = problem.isSatisfiable();
            int[] _model = problem.model();
            _xblockexpression_1 = new Solver.Result("sat4j", _isSatisfiable, _model);
          }
          _xtrycatchfinallyexpression = _xblockexpression_1;
        } catch (final Throwable _t) {
          if (_t instanceof ContradictionException) {
            Solver.Result _xblockexpression_2 = null;
            {
              System.out.println("Unsatisfiable (trivial)!");
              _xblockexpression_2 = new Solver.Result("sat4j", false);
            }
            _xtrycatchfinallyexpression = _xblockexpression_2;
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        _xblockexpression = _xtrycatchfinallyexpression;
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Override
  public Solver.Result solveCNF(final InputStream dimacs) {
    try {
      Solver.Result _xblockexpression = null;
      {
        final ISolver solver = SolverFactory.newDefault();
        solver.setTimeout(3600);
        final Reader reader = new DimacsReader(solver);
        Solver.Result _xtrycatchfinallyexpression = null;
        try {
          Solver.Result _xblockexpression_1 = null;
          {
            final IProblem problem = reader.parseInstance(dimacs);
            boolean _isSatisfiable = problem.isSatisfiable();
            int[] _model = problem.model();
            _xblockexpression_1 = new Solver.Result("sat4j", _isSatisfiable, _model);
          }
          _xtrycatchfinallyexpression = _xblockexpression_1;
        } catch (final Throwable _t) {
          if (_t instanceof ContradictionException) {
            Solver.Result _xblockexpression_2 = null;
            {
              System.out.println("Unsatisfiable (trivial)!");
              _xblockexpression_2 = new Solver.Result("sat4j", false);
            }
            _xtrycatchfinallyexpression = _xblockexpression_2;
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        _xblockexpression = _xtrycatchfinallyexpression;
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Override
  public Solver.Result solveCNF(final List<List<Integer>> a) {
    try {
      Solver.Result _xblockexpression = null;
      {
        final int MAXVAR = 1000000;
        final int NBCLAUSES = 500000;
        final ISolver solver = SolverFactory.newDefault();
        solver.newVar(MAXVAR);
        solver.setExpectedNumberOfClauses(NBCLAUSES);
        for (int i = 0; (i < NBCLAUSES); i++) {
          {
            final List<Integer> clause = a.get(i);
            VecInt _vecInt = new VecInt(((int[])Conversions.unwrapArray(clause, int.class)));
            solver.addClause(_vecInt);
          }
        }
        final IProblem problem = solver;
        boolean _isSatisfiable = problem.isSatisfiable();
        int[] _model = problem.model();
        _xblockexpression = new Solver.Result("sat4j", _isSatisfiable, _model);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
