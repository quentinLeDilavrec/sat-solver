package sat.solver;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import sat.cnf.Transform2CNF;
import sat.generator.PropositionalLogicLanguageGenerator;
import sat.propositionalLogicLanguage.File;
import sat.propositionalLogicLanguage.Proposition;
import sat.propositionalLogicLanguage.Using;
import sat.solver.minisat.MinisatSolver;
import sat.solver.picosat.PicosatSolver;
import sat.solver.sat4j.Sat4jSolver;

@SuppressWarnings("all")
public abstract class Solver {
  public static final class Result {
    private final boolean b;
    
    private final int[] model;
    
    private final String name;
    
    private HashMap<Integer, String> m;
    
    public Result(final String name, final boolean b, final int[] model) {
      this.b = b;
      this.name = name;
      this.model = model;
      this.m = null;
    }
    
    public Result(final String name, final boolean b) {
      this.b = b;
      this.name = name;
      this.model = null;
      this.m = null;
    }
    
    @Override
    public String toString() {
      String _xifexpression = null;
      if (this.b) {
        String _xifexpression_1 = null;
        if (((this.m == null) || (this.model == null))) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("YES\tgiven by ");
          _builder.append(this.name);
          _xifexpression_1 = _builder.toString();
        } else {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("YES\tgiven by ");
          _builder_1.append(this.name);
          _builder_1.append(" with model ");
          final Function1<Integer, String> _function = (Integer x) -> {
            String _xifexpression_2 = null;
            if (((x).intValue() < 0)) {
              String _get = this.m.get(Integer.valueOf(((-1) * (x).intValue())));
              _xifexpression_2 = ("¬" + _get);
            } else {
              _xifexpression_2 = this.m.get(x);
            }
            return _xifexpression_2;
          };
          String _join = IterableExtensions.join(ListExtensions.<Integer, String>map(((List<Integer>)Conversions.doWrapArray(this.model)), _function), " ");
          _builder_1.append(_join);
          _builder_1.newLineIfNotEmpty();
          _xifexpression_1 = _builder_1.toString();
        }
        _xifexpression = _xifexpression_1;
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("NO\tgiven by ");
        _builder_2.append(this.name);
        _xifexpression = _builder_2.toString();
      }
      return _xifexpression;
    }
    
    @Override
    public boolean equals(final Object o) {
      boolean _switchResult = false;
      boolean _matched = false;
      if (o instanceof Solver.Result) {
        _matched=true;
        _switchResult = (((Solver.Result)o).b == this.b);
      }
      if (!_matched) {
        _switchResult = false;
      }
      return _switchResult;
    }
  }
  
  private final static Sat4jSolver sat4j = new Sat4jSolver();
  
  private final static MinisatSolver minisat = new MinisatSolver();
  
  private final static PicosatSolver picosat = new PicosatSolver();
  
  private static final Solver assoc(final String name) {
    Solver _switchResult = null;
    if (name != null) {
      switch (name) {
        case "sat4j":
          _switchResult = Solver.sat4j;
          break;
        case "minisat":
          _switchResult = Solver.minisat;
          break;
        case "picosat":
          _switchResult = Solver.picosat;
          break;
        default:
          _switchResult = Solver.sat4j;
          break;
      }
    } else {
      _switchResult = Solver.sat4j;
    }
    return _switchResult;
  }
  
  public static final boolean compareSolvers(final File f) {
    final Function1<Proposition, Boolean> _function = (Proposition p) -> {
      boolean _xblockexpression = false;
      {
        final Function1<Using, Boolean> _function_1 = (Using u) -> {
          return Boolean.valueOf(Solver.assoc(u.getName()).solve(p).b);
        };
        final List<Boolean> l = ListExtensions.<Using, Boolean>map(f.getUsings(), _function_1);
        _xblockexpression = (IterableExtensions.<Boolean>forall(l, ((Function1<Boolean, Boolean>) (Boolean x) -> {
          return x;
        })) || IterableExtensions.<Boolean>forall(l, ((Function1<Boolean, Boolean>) (Boolean x) -> {
          return Boolean.valueOf((!(x).booleanValue()));
        })));
      }
      return Boolean.valueOf(_xblockexpression);
    };
    return IterableExtensions.<Proposition>forall(f.getContent(), _function);
  }
  
  public static final List<List<Solver.Result>> solve(final File f) {
    final Function1<Proposition, List<Solver.Result>> _function = (Proposition p) -> {
      List<Solver.Result> _xifexpression = null;
      int _length = ((Object[])Conversions.unwrapArray(f.getUsings(), Object.class)).length;
      boolean _equals = (_length == 0);
      if (_equals) {
        _xifexpression = CollectionLiterals.<Solver.Result>newArrayList(Solver.assoc("").solve(p));
      } else {
        final Function1<Using, Solver.Result> _function_1 = (Using u) -> {
          return Solver.assoc(u.getName()).solve(p);
        };
        _xifexpression = ListExtensions.<Using, Solver.Result>map(f.getUsings(), _function_1);
      }
      return _xifexpression;
    };
    return ListExtensions.<Proposition, List<Solver.Result>>map(f.getContent(), _function);
  }
  
  public final Solver.Result solve(final Proposition p) {
    Solver.Result _xblockexpression = null;
    {
      final Proposition cnf = new Transform2CNF().transform(p);
      final PropositionalLogicLanguageGenerator.DIMACSSerializer serializer = new PropositionalLogicLanguageGenerator.DIMACSSerializer();
      final Solver.Result res = this.solveCNF(serializer.serialize(cnf, "Default"));
      res.m = serializer.getVarMap();
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
  
  public Solver.Result solve(final CharSequence dimacs) {
    byte[] _bytes = dimacs.toString().getBytes();
    return this.solve(new ByteArrayInputStream(_bytes));
  }
  
  public Solver.Result solve(final InputStream dimacs) {
    return this.solveCNF(dimacs);
  }
  
  public Solver.Result solveCNF(final List<List<Integer>> a) {
    try {
      throw new Exception("Not Implemented");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public Solver.Result solveCNF(final CharSequence formulae) {
    byte[] _bytes = formulae.toString().getBytes();
    return this.solveCNF(new ByteArrayInputStream(_bytes));
  }
  
  public Solver.Result solveCNF(final InputStream dimacs) {
    try {
      throw new Exception("Not Implemented");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public Solver.Result solveCNF(final String filePath) {
    try {
      throw new Exception("Not Implemented");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
