package sat.solver.picosat;

import com.google.common.base.Objects;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import org.eclipse.xtext.xbase.lib.Exceptions;
import sat.solver.Solver;

@SuppressWarnings("all")
public class PicosatSolver extends Solver {
  @Override
  public Solver.Result solveCNF(final InputStream formula) {
    try {
      Solver.Result _xblockexpression = null;
      {
        InputStreamReader _inputStreamReader = new InputStreamReader(formula);
        String _collect = new BufferedReader(_inputStreamReader).lines().parallel().collect(
          Collectors.joining("\n"));
        String _plus = ("echo \"" + _collect);
        String _plus_1 = (_plus + "\" | /usr/bin/picosat | head -1");
        final Process process = Runtime.getRuntime().exec(
          new String[] { "/bin/sh", "-c", _plus_1 });
        process.waitFor();
        InputStream _inputStream = process.getInputStream();
        InputStreamReader _inputStreamReader_1 = new InputStreamReader(_inputStream);
        String _substring = new BufferedReader(_inputStreamReader_1).readLine().substring(2);
        boolean _equals = Objects.equal(_substring, "SATISFIABLE");
        _xblockexpression = new Solver.Result("minisat", _equals);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
