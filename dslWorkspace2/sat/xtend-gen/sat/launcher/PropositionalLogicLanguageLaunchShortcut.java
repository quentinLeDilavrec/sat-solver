package sat.launcher;

import com.google.common.base.Objects;
import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2;
import sat.PropositionalLogicLanguageStandaloneSetup;
import sat.propositionalLogicLanguage.File;
import sat.solver.Solver;

@SuppressWarnings("all")
public class PropositionalLogicLanguageLaunchShortcut implements ILaunchShortcut {
  private final static MessageConsoleStream stream = PropositionalLogicLanguageLaunchShortcut.makeConsoleStream("Solver");
  
  private static MessageConsoleStream makeConsoleStream(final String name) {
    try {
      final IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
      MessageConsole console = null;
      IConsole[] _consoles = manager.getConsoles();
      for (final IConsole e : _consoles) {
        String _name = e.getName();
        boolean _equals = Objects.equal(name, _name);
        if (_equals) {
          manager.showConsoleView(e);
          console = ((MessageConsole) e);
        }
      }
      if ((console == null)) {
        final MessageConsole newconsole = new MessageConsole(name, null);
        manager.addConsoles(new IConsole[] { newconsole });
        console = newconsole;
      }
      IViewPart _showView = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(
        IConsoleConstants.ID_CONSOLE_VIEW);
      ((IConsoleView) _showView).display(console);
      return console.newMessageStream();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Override
  public void launch(final ISelection selection, final String mode) {
    if ((selection instanceof IStructuredSelection)) {
      final IStructuredSelection structuredSelection = ((IStructuredSelection) selection);
      final Object object = structuredSelection.getFirstElement();
      if ((object instanceof IAdaptable)) {
        IResource _adapter = ((IAdaptable) object).<IResource>getAdapter(IResource.class);
        final IFile currFile = ((IFile) _adapter);
        InputStream in = null;
        try {
          in = currFile.getContents();
        } catch (final Throwable _t) {
          if (_t instanceof CoreException) {
            final CoreException e = (CoreException)_t;
            e.printStackTrace();
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        this.launch(in);
      }
    }
  }
  
  @Override
  public void launch(final IEditorPart editor, final String mode) {
    try {
      IFile _adapter = editor.getEditorInput().<IFile>getAdapter(IFile.class);
      final IFile currFile = ((IFile) _adapter);
      InputStream in = currFile.getContents();
      this.launch(in);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private void launch(final InputStream in) {
    try {
      final XtextResourceSet resourceSet = new PropositionalLogicLanguageStandaloneSetup().createInjectorAndDoEMFRegistration().<XtextResourceSet>getInstance(XtextResourceSet.class);
      resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
      final Resource resource = resourceSet.createResource(URI.createURI("dummy:/file.pll"));
      resource.load(in, resourceSet.getLoadOptions());
      EObject _get = resource.getContents().get(0);
      final File file = ((File) _get);
      try {
        PropositionalLogicLanguageLaunchShortcut.stream.println("---START SOLVING---");
        final Procedure2<List<Solver.Result>, Integer> _function = (List<Solver.Result> x, Integer i) -> {
          PropositionalLogicLanguageLaunchShortcut.stream.println((">>formula " + i));
          final Consumer<Solver.Result> _function_1 = (Solver.Result y) -> {
            PropositionalLogicLanguageLaunchShortcut.stream.println((">>formula " + i));
            PropositionalLogicLanguageLaunchShortcut.stream.println(y.toString());
          };
          x.forEach(_function_1);
        };
        IterableExtensions.<List<Solver.Result>>forEach(Solver.solve(file), _function);
        PropositionalLogicLanguageLaunchShortcut.stream.println("---FINISHED---");
      } catch (final Throwable _t) {
        if (_t instanceof Throwable) {
          final Throwable exception = (Throwable)_t;
          PropositionalLogicLanguageLaunchShortcut.stream.println(exception.toString());
          PropositionalLogicLanguageLaunchShortcut.stream.println(((List<StackTraceElement>)Conversions.doWrapArray(exception.getStackTrace())).toString());
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
