package sat.cnf;

import java.util.Arrays;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import sat.propositionalLogicLanguage.Biimplies;
import sat.propositionalLogicLanguage.Binary;
import sat.propositionalLogicLanguage.Conjunction;
import sat.propositionalLogicLanguage.Disjunction;
import sat.propositionalLogicLanguage.ExclusiveDisjunction;
import sat.propositionalLogicLanguage.Implies;
import sat.propositionalLogicLanguage.Negation;
import sat.propositionalLogicLanguage.NegativeConjunction;
import sat.propositionalLogicLanguage.NegativeDisjunction;
import sat.propositionalLogicLanguage.Primitive;
import sat.propositionalLogicLanguage.Proposition;
import sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage;

@SuppressWarnings("all")
public class Transform2CNF {
  private boolean changed;
  
  private final EcoreUtil.Copier copier = new EcoreUtil.Copier();
  
  public Transform2CNF() {
    this.changed = true;
  }
  
  public Proposition transform(final Proposition p) {
    Proposition _xblockexpression = null;
    {
      Proposition tmp = this.toCNF(p);
      while (this.changed) {
        {
          this.changed = false;
          tmp = this.toCNF(tmp);
        }
      }
      _xblockexpression = tmp;
    }
    return _xblockexpression;
  }
  
  public <T extends EObject> T copy(final T o) {
    EObject _copy = this.copier.copy(o);
    return ((T) _copy);
  }
  
  private final PropositionalLogicLanguagePackage pllEIntances = PropositionalLogicLanguagePackage.eINSTANCE;
  
  private final EFactory facto = this.pllEIntances.getEFactoryInstance();
  
  private Proposition negate(final Proposition p) {
    Negation _xblockexpression = null;
    {
      EObject _create = this.facto.create(this.pllEIntances.getNegation());
      final Negation tmp = ((Negation) _create);
      tmp.setContent(this.<Proposition>copy(p));
      _xblockexpression = tmp;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final Primitive e) {
    return e;
  }
  
  private Proposition _toCNF(final Implies e) {
    Disjunction _xblockexpression = null;
    {
      this.changed = true;
      EObject _create = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction disj = ((Disjunction) _create);
      disj.setLeft(this.negate(this.toCNF(this.<Proposition>copy(e.getLeft()))));
      disj.setRight(this.toCNF(this.<Proposition>copy(e.getRight())));
      _xblockexpression = disj;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final Biimplies e) {
    Conjunction _xblockexpression = null;
    {
      this.changed = true;
      final Proposition left = this.toCNF(this.<Proposition>copy(e.getLeft()));
      final Proposition right = this.toCNF(this.<Proposition>copy(e.getRight()));
      EObject _create = this.facto.create(this.pllEIntances.getConjunction());
      final Conjunction conj = ((Conjunction) _create);
      EObject _create_1 = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction disj1 = ((Disjunction) _create_1);
      conj.setLeft(disj1);
      disj1.setLeft(this.negate(this.<Proposition>copy(left)));
      disj1.setRight(this.<Proposition>copy(right));
      EObject _create_2 = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction disj2 = ((Disjunction) _create_2);
      conj.setRight(disj2);
      disj2.setLeft(this.negate(this.<Proposition>copy(right)));
      disj2.setRight(this.<Proposition>copy(left));
      _xblockexpression = conj;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final NegativeConjunction e) {
    Disjunction _xblockexpression = null;
    {
      this.changed = true;
      EObject _create = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction disj = ((Disjunction) _create);
      disj.setLeft(this.negate(this.toCNF(this.<Proposition>copy(e.getLeft()))));
      disj.setRight(this.negate(this.toCNF(this.<Proposition>copy(e.getRight()))));
      _xblockexpression = disj;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final NegativeDisjunction e) {
    Conjunction _xblockexpression = null;
    {
      this.changed = true;
      EObject _create = this.facto.create(this.pllEIntances.getConjunction());
      final Conjunction conj = ((Conjunction) _create);
      conj.setLeft(this.negate(this.toCNF(this.<Proposition>copy(e.getLeft()))));
      conj.setRight(this.negate(this.toCNF(this.<Proposition>copy(e.getRight()))));
      _xblockexpression = conj;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final ExclusiveDisjunction e) {
    Conjunction _xblockexpression = null;
    {
      this.changed = true;
      EObject _create = this.facto.create(this.pllEIntances.getConjunction());
      final Conjunction conj = ((Conjunction) _create);
      EObject _create_1 = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction disj1 = ((Disjunction) _create_1);
      conj.setLeft(disj1);
      disj1.setLeft(this.toCNF(this.<Proposition>copy(e.getLeft())));
      disj1.setRight(this.toCNF(this.<Proposition>copy(e.getRight())));
      EObject _create_2 = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction disj2 = ((Disjunction) _create_2);
      conj.setRight(disj2);
      disj2.setLeft(this.negate(this.toCNF(this.<Proposition>copy(e.getLeft()))));
      disj2.setRight(this.negate(this.toCNF(this.<Proposition>copy(e.getRight()))));
      _xblockexpression = conj;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final Disjunction e) {
    Binary _xblockexpression = null;
    {
      EObject _create = this.facto.create(this.pllEIntances.getDisjunction());
      final Disjunction r = ((Disjunction) _create);
      r.setLeft(this.toCNF(this.<Proposition>copy(e.getLeft())));
      r.setRight(this.toCNF(this.<Proposition>copy(e.getRight())));
      final Proposition left = r.getLeft();
      Binary _switchResult = null;
      boolean _matched = false;
      if (left instanceof Disjunction) {
        _matched=true;
        Binary _xblockexpression_1 = null;
        {
          final Proposition right = r.getRight();
          Binary _switchResult_1 = null;
          boolean _matched_1 = false;
          if (right instanceof Conjunction) {
            _matched_1=true;
            Conjunction _xblockexpression_2 = null;
            {
              this.changed = true;
              EObject _create_1 = this.facto.create(this.pllEIntances.getConjunction());
              final Conjunction conj = ((Conjunction) _create_1);
              EObject _create_2 = this.facto.create(this.pllEIntances.getDisjunction());
              final Disjunction disj1 = ((Disjunction) _create_2);
              conj.setLeft(disj1);
              disj1.setLeft(this.<Disjunction>copy(((Disjunction)left)));
              disj1.setRight(this.<Proposition>copy(((Conjunction)right).getLeft()));
              EObject _create_3 = this.facto.create(this.pllEIntances.getDisjunction());
              final Disjunction disj2 = ((Disjunction) _create_3);
              conj.setRight(disj2);
              disj2.setLeft(left);
              disj2.setRight(this.<Proposition>copy(((Conjunction)right).getRight()));
              _xblockexpression_2 = conj;
            }
            _switchResult_1 = _xblockexpression_2;
          }
          if (!_matched_1) {
            Disjunction _xblockexpression_2 = null;
            {
              this.changed = true;
              final Proposition leftright = this.<Proposition>copy(((Disjunction)left).getRight());
              ((Disjunction)left).setRight(r);
              r.setLeft(leftright);
              _xblockexpression_2 = ((Disjunction)left);
            }
            _switchResult_1 = _xblockexpression_2;
          }
          _xblockexpression_1 = _switchResult_1;
        }
        _switchResult = _xblockexpression_1;
      }
      if (!_matched) {
        if (left instanceof Conjunction) {
          _matched=true;
          Conjunction _xblockexpression_1 = null;
          {
            final Proposition right = r.getRight();
            Conjunction _switchResult_1 = null;
            boolean _matched_1 = false;
            if (right instanceof Conjunction) {
              _matched_1=true;
              Conjunction _xblockexpression_2 = null;
              {
                this.changed = true;
                EObject _create_1 = this.facto.create(this.pllEIntances.getConjunction());
                final Conjunction conj1 = ((Conjunction) _create_1);
                EObject _create_2 = this.facto.create(this.pllEIntances.getDisjunction());
                final Disjunction disj1 = ((Disjunction) _create_2);
                conj1.setLeft(disj1);
                disj1.setLeft(this.<Proposition>copy(((Conjunction)left).getLeft()));
                disj1.setRight(this.<Proposition>copy(((Conjunction)right).getLeft()));
                EObject _create_3 = this.facto.create(this.pllEIntances.getConjunction());
                final Conjunction conj2 = ((Conjunction) _create_3);
                conj1.setRight(conj2);
                EObject _create_4 = this.facto.create(this.pllEIntances.getDisjunction());
                final Disjunction disj2 = ((Disjunction) _create_4);
                conj2.setLeft(disj2);
                disj2.setLeft(this.<Proposition>copy(((Conjunction)left).getLeft()));
                disj2.setRight(this.<Proposition>copy(((Conjunction)right).getRight()));
                EObject _create_5 = this.facto.create(this.pllEIntances.getConjunction());
                final Conjunction conj3 = ((Conjunction) _create_5);
                conj2.setRight(conj3);
                EObject _create_6 = this.facto.create(this.pllEIntances.getDisjunction());
                final Disjunction disj3 = ((Disjunction) _create_6);
                conj3.setLeft(disj3);
                disj3.setLeft(this.<Proposition>copy(((Conjunction)left).getRight()));
                disj3.setRight(this.<Proposition>copy(((Conjunction)right).getLeft()));
                EObject _create_7 = this.facto.create(this.pllEIntances.getDisjunction());
                final Disjunction disj4 = ((Disjunction) _create_7);
                conj3.setRight(disj4);
                disj4.setLeft(this.<Proposition>copy(((Conjunction)left).getRight()));
                disj4.setRight(this.<Proposition>copy(((Conjunction)right).getRight()));
                _xblockexpression_2 = conj1;
              }
              _switchResult_1 = _xblockexpression_2;
            }
            if (!_matched_1) {
              Conjunction _xblockexpression_2 = null;
              {
                this.changed = true;
                EObject _create_1 = this.facto.create(this.pllEIntances.getConjunction());
                final Conjunction conj = ((Conjunction) _create_1);
                EObject _create_2 = this.facto.create(this.pllEIntances.getDisjunction());
                final Disjunction disj1 = ((Disjunction) _create_2);
                conj.setLeft(disj1);
                disj1.setLeft(this.<Proposition>copy(((Conjunction)left).getLeft()));
                disj1.setRight(this.<Proposition>copy(right));
                EObject _create_3 = this.facto.create(this.pllEIntances.getDisjunction());
                final Disjunction disj2 = ((Disjunction) _create_3);
                conj.setRight(disj2);
                disj2.setLeft(this.<Proposition>copy(((Conjunction)left).getRight()));
                disj2.setRight(this.<Proposition>copy(right));
                _xblockexpression_2 = conj;
              }
              _switchResult_1 = _xblockexpression_2;
            }
            _xblockexpression_1 = _switchResult_1;
          }
          _switchResult = _xblockexpression_1;
        }
      }
      if (!_matched) {
        if (left instanceof Negation) {
          _matched=true;
          _switchResult = r;
        }
      }
      if (!_matched) {
        if (left instanceof Primitive) {
          _matched=true;
          _switchResult = r;
        }
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final Conjunction e) {
    Conjunction _xblockexpression = null;
    {
      EObject _create = this.facto.create(this.pllEIntances.getConjunction());
      final Conjunction r = ((Conjunction) _create);
      r.setLeft(this.toCNF(this.<Proposition>copy(e.getLeft())));
      r.setRight(this.toCNF(this.<Proposition>copy(e.getRight())));
      final Proposition left = r.getLeft();
      Conjunction _switchResult = null;
      boolean _matched = false;
      if (left instanceof Conjunction) {
        _matched=true;
        Conjunction _xblockexpression_1 = null;
        {
          this.changed = true;
          final Proposition leftright = this.<Proposition>copy(((Conjunction)left).getRight());
          ((Conjunction)left).setRight(r);
          r.setLeft(leftright);
          _xblockexpression_1 = ((Conjunction)left);
        }
        _switchResult = _xblockexpression_1;
      }
      if (!_matched) {
        if (left instanceof Negation) {
          _matched=true;
          _switchResult = r;
        }
      }
      if (!_matched) {
        if (left instanceof Disjunction) {
          _matched=true;
          _switchResult = r;
        }
      }
      if (!_matched) {
        if (left instanceof Primitive) {
          _matched=true;
          _switchResult = r;
        }
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  private Proposition _toCNF(final Negation e) {
    Proposition _xblockexpression = null;
    {
      final Proposition tmp = this.toCNF(this.<Proposition>copy(e.getContent()));
      Proposition _switchResult = null;
      boolean _matched = false;
      if (tmp instanceof Negation) {
        _matched=true;
        Proposition _xblockexpression_1 = null;
        {
          this.changed = true;
          _xblockexpression_1 = ((Negation)tmp).getContent();
        }
        _switchResult = _xblockexpression_1;
      }
      if (!_matched) {
        if (tmp instanceof Disjunction) {
          _matched=true;
          Conjunction _xblockexpression_1 = null;
          {
            this.changed = true;
            EObject _create = this.facto.create(this.pllEIntances.getConjunction());
            final Conjunction conj = ((Conjunction) _create);
            conj.setLeft(this.negate(this.<Proposition>copy(((Disjunction)tmp).getLeft())));
            conj.setRight(this.negate(this.<Proposition>copy(((Disjunction)tmp).getRight())));
            _xblockexpression_1 = conj;
          }
          _switchResult = _xblockexpression_1;
        }
      }
      if (!_matched) {
        if (tmp instanceof Conjunction) {
          _matched=true;
          Disjunction _xblockexpression_1 = null;
          {
            this.changed = true;
            EObject _create = this.facto.create(this.pllEIntances.getDisjunction());
            final Disjunction disj = ((Disjunction) _create);
            disj.setLeft(this.negate(this.<Proposition>copy(((Conjunction)tmp).getLeft())));
            disj.setRight(this.negate(this.<Proposition>copy(((Conjunction)tmp).getRight())));
            _xblockexpression_1 = disj;
          }
          _switchResult = _xblockexpression_1;
        }
      }
      if (!_matched) {
        if (tmp instanceof Primitive) {
          _matched=true;
          _switchResult = this.<Negation>copy(e);
        }
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  private Proposition toCNF(final Proposition e) {
    if (e instanceof Biimplies) {
      return _toCNF((Biimplies)e);
    } else if (e instanceof Conjunction) {
      return _toCNF((Conjunction)e);
    } else if (e instanceof Disjunction) {
      return _toCNF((Disjunction)e);
    } else if (e instanceof ExclusiveDisjunction) {
      return _toCNF((ExclusiveDisjunction)e);
    } else if (e instanceof Implies) {
      return _toCNF((Implies)e);
    } else if (e instanceof Negation) {
      return _toCNF((Negation)e);
    } else if (e instanceof NegativeConjunction) {
      return _toCNF((NegativeConjunction)e);
    } else if (e instanceof NegativeDisjunction) {
      return _toCNF((NegativeDisjunction)e);
    } else if (e instanceof Primitive) {
      return _toCNF((Primitive)e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(e).toString());
    }
  }
}
