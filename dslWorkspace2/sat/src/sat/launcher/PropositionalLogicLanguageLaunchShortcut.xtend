package sat.launcher

import org.eclipse.debug.ui.ILaunchShortcut
import org.eclipse.ui.IEditorPart
import org.eclipse.jface.viewers.ISelection
import org.eclipse.ui.console.MessageConsoleStream
import org.eclipse.ui.console.ConsolePlugin
import org.eclipse.ui.console.MessageConsole
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.console.IConsoleConstants
import org.eclipse.ui.console.IConsoleView
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.core.runtime.IAdaptable
import org.eclipse.core.resources.IResource
import java.io.InputStream
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.resources.IFile
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.emf.common.util.URI
import sat.propositionalLogicLanguage.File
import sat.PropositionalLogicLanguageStandaloneSetup

class PropositionalLogicLanguageLaunchShortcut implements ILaunchShortcut {

	static val MessageConsoleStream stream = makeConsoleStream("Solver")

	private static def MessageConsoleStream makeConsoleStream(String name) {
		val manager = ConsolePlugin.^default.consoleManager
		var MessageConsole console
		for (e : manager.consoles) {
			if (name == e.name) {
				manager.showConsoleView(e)
				console = e as MessageConsole
			}
		}
		if (console === null) {
			val newconsole = new MessageConsole(name, null)
			manager.addConsoles(#[newconsole])
			console = newconsole
		}
		(PlatformUI.workbench.activeWorkbenchWindow.activePage.showView(
			IConsoleConstants.ID_CONSOLE_VIEW) as IConsoleView).display(console)
		return console.newMessageStream()
	}

	override void launch(ISelection selection, String mode) {
		if (selection instanceof IStructuredSelection) {
			val IStructuredSelection structuredSelection = (selection as IStructuredSelection)
			val Object object = structuredSelection.getFirstElement()
			if (object instanceof IAdaptable) {
				val currFile = ((object as IAdaptable)).getAdapter(IResource) as IFile
				var InputStream in = null
				try {
					in = currFile.getContents()
				} catch (CoreException e) {
					e.printStackTrace()
				}
				in.launch
			}
		}
	}

	override void launch(IEditorPart editor, String mode) {
		val currFile = editor.getEditorInput().getAdapter(IFile) as IFile
		var InputStream in = currFile.getContents()
		in.launch
	}

	private def launch(InputStream in) {
		val resourceSet = (new PropositionalLogicLanguageStandaloneSetup())
		.createInjectorAndDoEMFRegistration().getInstance(typeof(XtextResourceSet))
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
		
		val resource = resourceSet.createResource(URI.createURI("dummy:/file.pll"))
		
		resource.load(in, resourceSet.getLoadOptions())
		
		val file = resource.contents.get(0) as File

		try {
			stream.println("---START SOLVING---")
			sat.solver.Solver.solve(file).forEach [ x,i |
				stream.println(">>formula "+i)
				x.forEach [ y |
					stream.println(">>formula "+i)
					stream.println(y.toString)
				]
			]
			stream.println("---FINISHED---")
		} catch (Throwable exception) {
			stream.println(exception.toString)
			stream.println(exception.stackTrace.toString)
		}
	}
}
