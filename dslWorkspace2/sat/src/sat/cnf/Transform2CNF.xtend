package sat.cnf

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.HashMap
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
//import org.eclipse.m2m.atl.core.ATLCoreException
//import org.eclipse.m2m.atl.core.IExtractor
//import org.eclipse.m2m.atl.core.IInjector
//import org.eclipse.m2m.atl.core.IModel
//import org.eclipse.m2m.atl.core.IReferenceModel
//import org.eclipse.m2m.atl.core.ModelFactory
//import org.eclipse.m2m.atl.core.emf.EMFExtractor
//import org.eclipse.m2m.atl.core.emf.EMFInjector
//import org.eclipse.m2m.atl.core.emf.EMFModel
//import org.eclipse.m2m.atl.core.emf.EMFModelFactory
//import org.eclipse.m2m.atl.core.emf.EMFReferenceModel
//import org.eclipse.m2m.atl.core.launch.ILauncher
//import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher
import sat.propositionalLogicLanguage.Biimplies
import sat.propositionalLogicLanguage.Disjunction
import sat.propositionalLogicLanguage.ExclusiveDisjunction
import sat.propositionalLogicLanguage.Implies
import sat.propositionalLogicLanguage.NegativeConjunction
import sat.propositionalLogicLanguage.NegativeDisjunction
import sat.propositionalLogicLanguage.Proposition
import sat.propositionalLogicLanguage.Conjunction
import sat.propositionalLogicLanguage.Primitive
import sat.propositionalLogicLanguage.Negation
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage

/**
 * 
 */
class Transform2CNF {

		boolean changed

		val copier = new EcoreUtil.Copier

		new() {
			changed = true
		}

		def Proposition transform(Proposition p) {
			var tmp = p.toCNF
			while (changed) {
				changed = false
				tmp = tmp.toCNF
			}
			tmp
		}

		def <T extends EObject> copy(T o) {
			copier.copy(o) as T
		}

		// used to get Propositions
		val pllEIntances = PropositionalLogicLanguagePackage.eINSTANCE
		val facto = pllEIntances.EFactoryInstance

		private def Proposition negate(Proposition p) {
			val tmp = facto.create(pllEIntances.negation) as Negation
			tmp.content = p.copy
			tmp
		}

		private def dispatch Proposition toCNF(Primitive e) {
			e
		}

		private def dispatch Proposition toCNF(Implies e) {
			changed = true
			val disj = facto.create(pllEIntances.disjunction) as Disjunction
			disj.left = e.left.copy.toCNF.negate
			disj.right = e.right.copy.toCNF
			disj
		}

		private def dispatch Proposition toCNF(Biimplies e) {
			changed = true
			val left = e.left.copy.toCNF
			val right = e.right.copy.toCNF
			val conj = facto.create(pllEIntances.getConjunction) as Conjunction
			val disj1 = facto.create(pllEIntances.disjunction) as Disjunction
			conj.left = disj1
			disj1.left = left.copy.negate
			disj1.right = right.copy
			val disj2 = facto.create(pllEIntances.disjunction) as Disjunction
			conj.right = disj2
			disj2.left = right.copy.negate
			disj2.right = left.copy
			conj
		}

		private def dispatch Proposition toCNF(NegativeConjunction e) {
			changed = true
			val disj = facto.create(pllEIntances.disjunction) as Disjunction
			disj.left = e.left.copy.toCNF.negate
			disj.right = e.right.copy.toCNF.negate
			disj
		}

		private def dispatch Proposition toCNF(NegativeDisjunction e) {
			changed = true
			val conj = facto.create(pllEIntances.conjunction) as Conjunction
			conj.left = e.left.copy.toCNF.negate
			conj.right = e.right.copy.toCNF.negate
			conj
		}

		private def dispatch Proposition toCNF(ExclusiveDisjunction e) {
			changed = true
			val conj = facto.create(pllEIntances.conjunction) as Conjunction
			val disj1 = facto.create(pllEIntances.disjunction) as Disjunction
			conj.left = disj1
			disj1.left = e.left.copy.toCNF
			disj1.right = e.right.copy.toCNF
			val disj2 = facto.create(pllEIntances.disjunction) as Disjunction
			conj.right = disj2
			disj2.left = e.left.copy.toCNF.negate
			disj2.right = e.right.copy.toCNF.negate
			conj
		}

		private def dispatch Proposition toCNF(Disjunction e) {
			val r = facto.create(pllEIntances.disjunction) as Disjunction
			r.left = e.left.copy.toCNF
			r.right = e.right.copy.toCNF
			val left = r.left
			switch left {
				Disjunction: {
					val right = r.right
					switch right {
						Conjunction: {
							changed = true
							val conj = facto.create(pllEIntances.conjunction) as Conjunction

							val disj1 = facto.create(pllEIntances.disjunction) as Disjunction
							conj.left = disj1
							disj1.left = left.copy
							disj1.right = right.left.copy
							val disj2 = facto.create(pllEIntances.disjunction) as Disjunction
							conj.right = disj2
							disj2.left = left
							disj2.right = right.right.copy

							conj
						}
						default: {
							changed = true
							val leftright = left.right.copy;
							left.right = r
							r.left = leftright
							left
						}
					}
				}
				Conjunction: {
					val right = r.right
					switch right {
						Conjunction: {
							changed = true
							val conj1 = facto.create(pllEIntances.conjunction) as Conjunction

							val disj1 = facto.create(pllEIntances.disjunction) as Disjunction
							conj1.left = disj1
							disj1.left = left.left.copy
							disj1.right = right.left.copy

							val conj2 = facto.create(pllEIntances.conjunction) as Conjunction
							conj1.right = conj2
							val disj2 = facto.create(pllEIntances.disjunction) as Disjunction
							conj2.left = disj2
							disj2.left = left.left.copy
							disj2.right = right.right.copy

							val conj3 = facto.create(pllEIntances.conjunction) as Conjunction
							conj2.right = conj3
							val disj3 = facto.create(pllEIntances.disjunction) as Disjunction
							conj3.left = disj3
							disj3.left = left.right.copy
							disj3.right = right.left.copy
							val disj4 = facto.create(pllEIntances.disjunction) as Disjunction
							conj3.right = disj4
							disj4.left = left.right.copy
							disj4.right = right.right.copy

							conj1
						}
						default: {
							changed = true
							val conj = facto.create(pllEIntances.conjunction) as Conjunction

							val disj1 = facto.create(pllEIntances.disjunction) as Disjunction
							conj.left = disj1
							disj1.left = left.left.copy
							disj1.right = right.copy
							val disj2 = facto.create(pllEIntances.disjunction) as Disjunction
							conj.right = disj2
							disj2.left = left.right.copy
							disj2.right = right.copy

							conj
						}
					}
				}
				Negation:
					r
				Primitive:
					r
			}
		}

		private def dispatch Proposition toCNF(Conjunction e) {
			val r = facto.create(pllEIntances.conjunction) as Conjunction
			r.left = e.left.copy.toCNF
			r.right = e.right.copy.toCNF
			val left = r.left
			switch left {
				Conjunction: {
					changed = true
					val leftright = left.right.copy;
					left.right = r
					r.left = leftright
					left
				}
				Negation:
					r
				Disjunction:
					r
				Primitive:
					r
			}
		}

		private def dispatch Proposition toCNF(Negation e) {
			val tmp = e.content.copy.toCNF
			switch tmp {
				Negation: {
					changed = true
					tmp.content
				}
				Disjunction: {
					changed = true
					val conj = facto.create(pllEIntances.conjunction) as Conjunction
					conj.left = tmp.left.copy.negate
					conj.right = tmp.right.copy.negate
					conj
				}
				Conjunction: {
					changed = true
					val disj = facto.create(pllEIntances.disjunction) as Disjunction
					disj.left = tmp.left.copy.negate
					disj.right = tmp.right.copy.negate
					disj
				}
				Primitive:
					e.copy
			}
		}
//	def static Resource transform(Resource res) {
////		try {
//			/*
//			 * Initializations
//			 */
//			val ILauncher transformationLauncher = new EMFVMLauncher()
//			val EMFModelFactory modelFactory = new EMFModelFactory()
//			val EMFInjector injector = new EMFInjector()
//			//val EMFExtractor extractor = new EMFExtractor()
//
//			/*
//			 * Load metamodels
//			 */
//			val IReferenceModel pllMetamodel = modelFactory.newReferenceModel()
//			System.err.println("Working Directory = " +
//              System.getProperty("user.dir"));
//            //if(true)
//            //throw new NullPointerException("Working Directory = " +
//            //res.URI.toString); 
//            val file = URI.createURI("platform:/sat/model/custom/PropositionalLogicLanguage.ecore").toFileString
//			injector.inject(pllMetamodel, "resources:/model/custom/PropositionalLogicLanguage.ecore")
//
//			/*
//			 * Run "Cut" transformation
//			 */
//			val IModel pllModel = modelFactory.newModel(pllMetamodel)
//			injector.inject(pllModel, res)
//			transformationLauncher.initialize(new HashMap<String, Object>())
//			transformationLauncher.addInOutModel(pllModel, "IN", "PLL")
//			transformationLauncher.addOutModel(pllModel, "OUT", "CNF")
//			transformationLauncher.launch(ILauncher.RUN_MODE, new NullProgressMonitor(), new HashMap<String, Object>())//,
////				new FileInputStream("Transformations/Cut.asm"))
//
//			(pllModel as EMFModel).resource
//
////			val IModel companyModel_Cut = pllModel
////			extractor.extract(companyModel_Cut, "Models/Java/sampleCompany_Cut.xmi")
//
//			/*
//			 * Run "ComputeTotal" transformation
//			 */
////			val IModel companyModel_Total = modelFactory.newModel(totalMetamodel)
////
////			transformationLauncher.initialize(new HashMap<String, Object>())
////			transformationLauncher.addInModel(companyModel_Cut, "IN", "Company")
////			transformationLauncher.addOutModel(companyModel_Total, "OUT", "Total")
////			transformationLauncher.launch(ILauncher.RUN_MODE, new NullProgressMonitor(), new HashMap<String, Object>(),
////				new FileInputStream("Transformations/ComputeTotal.asm"))
////
////			extractor.extract(companyModel_Total, "Models/Java/sampleCompany_Total.xmi")
//
//			/*
//			 * Unload all models and metamodels (EMF-specific)
//			 */
////			val EMFModelFactory emfModelFactory = modelFactory as EMFModelFactory
////			emfModelFactory.unload(companyModel_Cut as EMFModel)
////			emfModelFactory.unload(companyModel_Total as EMFModel)
////			emfModelFactory.unload(pllMetamodel as EMFReferenceModel)
////			emfModelFactory.unload(totalMetamodel as EMFReferenceModel)
//			
//			
//
////		} catch (ATLCoreException e) {
////			e.printStackTrace()
////		} catch (FileNotFoundException e) {
////			e.printStackTrace()
////		}
//	}
}
