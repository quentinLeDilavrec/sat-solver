/*
 * generated by Xtext 2.15.0
 */
package sat.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import sat.propositionalLogicLanguage.Primitive
import sat.propositionalLogicLanguage.Binary
import sat.propositionalLogicLanguage.Unary
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import sat.propositionalLogicLanguage.Proposition
import org.eclipse.emf.ecore.EClass
import sat.propositionalLogicLanguage.Biimplies
import sat.propositionalLogicLanguage.Implies
import sat.propositionalLogicLanguage.ExclusiveDisjunction
import sat.propositionalLogicLanguage.NegativeDisjunction
import sat.propositionalLogicLanguage.Disjunction
import sat.propositionalLogicLanguage.NegativeConjunction
import sat.propositionalLogicLanguage.Conjunction
import sat.propositionalLogicLanguage.Negation
import java.util.Map
import java.util.HashMap
import sat.propositionalLogicLanguage.util.PropositionalLogicLanguageAdapterFactory
import sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage
import sat.cnf.Transform2CNF
import org.eclipse.emf.ecore.EObject
import sat.propositionalLogicLanguage.File

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class PropositionalLogicLanguageGenerator extends AbstractGenerator {

	static class PLLSerializer {

		static def String toSymbol(EClass e) {
			switch e.name {
				case "Biimplies": '↔'
				case "Implies": '⟶'
				case "ExclusiveDisjunction": '⨂'
				case "NegativeDisjunction": '↓'
				case "Disjunction": '∨'
				case "NegativeConjunction": '↑'
				case "Conjunction": '∧'
				case "Negation": '¬'
				default: e.toString
//				case True: '⟙'
//				case False: '⟘'
			}
		}

		static def dispatch String toPLL(Primitive e) {
			e.name
		}

		static def dispatch String toPLL(Binary e) {
			e.left.toPLL(e.eClass) + ' ' + e.eClass.toSymbol + ' ' + e.right.toPLL(e.eClass)
		}

		static def dispatch String toPLL(Binary e, EClass p) {
			if (e.eClass.name == p.name)
				e.toPLL
			else
				'(' + e.toPLL + ')'
		}

		static def dispatch String toPLL(Proposition e, EClass p) {
			e.toPLL
		}

		static def dispatch String toPLL(Unary e) {
			e.eClass.toSymbol + ' ' + e.content.toPLL(e.eClass)
		}

	}

	static def String toPLL(Proposition p) {
		PLLSerializer::toPLL(p)
	}

	// DIMACS
	static class DIMACSSerializer {

		Integer c = 0
		Integer v = 0
		Map<String, Integer> m

		def getVarMap() {
			val dict = new HashMap<Integer, String>()
			for (p : m.entrySet) {
				dict+=(p.value -> p.key)
			}
			dict
		}

		new() {
			c = 0
			v = 0
			m = new HashMap<String, Integer>()
		}

		def serialize(Proposition p, String name) {
			c++
			val corps = c + ' ' + p.toDIMACS + ' 0'
			'''
				c «name»_v«v»_c«c».cnf
				«FOR e : m.entrySet»
					c «e.key» «e.value»
				«ENDFOR»
				p cnf «v» «c»
				«corps»
			'''
		}

		def dispatch String toDIMACS(Primitive e) {
			if (!m.containsKey(e.name)) {
				v++
				m += (e.name -> v)
				v.toString
			} else {
				m.get(e.name).toString
			}
		}

		def dispatch String toDIMACS(Conjunction e) {
			c++
			val tmp = c
			e.left.toDIMACS + ' 0\n' + tmp + ' ' + e.right.toDIMACS
		}

		def dispatch String toDIMACS(Disjunction e) {
			e.left.toDIMACS + ' ' + e.right.toDIMACS
		}

		def dispatch String toDIMACS(Negation e) {
			'-' + e.content.toDIMACS
		}
	}

	static def toDIMACS(Proposition p, String name) {
		(new DIMACSSerializer).serialize(p, name)
	}

	private static def Proposition toCNF(Proposition p) {
		(new Transform2CNF).transform(p)
	}

	// implemented generator
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		val name = resource.URI.lastSegment.split('\\.').get(0)
		val file = resource.contents.head as File
		file.content.forEach [ ast, i |
			fsa.generateFile('''«name».«i».pll''', ast.toPLL)
			val tmp = ast.toCNF
			fsa.generateFile('''«name».«i».cnf.pll''', tmp.toPLL)
			fsa.generateFile('''«name».«i».cnf''', tmp.toDIMACS(name))
		]
	}

}
