package sat.solver.sat4j

import org.sat4j.specs.ISolver
import org.sat4j.minisat.SolverFactory
import org.sat4j.reader.DimacsReader
import org.sat4j.reader.Reader
import java.io.PrintWriter
import org.sat4j.specs.IProblem
import java.io.FileNotFoundException
import org.sat4j.reader.ParseFormatException
import java.io.IOException
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException
import java.io.InputStream
import java.util.List
import org.sat4j.core.VecInt
import sat.launcher.PropositionalLogicLanguageLaunchShortcut

class Sat4jSolver extends sat.solver.Solver{
	override Result solveCNF(String filePath) {
		val ISolver solver = SolverFactory.newDefault()
		solver.setTimeout(3600) // 1 hour timeout
		val Reader reader = new DimacsReader(solver)
		// CNF filename is given on the command line 
		try {
			val IProblem problem = reader.parseInstance(filePath)
			new Result("sat4j",problem.isSatisfiable(),problem.model())
		} catch (ContradictionException e) {
			System.out.println("Unsatisfiable (trivial)!")
			new Result("sat4j",false)
		} 
//		catch (FileNotFoundException e) {
//			new Result(false)
//		} catch (ParseFormatException e) {
//			new Result(false)
//		} catch (IOException e) {
//			new Result(false)
//		catch (TimeoutException e) {
//			System.out.println("Timeout, sorry!");
//			new Result(false)
//		}
	}

	override Result solveCNF(InputStream dimacs) {
		val ISolver solver = SolverFactory.newDefault
		solver.setTimeout(3600)
		val Reader reader = new DimacsReader(solver);
		// CNF filename is given on the command line 
		try {
			val IProblem problem = reader.parseInstance(dimacs);
			new Result("sat4j",problem.isSatisfiable(),problem.model())
		} catch (ContradictionException e) {
			System.out.println("Unsatisfiable (trivial)!");
			new Result("sat4j",false)
		}
//		catch (FileNotFoundException e) {
//			new Result(false)
//			// TODO Auto-generated catch block
//		} catch (ParseFormatException e) {
//			new Result(false)
//			// TODO Auto-generated catch block
//		} catch (IOException e) {
//			new Result(false)
//			// TODO Auto-generated catch block
//		}
//catch (TimeoutException e) {
//			System.out.println("Timeout, sorry!");
//			new Result(false)
//		}
	}

	override Result solveCNF(List<List<Integer>> a) {
		val int MAXVAR = 1000000;
		val int NBCLAUSES = 500000;

		val ISolver solver = SolverFactory.newDefault();

		// prepare the solver to accept MAXVAR variables. MANDATORY for MAXSAT solving
		solver.newVar(MAXVAR);
		solver.setExpectedNumberOfClauses(NBCLAUSES);
		// Feed the solver using Dimacs format, using arrays of int
		// (best option to avoid dependencies on SAT4J IVecInt)
		for (var i = 0; i < NBCLAUSES; i++) {
			val clause = a.get(i) // get the clause from somewhere
			// the clause should not contain a 0, only integer (positive or negative)
			// with absolute values less or equal to MAXVAR
			// e.g. int [] clause = {1, -3, 7}; is fine
			// while int [] clause = {1, -3, 7, 0}; is not fine 
			solver.addClause(new VecInt(clause)); // adapt Array to IVecInt
		}

		// we are done. Working now on the IProblem interface
		val IProblem problem = solver;
		new Result("sat4j",problem.isSatisfiable(),problem.model())
	}
}
