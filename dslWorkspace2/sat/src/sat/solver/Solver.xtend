package sat.solver

import org.sat4j.specs.ISolver
import org.sat4j.maxsat.SolverFactory
import org.sat4j.reader.DimacsReader
import org.sat4j.reader.Reader
import java.io.PrintWriter
import org.sat4j.specs.IProblem
import java.io.FileNotFoundException
import org.sat4j.reader.ParseFormatException
import java.io.IOException
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException
import java.io.InputStream
import java.util.List
import org.sat4j.core.VecInt
import sat.propositionalLogicLanguage.Proposition
import sat.propositionalLogicLanguage.File
import sat.cnf.Transform2CNF
import sat.generator.PropositionalLogicLanguageGenerator.DIMACSSerializer
import java.util.stream.Stream
import java.io.SequenceInputStream
import java.io.ByteArrayInputStream
import sat.solver.sat4j.Sat4jSolver
import java.util.Map
import java.util.HashMap
import sat.solver.minisat.MinisatSolver
import sat.solver.picosat.PicosatSolver

abstract class Solver {

	static final class Result {
		val boolean b
		val int[] model
		val String name
	    var HashMap<Integer,String> m
		
		new(String name, boolean b, int[] model) {
			this.b = b
			this.name = name
			this.model = model
			this.m = null
		}

		new(String name, boolean b) {
			this.b = b
			this.name = name
			this.model = null
			this.m = null
		}

		override toString() {
			if (this.b) {
				if(this.m===null || this.model===null){
					'''YES	given by «name»'''
				}
				else{
					'''
					YES	given by «name» with model «
					this.model.map[x|
					if(x<0) "¬"+m.get(-1*x)
					else m.get(x)
					].join(" ")»
					'''
				}
			} else {
				'''NO	given by «name»'''
			}
		}

		override equals(Object o) {
			switch o {
				Result:
					o.b == this.b
				default:
					false
			}
		}
	}

	static val sat4j = new Sat4jSolver
	static val minisat = new MinisatSolver
	static val picosat = new PicosatSolver

	private static final def assoc(String name) {
		switch name {
			case "sat4j": sat4j
			case "minisat": minisat
			case "picosat": picosat
			default: sat4j
		}
	}

	static final def boolean compareSolvers(File f) {
		f.content.forall [ p |
			val l = f.usings.map[u|u.name.assoc.solve(p).b]
			l.forall[x|x] || l.forall[x|!x]
		]
	}

	static final def List<List<Result>> solve(File f) {
		f.content.map [ p |
			if (f.usings.length == 0)
				newArrayList("".assoc.solve(p))
			else
				f.usings.map[u| u.name.assoc.solve(p)]
		]
	}

	def final Result solve(Proposition p) {
		val cnf = (new Transform2CNF).transform(p)
		val serializer = (new DIMACSSerializer)
		val res = serializer.serialize(cnf, "Default").solveCNF
		res.m = serializer.varMap
		res
	}

	def Result solve(CharSequence dimacs) {
		(new ByteArrayInputStream(dimacs.toString.getBytes())).solve
	}

	def Result solve(InputStream dimacs) {
		dimacs.solveCNF
	}

	def Result solveCNF(List<List<Integer>> a) {
		throw new Exception("Not Implemented")
	}

	def Result solveCNF(CharSequence formulae) {
		(new ByteArrayInputStream(formulae.toString.getBytes())).solveCNF
	}

	def Result solveCNF(InputStream dimacs) {
		throw new Exception("Not Implemented")
	}

	def Result solveCNF(String filePath) {
		throw new Exception("Not Implemented")
	}
}
