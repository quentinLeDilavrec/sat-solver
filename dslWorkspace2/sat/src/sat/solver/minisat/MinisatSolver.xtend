package sat.solver.minisat


import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

class MinisatSolver extends sat.solver.Solver {

	override Result solveCNF(InputStream formula) {
		val process = Runtime.runtime.exec(#[
			"/bin/sh",
			"-c",
			"echo\"" +
				(new BufferedReader(new InputStreamReader(formula))).lines().parallel().collect(
					Collectors.joining("\n")) + "\" | /usr/bin/minisat | tail -1"
		])
		process.waitFor()

		new Result("minisat",
			(new BufferedReader(new InputStreamReader(process.getInputStream()))).readLine ?: "a" == 'SATISFIABLE')
	}
}
