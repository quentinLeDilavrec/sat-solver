/**
 * generated by Xtext 2.15.0
 */
package sat.propositionalLogicLanguage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sat.propositionalLogicLanguage.Binary#getRight <em>Right</em>}</li>
 *   <li>{@link sat.propositionalLogicLanguage.Binary#getLeft <em>Left</em>}</li>
 * </ul>
 *
 * @see sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage#getBinary()
 * @model abstract="true"
 * @generated
 */
public interface Binary extends Proposition
{
	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Proposition)
	 * @see sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage#getBinary_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Proposition getRight();

	/**
	 * Sets the value of the '{@link sat.propositionalLogicLanguage.Binary#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Proposition value);

	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Proposition)
	 * @see sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage#getBinary_Left()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Proposition getLeft();

	/**
	 * Sets the value of the '{@link sat.propositionalLogicLanguage.Binary#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Proposition value);

} // Binary
