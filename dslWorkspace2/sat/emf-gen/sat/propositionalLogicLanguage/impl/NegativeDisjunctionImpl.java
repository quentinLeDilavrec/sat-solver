/**
 * generated by Xtext 2.15.0
 */
package sat.propositionalLogicLanguage.impl;

import org.eclipse.emf.ecore.EClass;

import sat.propositionalLogicLanguage.NegativeDisjunction;
import sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Negative Disjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NegativeDisjunctionImpl extends BinaryImpl implements NegativeDisjunction
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NegativeDisjunctionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return PropositionalLogicLanguagePackage.Literals.NEGATIVE_DISJUNCTION;
	}

} //NegativeDisjunctionImpl
