/**
 * generated by Xtext 2.15.0
 */
package sat.propositionalLogicLanguage.impl;

import org.eclipse.emf.ecore.EClass;

import sat.propositionalLogicLanguage.Disjunction;
import sat.propositionalLogicLanguage.PropositionalLogicLanguagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Disjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DisjunctionImpl extends BinaryImpl implements Disjunction
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DisjunctionImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return PropositionalLogicLanguagePackage.Literals.DISJUNCTION;
	}

} //DisjunctionImpl
