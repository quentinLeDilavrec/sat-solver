/**
 * generated by Xtext 2.15.0
 */
package sat.propositionalLogicLanguage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see sat.propositionalLogicLanguage.PropositionalLogicLanguageFactory
 * @model kind="package"
 * @generated
 */
public interface PropositionalLogicLanguagePackage extends EPackage
{
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "propositionalLogicLanguage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.PropositionalLogicLanguage.sat";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "propositionalLogicLanguage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PropositionalLogicLanguagePackage eINSTANCE = sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl.init();

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.Proposition <em>Proposition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.Proposition
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getProposition()
	 * @generated
	 */
	int PROPOSITION = 6;

	/**
	 * The number of structural features of the '<em>Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPOSITION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.BinaryImpl <em>Binary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.BinaryImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getBinary()
	 * @generated
	 */
	int BINARY = 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__RIGHT = PROPOSITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__LEFT = PROPOSITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FEATURE_COUNT = PROPOSITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.NegativeConjunctionImpl <em>Negative Conjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.NegativeConjunctionImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getNegativeConjunction()
	 * @generated
	 */
	int NEGATIVE_CONJUNCTION = 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_CONJUNCTION__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_CONJUNCTION__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Negative Conjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_CONJUNCTION_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.NegativeDisjunctionImpl <em>Negative Disjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.NegativeDisjunctionImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getNegativeDisjunction()
	 * @generated
	 */
	int NEGATIVE_DISJUNCTION = 2;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DISJUNCTION__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DISJUNCTION__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Negative Disjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_DISJUNCTION_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.UnaryImpl <em>Unary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.UnaryImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getUnary()
	 * @generated
	 */
	int UNARY = 3;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY__CONTENT = PROPOSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FEATURE_COUNT = PROPOSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.ExcludeImpl <em>Exclude</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.ExcludeImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getExclude()
	 * @generated
	 */
	int EXCLUDE = 4;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Exclude</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.ExclusiveDisjunctionImpl <em>Exclusive Disjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.ExclusiveDisjunctionImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getExclusiveDisjunction()
	 * @generated
	 */
	int EXCLUSIVE_DISJUNCTION = 5;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUSIVE_DISJUNCTION__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUSIVE_DISJUNCTION__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Exclusive Disjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUSIVE_DISJUNCTION_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.ImpliesImpl <em>Implies</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.ImpliesImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getImplies()
	 * @generated
	 */
	int IMPLIES = 7;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Implies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.DisjunctionImpl <em>Disjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.DisjunctionImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getDisjunction()
	 * @generated
	 */
	int DISJUNCTION = 8;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Disjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.BiimpliesImpl <em>Biimplies</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.BiimpliesImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getBiimplies()
	 * @generated
	 */
	int BIIMPLIES = 9;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIIMPLIES__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIIMPLIES__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Biimplies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIIMPLIES_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.ConjunctionImpl <em>Conjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.ConjunctionImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getConjunction()
	 * @generated
	 */
	int CONJUNCTION = 10;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__RIGHT = BINARY__RIGHT;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__LEFT = BINARY__LEFT;

	/**
	 * The number of structural features of the '<em>Conjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.NegationImpl <em>Negation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.NegationImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getNegation()
	 * @generated
	 */
	int NEGATION = 11;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION__CONTENT = UNARY__CONTENT;

	/**
	 * The number of structural features of the '<em>Negation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.PrimitiveImpl <em>Primitive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.PrimitiveImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getPrimitive()
	 * @generated
	 */
	int PRIMITIVE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE__NAME = PROPOSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_FEATURE_COUNT = PROPOSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.FileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.FileImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getFile()
	 * @generated
	 */
	int FILE = 13;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__CONTENT = 0;

	/**
	 * The feature id for the '<em><b>Usings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__USINGS = 1;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link sat.propositionalLogicLanguage.impl.UsingImpl <em>Using</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see sat.propositionalLogicLanguage.impl.UsingImpl
	 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getUsing()
	 * @generated
	 */
	int USING = 14;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USING__KIND = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USING__NAME = 1;

	/**
	 * The number of structural features of the '<em>Using</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USING_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.NegativeConjunction <em>Negative Conjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Conjunction</em>'.
	 * @see sat.propositionalLogicLanguage.NegativeConjunction
	 * @generated
	 */
	EClass getNegativeConjunction();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Binary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary</em>'.
	 * @see sat.propositionalLogicLanguage.Binary
	 * @generated
	 */
	EClass getBinary();

	/**
	 * Returns the meta object for the containment reference '{@link sat.propositionalLogicLanguage.Binary#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see sat.propositionalLogicLanguage.Binary#getRight()
	 * @see #getBinary()
	 * @generated
	 */
	EReference getBinary_Right();

	/**
	 * Returns the meta object for the containment reference '{@link sat.propositionalLogicLanguage.Binary#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see sat.propositionalLogicLanguage.Binary#getLeft()
	 * @see #getBinary()
	 * @generated
	 */
	EReference getBinary_Left();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.NegativeDisjunction <em>Negative Disjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Disjunction</em>'.
	 * @see sat.propositionalLogicLanguage.NegativeDisjunction
	 * @generated
	 */
	EClass getNegativeDisjunction();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Unary <em>Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary</em>'.
	 * @see sat.propositionalLogicLanguage.Unary
	 * @generated
	 */
	EClass getUnary();

	/**
	 * Returns the meta object for the containment reference '{@link sat.propositionalLogicLanguage.Unary#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see sat.propositionalLogicLanguage.Unary#getContent()
	 * @see #getUnary()
	 * @generated
	 */
	EReference getUnary_Content();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Exclude <em>Exclude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exclude</em>'.
	 * @see sat.propositionalLogicLanguage.Exclude
	 * @generated
	 */
	EClass getExclude();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.ExclusiveDisjunction <em>Exclusive Disjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exclusive Disjunction</em>'.
	 * @see sat.propositionalLogicLanguage.ExclusiveDisjunction
	 * @generated
	 */
	EClass getExclusiveDisjunction();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Proposition <em>Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Proposition</em>'.
	 * @see sat.propositionalLogicLanguage.Proposition
	 * @generated
	 */
	EClass getProposition();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Implies <em>Implies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Implies</em>'.
	 * @see sat.propositionalLogicLanguage.Implies
	 * @generated
	 */
	EClass getImplies();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Disjunction <em>Disjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjunction</em>'.
	 * @see sat.propositionalLogicLanguage.Disjunction
	 * @generated
	 */
	EClass getDisjunction();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Biimplies <em>Biimplies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Biimplies</em>'.
	 * @see sat.propositionalLogicLanguage.Biimplies
	 * @generated
	 */
	EClass getBiimplies();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Conjunction <em>Conjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conjunction</em>'.
	 * @see sat.propositionalLogicLanguage.Conjunction
	 * @generated
	 */
	EClass getConjunction();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Negation <em>Negation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negation</em>'.
	 * @see sat.propositionalLogicLanguage.Negation
	 * @generated
	 */
	EClass getNegation();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Primitive <em>Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive</em>'.
	 * @see sat.propositionalLogicLanguage.Primitive
	 * @generated
	 */
	EClass getPrimitive();

	/**
	 * Returns the meta object for the attribute '{@link sat.propositionalLogicLanguage.Primitive#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see sat.propositionalLogicLanguage.Primitive#getName()
	 * @see #getPrimitive()
	 * @generated
	 */
	EAttribute getPrimitive_Name();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.File <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see sat.propositionalLogicLanguage.File
	 * @generated
	 */
	EClass getFile();

	/**
	 * Returns the meta object for the containment reference list '{@link sat.propositionalLogicLanguage.File#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Content</em>'.
	 * @see sat.propositionalLogicLanguage.File#getContent()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_Content();

	/**
	 * Returns the meta object for the containment reference list '{@link sat.propositionalLogicLanguage.File#getUsings <em>Usings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Usings</em>'.
	 * @see sat.propositionalLogicLanguage.File#getUsings()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_Usings();

	/**
	 * Returns the meta object for class '{@link sat.propositionalLogicLanguage.Using <em>Using</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Using</em>'.
	 * @see sat.propositionalLogicLanguage.Using
	 * @generated
	 */
	EClass getUsing();

	/**
	 * Returns the meta object for the attribute '{@link sat.propositionalLogicLanguage.Using#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see sat.propositionalLogicLanguage.Using#getKind()
	 * @see #getUsing()
	 * @generated
	 */
	EAttribute getUsing_Kind();

	/**
	 * Returns the meta object for the attribute '{@link sat.propositionalLogicLanguage.Using#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see sat.propositionalLogicLanguage.Using#getName()
	 * @see #getUsing()
	 * @generated
	 */
	EAttribute getUsing_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PropositionalLogicLanguageFactory getPropositionalLogicLanguageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.NegativeConjunctionImpl <em>Negative Conjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.NegativeConjunctionImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getNegativeConjunction()
		 * @generated
		 */
		EClass NEGATIVE_CONJUNCTION = eINSTANCE.getNegativeConjunction();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.BinaryImpl <em>Binary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.BinaryImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getBinary()
		 * @generated
		 */
		EClass BINARY = eINSTANCE.getBinary();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY__RIGHT = eINSTANCE.getBinary_Right();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY__LEFT = eINSTANCE.getBinary_Left();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.NegativeDisjunctionImpl <em>Negative Disjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.NegativeDisjunctionImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getNegativeDisjunction()
		 * @generated
		 */
		EClass NEGATIVE_DISJUNCTION = eINSTANCE.getNegativeDisjunction();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.UnaryImpl <em>Unary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.UnaryImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getUnary()
		 * @generated
		 */
		EClass UNARY = eINSTANCE.getUnary();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY__CONTENT = eINSTANCE.getUnary_Content();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.ExcludeImpl <em>Exclude</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.ExcludeImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getExclude()
		 * @generated
		 */
		EClass EXCLUDE = eINSTANCE.getExclude();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.ExclusiveDisjunctionImpl <em>Exclusive Disjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.ExclusiveDisjunctionImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getExclusiveDisjunction()
		 * @generated
		 */
		EClass EXCLUSIVE_DISJUNCTION = eINSTANCE.getExclusiveDisjunction();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.Proposition <em>Proposition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.Proposition
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getProposition()
		 * @generated
		 */
		EClass PROPOSITION = eINSTANCE.getProposition();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.ImpliesImpl <em>Implies</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.ImpliesImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getImplies()
		 * @generated
		 */
		EClass IMPLIES = eINSTANCE.getImplies();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.DisjunctionImpl <em>Disjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.DisjunctionImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getDisjunction()
		 * @generated
		 */
		EClass DISJUNCTION = eINSTANCE.getDisjunction();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.BiimpliesImpl <em>Biimplies</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.BiimpliesImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getBiimplies()
		 * @generated
		 */
		EClass BIIMPLIES = eINSTANCE.getBiimplies();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.ConjunctionImpl <em>Conjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.ConjunctionImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getConjunction()
		 * @generated
		 */
		EClass CONJUNCTION = eINSTANCE.getConjunction();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.NegationImpl <em>Negation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.NegationImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getNegation()
		 * @generated
		 */
		EClass NEGATION = eINSTANCE.getNegation();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.PrimitiveImpl <em>Primitive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.PrimitiveImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getPrimitive()
		 * @generated
		 */
		EClass PRIMITIVE = eINSTANCE.getPrimitive();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMITIVE__NAME = eINSTANCE.getPrimitive_Name();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.FileImpl <em>File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.FileImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getFile()
		 * @generated
		 */
		EClass FILE = eINSTANCE.getFile();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__CONTENT = eINSTANCE.getFile_Content();

		/**
		 * The meta object literal for the '<em><b>Usings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__USINGS = eINSTANCE.getFile_Usings();

		/**
		 * The meta object literal for the '{@link sat.propositionalLogicLanguage.impl.UsingImpl <em>Using</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see sat.propositionalLogicLanguage.impl.UsingImpl
		 * @see sat.propositionalLogicLanguage.impl.PropositionalLogicLanguagePackageImpl#getUsing()
		 * @generated
		 */
		EClass USING = eINSTANCE.getUsing();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USING__KIND = eINSTANCE.getUsing_Kind();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USING__NAME = eINSTANCE.getUsing_Name();

	}

} //PropositionalLogicLanguagePackage
