/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sat.Unary#getArg <em>Arg</em>}</li>
 * </ul>
 *
 * @see sat.SATPackage#getUnary()
 * @model abstract="true"
 * @generated
 */
public interface Unary extends Proposition, LogicalOperator {
	/**
	 * Returns the value of the '<em><b>Arg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arg</em>' containment reference.
	 * @see #setArg(Proposition)
	 * @see sat.SATPackage#getUnary_Arg()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Proposition getArg();

	/**
	 * Sets the value of the '{@link sat.Unary#getArg <em>Arg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arg</em>' containment reference.
	 * @see #getArg()
	 * @generated
	 */
	void setArg(Proposition value);

} // Unary
