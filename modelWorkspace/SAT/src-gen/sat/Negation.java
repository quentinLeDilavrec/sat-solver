/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Negation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getNegation()
 * @model
 * @generated
 */
public interface Negation extends Unary {
} // Negation
