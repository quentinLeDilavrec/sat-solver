/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Implies</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getImplies()
 * @model
 * @generated
 */
public interface Implies extends Binary {
} // Implies
