/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Disjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getDisjunction()
 * @model
 * @generated
 */
public interface Disjunction extends Binary {
} // Disjunction
