/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Biimplies</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getBiimplies()
 * @model
 * @generated
 */
public interface Biimplies extends Binary {
} // Biimplies
