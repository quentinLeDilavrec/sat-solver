/**
 */
package sat.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import sat.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see sat.SATPackage
 * @generated
 */
public class SATAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SATPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SATAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = SATPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SATSwitch<Adapter> modelSwitch =
		new SATSwitch<Adapter>()
		{
			@Override
			public Adapter caseProposition(Proposition object)
			{
				return createPropositionAdapter();
			}
			@Override
			public Adapter caseLogicalOperator(LogicalOperator object)
			{
				return createLogicalOperatorAdapter();
			}
			@Override
			public Adapter case( object)
			{
				return createAdapter();
			}
			@Override
			public Adapter casePrimitive(Primitive object)
			{
				return createPrimitiveAdapter();
			}
			@Override
			public Adapter caseUnary(Unary object)
			{
				return createUnaryAdapter();
			}
			@Override
			public Adapter caseBinary(Binary object)
			{
				return createBinaryAdapter();
			}
			@Override
			public Adapter caseNegation(Negation object)
			{
				return createNegationAdapter();
			}
			@Override
			public Adapter caseConjunction(Conjunction object)
			{
				return createConjunctionAdapter();
			}
			@Override
			public Adapter caseDisjunction(Disjunction object)
			{
				return createDisjunctionAdapter();
			}
			@Override
			public Adapter caseImplies(Implies object)
			{
				return createImpliesAdapter();
			}
			@Override
			public Adapter caseExclude(Exclude object)
			{
				return createExcludeAdapter();
			}
			@Override
			public Adapter caseBiimplies(Biimplies object)
			{
				return createBiimpliesAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object)
			{
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link sat.Proposition <em>Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Proposition
	 * @generated
	 */
	public Adapter createPropositionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.LogicalOperator <em>Logical Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.LogicalOperator
	 * @generated
	 */
	public Adapter createLogicalOperatorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat. <em>SAT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.
	 * @generated
	 */
	public Adapter createAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Primitive <em>Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Primitive
	 * @generated
	 */
	public Adapter createPrimitiveAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Unary <em>Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Unary
	 * @generated
	 */
	public Adapter createUnaryAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Binary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Binary
	 * @generated
	 */
	public Adapter createBinaryAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Negation <em>Negation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Negation
	 * @generated
	 */
	public Adapter createNegationAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Conjunction <em>Conjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Conjunction
	 * @generated
	 */
	public Adapter createConjunctionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Disjunction <em>Disjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Disjunction
	 * @generated
	 */
	public Adapter createDisjunctionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Implies <em>Implies</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Implies
	 * @generated
	 */
	public Adapter createImpliesAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Exclude <em>Exclude</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Exclude
	 * @generated
	 */
	public Adapter createExcludeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link sat.Biimplies <em>Biimplies</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see sat.Biimplies
	 * @generated
	 */
	public Adapter createBiimpliesAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} //SATAdapterFactory
