/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sat.Binary#getRhs <em>Rhs</em>}</li>
 *   <li>{@link sat.Binary#getLhs <em>Lhs</em>}</li>
 * </ul>
 *
 * @see sat.SATPackage#getBinary()
 * @model abstract="true"
 * @generated
 */
public interface Binary extends Proposition, LogicalOperator {
	/**
	 * Returns the value of the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rhs</em>' containment reference.
	 * @see #setRhs(Proposition)
	 * @see sat.SATPackage#getBinary_Rhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Proposition getRhs();

	/**
	 * Sets the value of the '{@link sat.Binary#getRhs <em>Rhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rhs</em>' containment reference.
	 * @see #getRhs()
	 * @generated
	 */
	void setRhs(Proposition value);

	/**
	 * Returns the value of the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs</em>' containment reference.
	 * @see #setLhs(Proposition)
	 * @see sat.SATPackage#getBinary_Lhs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Proposition getLhs();

	/**
	 * Sets the value of the '{@link sat.Binary#getLhs <em>Lhs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs</em>' containment reference.
	 * @see #getLhs()
	 * @generated
	 */
	void setLhs(Proposition value);

} // Binary
