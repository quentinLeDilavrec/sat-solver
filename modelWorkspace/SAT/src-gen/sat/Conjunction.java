/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getConjunction()
 * @model
 * @generated
 */
public interface Conjunction extends Binary {
} // Conjunction
