/**
 */
package sat;

import java.util.Map;
import java.util.Optional;

import jdk.nashorn.internal.ir.annotations.Immutable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sat.Primitive#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see sat.SATPackage#getPrimitive()
 * @model
 * @generated
 */
public interface Primitive extends Proposition {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see sat.SATPackage#getPrimitive_Name()
	 * @model
	 * @generated
	 */
	String getName();

	@Override
	default Optional<Boolean> valuation(Map<String, Boolean> vars) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Sets the value of the '{@link sat.Primitive#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Primitive
