/**
 */
package sat;

import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Proposition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getProposition()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Proposition extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @model
	 * @generated
	 */
	public Optional<Boolean> valuation(Map<String, Boolean> vars);

} // Proposition
