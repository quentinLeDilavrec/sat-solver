/**
 */
package sat.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import sat.Proposition;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SAT</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Impl extends MinimalEObjectImpl.Container implements Proposition
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Impl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return SATPackage.Literals.SAT;
	}

} //Impl
