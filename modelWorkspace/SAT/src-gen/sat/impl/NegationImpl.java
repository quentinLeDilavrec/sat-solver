/**
 */
package sat.impl;

import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;

import sat.Negation;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Negation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NegationImpl extends UnaryImpl implements Negation {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NegationImpl() {
		super();
	}
	
	@Override
	public Optional<Boolean> valuation(Map<String, Boolean> vars) {
		return arg.valuation(vars).map(b->!b);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SATPackage.Literals.NEGATION;
	}

} //NegationImpl
