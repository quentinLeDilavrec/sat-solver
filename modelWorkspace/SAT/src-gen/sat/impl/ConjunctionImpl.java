/**
 */
package sat.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.emf.ecore.EClass;

import sat.Conjunction;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConjunctionImpl extends BinaryImpl implements Conjunction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConjunctionImpl() {
		super();
	}
	
	@Override
	public Optional<Boolean> valuation(Map<String, Boolean> vars) {
		Optional<Boolean> _lhs = lhs.valuation(vars), _rhs = rhs.valuation(vars);
		if (_lhs.isPresent())
			if (_lhs.get())
				return _rhs;
			else
				return Optional.of(Boolean.FALSE);
		return _rhs.filter(b->!b);
		//boolean none = false;
		//l.stream().forEach(b -> b.isPresent());
		//if (none)
		//	return Optional.empty();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SATPackage.Literals.CONJUNCTION;
	}

} //ConjunctionImpl
