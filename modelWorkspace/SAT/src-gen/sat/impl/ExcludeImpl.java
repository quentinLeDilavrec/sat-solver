/**
 */
package sat.impl;

import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;

import sat.Exclude;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exclude</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExcludeImpl extends BinaryImpl implements Exclude {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExcludeImpl() {
		super();
	}
	
	@Override
	public Optional<Boolean> valuation(Map<String, Boolean> vars) {
		Optional<Boolean> _lhs = lhs.valuation(vars), _rhs = rhs.valuation(vars);
		if (_lhs.isPresent())
			if (_lhs.get())
				return _rhs.map(b->!b);
			else
				return Optional.of(Boolean.FALSE).map(b->!b);
		return _rhs.filter(b->!b).map(b->!b);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SATPackage.Literals.EXCLUDE;
	}

} //ExcludeImpl
