/**
 */
package sat.impl;

import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;

import sat.Implies;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Implies</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ImpliesImpl extends BinaryImpl implements Implies {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpliesImpl() {
		super();
	}
	
	@Override
	public Optional<Boolean> valuation(Map<String, Boolean> vars) {
		Optional<Boolean> _lhs = lhs.valuation(vars), _rhs = rhs.valuation(vars);
		if (_lhs.isPresent())
			if (_lhs.get())
				return _rhs;
			else
				return Optional.of(Boolean.TRUE);
		return Optional.empty();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SATPackage.Literals.IMPLIES;
	}

} //ImpliesImpl
