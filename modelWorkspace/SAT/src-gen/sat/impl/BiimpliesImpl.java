/**
 */
package sat.impl;

import org.eclipse.emf.ecore.EClass;

import sat.Biimplies;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Biimplies</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BiimpliesImpl extends BinaryImpl implements Biimplies {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BiimpliesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SATPackage.Literals.BIIMPLIES;
	}

} //BiimpliesImpl
