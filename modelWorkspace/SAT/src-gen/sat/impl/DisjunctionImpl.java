/**
 */
package sat.impl;

import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;

import sat.Disjunction;
import sat.SATPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Disjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DisjunctionImpl extends BinaryImpl implements Disjunction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DisjunctionImpl() {
		super();
	}
	
	@Override
	public Optional<Boolean> valuation(Map<String, Boolean> vars) {
		Optional<Boolean> _lhs = lhs.valuation(vars), _rhs = rhs.valuation(vars);
		if (_lhs.isPresent())
			if (_lhs.get())
				return Optional.of(Boolean.FALSE);
			else
				return _rhs;
		return _rhs.filter(b->b);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SATPackage.Literals.DISJUNCTION;
	}

} //DisjunctionImpl
