/**
 */
package sat;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exclude</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sat.SATPackage#getExclude()
 * @model
 * @generated
 */
public interface Exclude extends Binary {
} // Exclude
